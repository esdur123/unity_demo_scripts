using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TowerGameEnemyCtrl : MonoBehaviour
{
    public enum EnemyTypeEnum
    {
        footman,
        length
    }

    [Header("Footman")]
    [SerializeField] GameObject sf_prefab_footman;
    [Header("Pool")]
    [SerializeField] Transform sf_tr_pool;
    [Header("ShowRoot")]
    [SerializeField] Transform sf_tr_show_root;
    [Header("ShowPosGeneral")]
    [SerializeField] Transform[] sf_tr_show_pos;

    Vector3 m_footman_scale = new Vector3(120,120,120);
    const int footman_max_count = 20;

    public void init()
    {
        initFootman();
        m_is_clear = false;
    }

    public void reInit()
    {
        m_i_dead_enemy_total = 0;
        m_is_clear = false;
    }


    List<EnemyUnit> m_enemy_unit_list = new List<EnemyUnit>();
    float hp_footman = 500;
    void initFootman()
    {
        //Debug.LogError("initFootman");
        for (int i = 0; i < footman_max_count; i++)
        {
            GameObject obj = Instantiate(sf_prefab_footman);
            obj.transform.SetParent(sf_tr_pool);
            obj.transform.localPosition = Vector3.zero;
            obj.name = obj.name + " [" + m_enemy_unit_list.Count + "]";
            EnemyUnit script = obj.GetComponent<EnemyUnit>();
            script.m_Animator.enabled = false;
            script.index = i;
            script.is_in_use = false;
            script.col.enabled = false;
            script.type_enemy = EnemyTypeEnum.footman;
            script.cur_hp = hp_footman;
            script.max_hp = hp_footman;
            m_enemy_unit_list.Add(script);
        }
    }

    public void callEnemy(EnemyTypeEnum _type, int _count)
    {
        //Debug.LogError("callEnemy");
        int cur_count = 0;
        for (int i = 0; i < m_enemy_unit_list.Count; i++)
        {
            if (m_enemy_unit_list[i].is_in_use) continue;

            m_enemy_unit_list[i].is_in_use = true;
            m_enemy_unit_list[i].obj.transform.SetParent(sf_tr_show_root);
            Transform tr = getGeneralTransform();
            m_enemy_unit_list[i].obj.transform.position = tr.position;
            m_enemy_unit_list[i].obj.transform.forward = tr.forward;
            m_enemy_unit_list[i].obj.transform.localScale = m_footman_scale;
            m_enemy_unit_list[i].init(); // 啟動
            cur_count++;

            if (cur_count == _count) break;
        }
    }

    bool m_is_clear = false;
    public void clear()
    {
        m_is_clear = true;
        for (int i = 0; i < m_enemy_unit_list.Count; i++)
        {
            if (!m_enemy_unit_list[i].is_in_use) continue;
            m_enemy_unit_list[i].recycle();
        }

        
    }

    public void recycle(int _index)
    {
        m_enemy_unit_list[_index].is_in_use = false;
        m_enemy_unit_list[_index].gameObject.transform.SetParent(sf_tr_pool);
        m_enemy_unit_list[_index].gameObject.transform.localPosition = Vector3.zero;

        // 有人回收了 把靠近名單更新
        //for (int i = 0; i < m_enemy_unit_list.Count; i++)
        //{
        //    if (!m_enemy_unit_list[i].is_in_use) continue;
        //    m_enemy_unit_list[i].removeFromTriggerEnemyList(_index);
        //}
    }


    int m_g_index = 0;
    Transform getGeneralTransform()
    {        
        m_g_index++;
        if (m_g_index == sf_tr_show_pos.Length)
        {
            m_g_index = 0;
        }

        //Debug.Log("::::: m_g_index = "+ m_g_index+", pos="+ sf_tr_show_pos[m_g_index].position);
        return sf_tr_show_pos[m_g_index];
    }

    public void myUpdate()
    {
        //for (int i = 0; i < m_enemy_unit_list.Count; i++)
        //{
        //    if (!m_enemy_unit_list[i].is_in_use) continue;
        //    m_enemy_unit_list[i].myUpdate();
        //}
    }


    public List<EnemyUnit> getCurEnemyList()
    {
        List<EnemyUnit> list = new List<EnemyUnit>();
        for (int i = 0; i < m_enemy_unit_list.Count; i++)
        {
            if (!m_enemy_unit_list[i].is_in_use) continue;
            list.Add(m_enemy_unit_list[i]);
        }

        return list;
    }

    public void killAll()
    {
        for (int i = 0; i < m_enemy_unit_list.Count; i++)
        {
            if (!m_enemy_unit_list[i].is_in_use) continue;
            m_enemy_unit_list[i].triggerDead();
        }
    }


    // 全場傷害
    public void aoeFireball()
    {
        StartCoroutine(_aoeFireball());
    }

    IEnumerator _aoeFireball()
    {
        
        float dmg = getPlayerCtrl().getSkillDmg(SkillUnit.type.skill_fireball);
        getAudioCtrl().playMusic(AudioCtrl.MusicClipsEnum.CSG_Dance_Party_Jingle_Loop);          

        for (int i = 0; i < 5; i++) // 每隔兩秒 循環五次 
        {
            yield return new WaitForSeconds(2);
            if (m_is_clear)
            {
                Debug.LogError("m_is_clear");
                yield break;
            }

            for (int j = 0; j < m_enemy_unit_list.Count; j++)
            {
                if (!m_enemy_unit_list[j].is_in_use) continue;
                //if(m_enemy_unit_list[j].getIsDead()) continue;
                //Debug.LogError("["+j+"] DDDDDDDDDDDDDDDDDDDD");       

                bool is_dead = m_enemy_unit_list[j].receiveDamage(dmg);
                //if (j == 0)
                //{
                //    Debug.Log("dmg="+ dmg + ", hp=" + m_enemy_unit_list[j].cur_hp);
                //}
                // 傷害的聲音
                getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.FA_Funny_Impact_1_1);

                if (!is_dead)
                {
                    m_enemy_unit_list[j].triggerHit();
                    //m_enemy_unit_list[j].receiveDamage(dmg);
                }
                else
                {
                    m_enemy_unit_list[j].triggerDead();
                }

            }
            
        }

        getAudioCtrl().playMusic(AudioCtrl.MusicClipsEnum.tower_game_play);
        yield break;       
    }


    public void endGameWorks()
    {
        // 讓場上小兵都消失?
        clear();
    }

    int m_i_dead_enemy_total = 0;
    public void addKilledEnemy(int _count)
    {
        m_i_dead_enemy_total += _count;
    }

    public int getKilledEnemyAmount()
    {
        return m_i_dead_enemy_total;
    }

    PlayerCtrl m_PlayerCtrl = null;
    PlayerCtrl getPlayerCtrl()
    {
        if (m_PlayerCtrl == null) m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        return m_PlayerCtrl;
    }

    AudioCtrl m_AudioCtrl;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        return m_AudioCtrl;
    }
}
