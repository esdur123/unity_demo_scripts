﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameFlowUnit // 單位
{
    public enum GemeFlowTypeEnum
    {
        none,
        init,
        dialogue,
        engame_dialogue,
        engame_select_char,
        engame_play,
        portal,
        tower_game, 
        length
    }

    public GemeFlowTypeEnum type = GemeFlowTypeEnum.none;
    public DialogueSets dialogue_sets = null;
    public PortalSet portal_set = null;
    public InitSet init_set = null;
    public EnGameSelectSet en_select_set = null;
    public TowerGameSet tower_game_set = null;
}


public class GameFlowCtrl : MonoBehaviour
{
    //DialogueCtrl MyFinder.Instance.getDialogueCtrl();
    //PortalCtrl MyFinder.Instance.getPortalCtrl();
    //FlowInitCtrl MyFinder.Instance.getFlowInitCtrl();
    //DirCtrl MyFinder.Instance.getDirCtrl();
    //EnGameSelectChar MyFinder.Instance.getEnGameSelectChar();
    // List<DialogueUnit> m_dialogueList = new List<DialogueUnit>();
    //[Header("360圖片")]
    //[SerializeField] GameObject sf_obj_360;
    [Header("地板網格")]
    [SerializeField] Animator sf_anim_grid_ground;
    [Header("重新開始按鈕")]
    [SerializeField] GameObject sf_obj_btn_restartall;
    //[Header("回教室按鈕")]
    //[SerializeField] GameObject sf_obj_btn_back_to_classroom;

    ClickEffect m_ClickEffect = null;
    TowerBottomBoard m_TowerBottomBoard = null;
    TowerGameCtrl m_TowerGameCtrl = null;
    MySystem m_mySystem = null;
    //Main.demoEnum m_demo_mode;
    public void init()
    {
        if (m_ClickEffect == null) m_ClickEffect = MyFinder.Instance.getClickEffect();
        if (m_TowerBottomBoard == null) m_TowerBottomBoard = MyFinder.Instance.getTowerBottomBoard();
        if (m_TowerGameCtrl == null) m_TowerGameCtrl = MyFinder.Instance.getTowerGameCtrl();
        if (m_mySystem == null) m_mySystem = MyFinder.Instance.getMySystem();
        //m_demo_mode = MyFinder.Instance.getMain().p_demo_mode;

        //if (m_demo_mode == Main.demoEnum.loop_tower_game)
        //{
        //    sf_obj_btn_back_to_classroom.SetActive(false);
        //}
    }

    
    public void initGameFlow()
    {
        
        //if (m_demo_mode == Main.demoEnum.loop_tower_game)
        //{
        //    sf_obj_btn_restartall.SetActive(true);
        //}
        //else
        //{
        //    sf_obj_btn_restartall.SetActive(false);
        //}


            
        readGameFlow();
        //sf_obj_360.SetActive(true);
        m_mySystem.sf_obj_360.SetActive(true);
        m_mySystem.changeSkybox(MySystem.skyboxEnum.default_skybox);

        startGameFlow();
    }



    // 進入塔防遊戲 一切game flow重置
    public void checkInOut(bool _is_fadein, BuildingCtrl.buildings_order_enum _from_enum, BuildingCtrl.buildings_order_enum _to_enum)
    {
        // 進入game flow
        if (_is_fadein == true && _to_enum == BuildingCtrl.buildings_order_enum.tower_game_flow)
        {
            Debug.Log("進入game flow");
            // click effct
            m_ClickEffect.setUpdate(true);

            m_i_cur_flow = -1; // 立一個flag, 讓等等cover fadein之後 知道要init game flow

            // 地板網格
            //sf_anim_grid_ground.gameObject.SetActive(false);

            
            m_TowerBottomBoard.init();
            //m_TowerGameCtrl.clear();
        }


        // 離開game flow
        if (_from_enum == BuildingCtrl.buildings_order_enum.tower_game_flow)
        {
            Debug.Log("離開game flow");
            // click effct

            m_ClickEffect.setUpdate(false);

            // 地板網格
            sf_anim_grid_ground.gameObject.SetActive(false);

            m_TowerBottomBoard.exit();
            m_TowerGameCtrl.clear();
        }
    }


    public void startGameFlow()
    {
        toNextFlow();
        MyFinder.Instance.getDirCtrl().setCheckingBool(true, m_game_flow_list[m_i_cur_flow].type);
    }


    List<GameFlowUnit> m_game_flow_list = new List<GameFlowUnit>();
    int m_i_cur_flow = -1;
    public void readGameFlow()
    {
        m_i_cur_flow = -1;
        m_game_flow_list = MyFinder.Instance.getCSVReader().getCurGameFlow();
    }


    public void toNextFlow()
    {
        Debug.Log("-------------toNextFlow: [" + m_i_cur_flow + "]->[" + (m_i_cur_flow + 1) + "]");
        m_i_cur_flow++;

        Debug.Log("m_i_cur_flow="+m_i_cur_flow);
        Debug.Log("m_game_flow_list.Count="+ m_game_flow_list.Count);
        if (m_i_cur_flow >= m_game_flow_list.Count)
        {
            m_i_cur_flow = m_game_flow_list.Count - 1;
            MyFinder.Instance.getDirCtrl().setCheckingBool(false, m_game_flow_list[m_i_cur_flow].type);
            Debug.Log("*************************************END GAME FLOW....");
            return;
        }

        // 準備處理 ｇａｍｅｆｌｏｗ　邏輯
        Debug.Log("WWWWWWWW Cur game flow type: " + m_game_flow_list[m_i_cur_flow].type);

        if (m_game_flow_list[m_i_cur_flow].type == GameFlowUnit.GemeFlowTypeEnum.init)
        {
            InitSet set = m_game_flow_list[m_i_cur_flow].init_set;
            MyFinder.Instance.getFlowInitCtrl().initFlow(set);
        }

        if (m_game_flow_list[m_i_cur_flow].type == GameFlowUnit.GemeFlowTypeEnum.dialogue)
        {
            DialogueSets sets = m_game_flow_list[m_i_cur_flow].dialogue_sets;
            MyFinder.Instance.getDialogueCtrl().initDialogue(sets);
        }

        if (m_game_flow_list[m_i_cur_flow].type == GameFlowUnit.GemeFlowTypeEnum.portal)
        {
            PortalSet set = m_game_flow_list[m_i_cur_flow].portal_set;
            MyFinder.Instance.getPortalCtrl().initPortal(set);
        }

        if (m_game_flow_list[m_i_cur_flow].type == GameFlowUnit.GemeFlowTypeEnum.engame_dialogue)
        {
            DialogueSets sets = m_game_flow_list[m_i_cur_flow].dialogue_sets;
            MyFinder.Instance.getDialogueCtrl().initDialogueEn(sets);
        }

        if (m_game_flow_list[m_i_cur_flow].type == GameFlowUnit.GemeFlowTypeEnum.engame_select_char)
        {
            EnGameSelectSet sets = m_game_flow_list[m_i_cur_flow].en_select_set;
            MyFinder.Instance.getEnGameSelectChar().initSelectChar(sets);
        }

        if (m_game_flow_list[m_i_cur_flow].type == GameFlowUnit.GemeFlowTypeEnum.tower_game)
        {
            TowerGameSet set = m_game_flow_list[m_i_cur_flow].tower_game_set;
            MyFinder.Instance.getTowerGameCtrl().init(set);
        }
    }
    

    public GameFlowUnit getCurFlow()
    {        
        return m_game_flow_list[m_i_cur_flow];
    }

    public int getCurFlowIndex()
    {
        return m_i_cur_flow;
    }


    public void readEnGameGameFlow()
    {
        Debug.Log("readEnGameGameFlow");
        m_i_cur_flow = -1;
        m_game_flow_list = new List<GameFlowUnit>();

        /// 
        GameFlowUnit g_unit01 = new GameFlowUnit();
        g_unit01.type = GameFlowUnit.GemeFlowTypeEnum.engame_dialogue;
        g_unit01.dialogue_sets = new DialogueSets();
        g_unit01.dialogue_sets.first_content = "[請點擊]";
        g_unit01.dialogue_sets.unit_list = new List<DialogueUnit>();
        g_unit01.dialogue_sets.pos = new Vector3(-5.826264f, 2.23f, 17.57067f);

        DialogueUnit a01 = new DialogueUnit();
        a01.content = "外星生物入侵! 請選擇人物，和您要挑戰的關卡!";
        g_unit01.dialogue_sets.unit_list.Add(a01);

        //DialogueUnit a02 = new DialogueUnit();
        //a02.content = "2222 2222 222222 2222 2222 222222 2222 2222 222222 2222 2222 222222 2222 2222 222222 2222 2222 222222 2222 2222 222222 2222 2222 222222 ";
        //unit.dialogue_sets.unit_list.Add(a02);

        //DialogueUnit a03 = new DialogueUnit();
        //a03.content = "對話結束 ";
        //g_unit01.dialogue_sets.unit_list.Add(a03);

        m_game_flow_list.Add(g_unit01);


        ///
        GameFlowUnit g_unit02 = new GameFlowUnit();
        g_unit02.type = GameFlowUnit.GemeFlowTypeEnum.engame_select_char;
        g_unit02.en_select_set = new EnGameSelectSet();
        m_game_flow_list.Add(g_unit02);
    }


    public void myUpdate()
    {
        m_TowerBottomBoard.myUpdate(); // 他自己控制開關
    }
}

