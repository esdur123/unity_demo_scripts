using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerGameSet
{
    public Vector3 pos = new Vector3();
    public int Level = -1;
    public int questions = -1;
}


public class TowerGameCtrl : MonoBehaviour
{
    [Header("Questions")]
    [SerializeField] TowerQuest[] sf_quest_arr;
    //[Header("Enemy")]
    //[SerializeField] GameObject[] sf_obj_enemy_arr;
    [Header("DirTarget")]
    [SerializeField] GameObject sf_obj_dir_target;
    [Header("Camera")]
    [SerializeField] Transform sf_tr_camera;
    [Header("地板網格")]
    [SerializeField] Animator sf_anim_grid_ground;
    [Header("Tower")]
    [SerializeField] GameObject sf_obj_buildings;
    [SerializeField] GameObject sf_obj_enemy_castle_crash_fx;
    [SerializeField] GameObject sf_obj_enemy_castle_smoke;
    [Header("Alarm")]
    [SerializeField] GameObject sf_obj_crack_alarm;
    [Header("SetPos")]
    [SerializeField] Transform sf_tr_set_pos_lock_y;
    [Header("Score")]
    [SerializeField] public Image sf_img_score;
    [SerializeField] Text sf_txt_score;
    [Header("Btn")]
    [SerializeField] GameObject sf_obj_skill_root;
    [SerializeField] GameObject sf_obj_backage_btn;
    //[SerializeField] GameObject sf_obj_back_to_class;
    Main.demoEnum m_demo_mode;

    const int c_max_quest = 5;
    //Vector3[] m_question_pos_arr = new Vector3[] {
    //      new Vector3(0    ,0.95f  ,0)
    //    , new Vector3(1.1f ,1.15f  ,0)
    //    , new Vector3(-0.7f,1.05f  ,0)
    //    , new Vector3(-3.0f,1.8f  ,0)
    //    , new Vector3(5.2f,3.5f  ,0)
    //};

    //Vector3[] m_question_pos_arr = new Vector3[] {
    //      new Vector3(0f    ,58.5f  ,-14.4f) // 中
    //    , new Vector3( -36.95f ,55.4f  ,-23.3f) // 左一
    //     , new Vector3(-70,42 ,-52.7f) // 左二
    //    , new Vector3(49.7f    ,52f  ,-30.8f) // 右一
    //    , new Vector3(69.6f,46.86f ,-62.7f) // 右二
    //};

 
    float c_dist_dir_target = 100.0f;
    //const int c_set_questions = 5;

    DirCtrl m_DirCtrl = null;
    MyTools m_MyTools = null;
    CSVReader m_CSVReader = null;
    TowerGameEnemyCtrl m_TowerGameEnemyCtrl = null;
    AudioCtrl m_AudioCtrl = null;
    int m_lvl = -1;
    int m_question_count = -1;
    List<EnglishWords> m_en_quest_list = new List<EnglishWords>();
    int m_en_quest_cur_index = 0;
    int m_en_quest_max_count = 0;
    TowerGameSet m_reveived_TowerGameSet = null;
    TowerGameBackageCtrl m_TowerGameBackageCtrl = null;
    TowerGameSkillCtrl m_TowerGameSkillCtrl = null;
    //PlayerCtrl m_PlayerCtrl = null;
    //bool m_is_finish_quests = false;
    public void init(TowerGameSet _set)
    {
        m_demo_mode = MyFinder.Instance.getMain().p_demo_mode;
        // 題目
        //for (int i = 0; i < sf_quest_arr.Length; i++)
        //{
        //    //sf_quest_arr[i].p_is_in_use = false;
        //    sf_quest_arr[i].clear();
        //    sf_quest_arr[i].gameObject.SetActive(false);
        //}
        //sf_obj_buildings.SetActive(false);

        //m_is_finish_quests = false;
        sf_obj_enemy_castle_crash_fx.SetActive(false);
        sf_obj_enemy_castle_smoke.SetActive(false);
        sf_obj_skill_root.SetActive(true);
        sf_obj_backage_btn.SetActive(true);

        is_stop_guest = false;
        m_is_clear = false;
        m_reveived_TowerGameSet = _set;
        if (m_MyTools == null) m_MyTools = MyFinder.Instance.getMyTools();
        //if (m_PlayerCtrl == null) m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl(); //來一個 getPlayerCtrl()

        //_set.pos = new Vector3(-1,0,-1);

        Vector3 lock_y_pos = new Vector3(_set.pos.x, sf_tr_camera.position.y-10, _set.pos.z);// y綁死

        // 建築物中心參考點
        sf_tr_set_pos_lock_y.position = lock_y_pos;
        sf_tr_set_pos_lock_y.LookAt(sf_tr_camera);
        Vector3 rot = new Vector3(
                  0
                , sf_tr_set_pos_lock_y.localRotation.eulerAngles.y
                , 0
            );

        sf_tr_set_pos_lock_y.localRotation = Quaternion.Euler(
                 rot
            );

        // dir
        //sf_obj_dir_target.transform.position = m_MyTools.CameraDistWorldPos(
        //          sf_tr_camera
        //        , lock_y_pos // y綁死
        //        , c_dist_dir_target
        //    );

        if (m_DirCtrl == null) m_DirCtrl = MyFinder.Instance.getDirCtrl();
        m_DirCtrl.setTarget(sf_obj_dir_target.transform);
        m_DirCtrl.setCheckingBool(true, GameFlowUnit.GemeFlowTypeEnum.tower_game);

        m_lvl = _set.Level;
        m_question_count = _set.questions;


        getTowerGameEnemyCtrl();

        m_TowerGameEnemyCtrl.reInit();
        //m_TowerGameEnemyCtrl.init(); // main init過一次

        // 打開地板 敵人 和題目?
        // 地板網格
        sf_anim_grid_ground.gameObject.SetActive(true);

        // 取得題目      
        m_en_quest_max_count = m_question_count;
        if (m_CSVReader == null) m_CSVReader = MyFinder.Instance.getCSVReader();        
        m_en_quest_list = m_CSVReader.getLvlQuest(_set.Level, m_en_quest_max_count, false);
        m_en_quest_cur_index = 0;

        // 秀題組
        StartCoroutine(showQuestSet(_set, 0.4f));

        //if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        getAudioCtrl().playMusic(AudioCtrl.MusicClipsEnum.tower_game_play);

        sf_obj_crack_alarm.SetActive(false);
        m_is_stop_call_enemy_footman = false;
        sf_obj_buildings.SetActive(true);

        // 敵人
        StartCoroutine(callEnemyFootman());


        // 背包
        getTowerGameBackageCtrl().init();

        // Skill
        getTowerGameSkillCtrl().init();

        // Player
        getPlayerCtrl().init();
        //for (int i = 0; i < sf_obj_enemy_arr.Length; i++)
        //{
        //    sf_obj_enemy_arr[i].SetActive(true);
        //}

        // tower
        //sf_obj_tower.SetActive(true);

        //showScoreEndGame(59);
    }

    TowerGameEnemyCtrl getTowerGameEnemyCtrl()
    {
        if (m_TowerGameEnemyCtrl == null) m_TowerGameEnemyCtrl = MyFinder.Instance.getTowerGameEnemyCtrl();
        return m_TowerGameEnemyCtrl;
    }


    TowerGameBackageCtrl getTowerGameBackageCtrl()
    {
        if (m_TowerGameBackageCtrl == null) m_TowerGameBackageCtrl = MyFinder.Instance.getTowerGameBackageCtrl();
        return m_TowerGameBackageCtrl;
    }

    TowerGameSkillCtrl getTowerGameSkillCtrl()
    {
        if (m_TowerGameSkillCtrl == null) m_TowerGameSkillCtrl = MyFinder.Instance.getTowerGameSkillCtrl();
        return m_TowerGameSkillCtrl;
    }


    // 招喚小兵
    bool m_is_stop_call_enemy_footman = false;
    IEnumerator callEnemyFootman()
    {
        do
        {
           
            int _count = UnityEngine.Random.Range(2, 4);
            getTowerGameEnemyCtrl().callEnemy(TowerGameEnemyCtrl.EnemyTypeEnum.footman, _count);
            yield return new WaitForSeconds(20);

            if (m_is_stop_call_enemy_footman)
            {
                yield break;
            }

        } while (!m_is_stop_call_enemy_footman);

        yield break;
    }

    // 秀題組
    IEnumerator showQuestSet(TowerGameSet _set, float _delay)
    {

        yield return new WaitForSeconds(_delay);
        if (m_is_clear|| is_stop_guest|| m_is_stop_call_enemy_footman)
        {
            yield break;
        }

        //Debug.LogError("m_is_clear=");
        sf_obj_crack_alarm.SetActive(true);
        //getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.Interface_Push_Button); // 題目的提示牌
        
        yield return new WaitForSeconds(2.5f);

        if (m_is_stop_call_enemy_footman) // 借用這個flag來停止
        {
            yield break;
        }



        //Debug.Log("++++++++++++++showQuestSet");
        
        //sf_obj_crack_alarm.SetActive(true);

        int max_qs_this_time = m_en_quest_max_count - m_en_quest_cur_index;
        if (max_qs_this_time >= c_max_quest)
        {
            max_qs_this_time = c_max_quest;
        }
        max_qs_this_time =  UnityEngine.Random.Range(2, max_qs_this_time);  //666
        //Debug.LogError("max_qs_this_time="+max_qs_this_time);

        int[] rand_index = new int[] { 0, 1, 2, 3, 4 }; // 現在是異界max=5個的情況下
        rand_index = MyFinder.Instance.getMyTools().ShuffleIntArr(rand_index);
        //m_question_pos_arr = MyFinder.Instance.getMyTools().ShuffleVector3Arr(m_question_pos_arr); // 打亂順序
        //m_question_pos_arr.Length;


        int i_local_set_index = 0;
        for (int i = 0; i < sf_quest_arr.Length; i++)
        {
            int index_shuffle = rand_index[i];
            //if (sf_quest_arr[i].p_is_in_use) continue;
            if (i >= max_qs_this_time) continue; //  一次組目前綁定三題

            //sf_quest_arr[i].p_is_in_use = true;
            sf_quest_arr[index_shuffle].gameObject.SetActive(true);

            m_en_quest_list[m_en_quest_cur_index].is_answered = false;
            m_en_quest_list[m_en_quest_cur_index].is_correct = false;
            m_en_quest_list[m_en_quest_cur_index].is_online = true;


            //Vector3 fix_pos = new Vector3(
            //        _set.pos.x
            //        , 0
            //        , _set.pos.z
            //    );
            //fix_pos = fix_pos.normalized;

            sf_quest_arr[index_shuffle].init(ref sf_tr_camera, m_en_quest_list[m_en_quest_cur_index], m_en_quest_cur_index);
            getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.Pickup_Soft, 0.4f); // 


            i_local_set_index ++;

            m_en_quest_cur_index++;
            if (m_en_quest_cur_index >= m_en_quest_max_count)
            {
                //m_is_finish_quests = true;
                break;
            }
        }

        //if (m_is_clear) clear();


        yield break;
    }

    bool m_is_clear = false;    
    public void clear()
    {
        // 題目
        for (int i = 0; i < sf_quest_arr.Length; i++)
        {
            //sf_quest_arr[i].p_is_in_use = false;
            sf_quest_arr[i].clear();
            sf_quest_arr[i].gameObject.SetActive(false);            
        }
        sf_obj_buildings.SetActive(false);
        m_is_stop_call_enemy_footman = true;
       
        getTowerGameEnemyCtrl().clear();
        getTowerGameBackageCtrl().clear();
        getTowerGameSkillCtrl().clear();
        getPlayerCtrl().clear();
        sf_img_score.gameObject.SetActive(false);

        sf_obj_skill_root.SetActive(false);
        sf_obj_backage_btn.SetActive(false);

        sf_anim_grid_ground.gameObject.SetActive(false);
        // 敵人
        //for (int i = 0; i < sf_obj_enemy_arr.Length; i++)
        //{
        //    sf_obj_enemy_arr[i].SetActive(false);
        //}

        // tower
        //sf_obj_tower.SetActive(false);
        m_is_clear = true;
    }

    //Vector3[] Shuffle(Vector3[] _arr)
    //{
    //    Vector3[] new_arr = _arr;
    //    //if (_arr == null) return;
    //    int len = new_arr.Length;//用變數記會快一點點點
    //    //Random rd = new Random();
    //    int r;//記下隨機產生的號碼
    //    Vector3 tmp;//暫存用
    //    for (int i = 0; i < len - 1; i++)
    //    {
    //        r = UnityEngine.Random.Range(i, len); //取亂數，範圍從自己到最後，決定要和哪個位置交換，因此也不用跑最後一圈了
    //        //r = rd.Next(i, len);
    //        if (i == r) continue;
    //        tmp = new_arr[i];
    //        new_arr[i] = new_arr[r];
    //        new_arr[r] = tmp;
    //    }

    //    return new_arr;
    //}

    public void reveiveAnswer(int _i_quest, int _i_ans, bool _is_correct)
    {       
        m_en_quest_list[_i_quest].is_answered = true;
        m_en_quest_list[_i_quest].is_online = false;
        m_en_quest_list[_i_quest].is_correct = _is_correct;

        int i_online = 0;
        int i_answered = 0;
        int i_correct = 0;
        for (int i = 0; i < m_en_quest_list.Count; i++)
        {
            if (m_en_quest_list[i].is_online) i_online++;
            if (m_en_quest_list[i].is_answered) i_answered++;
            if (m_en_quest_list[i].is_correct) i_correct++;            
        }

        // 答對幾題 給獎勵
        if (_is_correct)
        {
            gainAward(i_correct);
        }

        //MyFinder.Instance.getUIDebugCtrl().showDebugTxt(",總"+ m_en_quest_list.Count+ "答" + i_answered +",對"+ i_correct);
        // 答對80%開始冒煙
        float final_point = 100 * (float.Parse(i_correct.ToString()) / float.Parse(m_en_quest_list.Count.ToString()));
        if (final_point > 80)
        {
            sf_obj_enemy_castle_smoke.SetActive(true);
        }
        

        if (i_online == 0) // 全部答題完畢
        {
            
            if (i_answered == m_en_quest_list.Count)
            {
                
                
                m_final_point = Mathf.Round(final_point);

                m_i_correct = i_correct;
                //Debug.Log("-------------- 全部答題完畢, "+ m_final_point + "分 (" + m_i_correct + "/" + m_en_quest_list.Count + ")");
                showScoreEndGame();
                //sf_txt_header.text = final_point + "分 (" + i_correct_count + "/" + m_cur_quest_list.Count+")";
            }
            else // 開啟下一輪
            {
                StartCoroutine(showQuestSet(m_reveived_TowerGameSet, 10));
                //Debug.Log("****** 三題答完 開啟下一輪");
            }

        }


        //Debug.Log("****** 1題++: is_online:" + i_online + " ,i_answered=" + i_answered);

        //Debug.Log("****** 1題++");
    }


    // 答對多少題目 可以贏得獎勵
    void gainAward(int _count)
    {
        SkillUnit.type gain_type = SkillUnit.type.empty;
        if (_count == 1)
        {
            gain_type = SkillUnit.type.skill_fireball;            
        }   
        else if (_count % 4==0)
        {
            //int rand = UnityEngine.Random.Range(1, (int)SkillUnit.type.length);
            //gain_type = (SkillUnit.type)rand;
            int rand = UnityEngine.Random.Range(0,3);
            gain_type = SkillUnit.type.skill_fireball;
            if (rand==0) gain_type = SkillUnit.type.skill_heal;
        }

        //機率得到獎勵
        if (gain_type != SkillUnit.type.empty)
        {
            getTowerGameSkillCtrl().gainSkill(gain_type);
            string str_award = "[背包]獲得物品\n";
            switch (gain_type)
            {
                case SkillUnit.type.empty:
                    break;
                case SkillUnit.type.skill_fireball:
                    str_award += "隕石卷軸";
                    break;
                case SkillUnit.type.skill_heal:
                    str_award += "治療卷軸";
                    break;
                case SkillUnit.type.length:
                    break;
                default:
                    break;
            }

            //str_award = "獲得物品 ";
            getCoverCtrl().playAwardAlarm(str_award);
        }

    }


    bool is_stop_guest = false;
    int m_i_correct = 0;
    float m_final_point = 0;
    public void showScoreEndGame(int is_test = -1)
    {
        getTowerGameEnemyCtrl().endGameWorks();


        m_is_stop_call_enemy_footman = true;
        is_stop_guest = true;
        // 算分數

        bool is_dead = getPlayerCtrl().isDeadOREndGame();

        

        string str_point = "\n得到" + m_final_point + "分 (" + m_i_correct + "/" + m_en_quest_list.Count + ")";
        str_point += "\n(難度" + m_lvl + ") 擊殺" + getTowerGameEnemyCtrl().getKilledEnemyAmount().ToString()+"敵人";


        //if (is_test!=-1)
        //{
        //    m_final_point = 100;
        //    str_point = "\n 得到" + m_final_point + "分 (" + m_i_correct + "/" + m_en_quest_list.Count + ")";
        //}

        bool is_win = true;
        if (is_dead)
        {
            is_win = false;
            sf_txt_score.text = "挑戰失敗" + str_point;            
        }
        else if(m_final_point < 60)
        {
            sf_txt_score.text = "再接再厲" + str_point;
        }
        else if (m_final_point >= 60 && m_final_point < 80)
        {
            sf_txt_score.text = "成功" + str_point;
        }
        else if (m_final_point >= 80 && m_final_point < 90)
        {            
            sf_txt_score.text = "有點厲害" + str_point;
        }
        else if (m_final_point >= 90 && m_final_point < 100)
        {
            sf_txt_score.text = "非常厲害" + str_point;
        }
        else
        {
            sf_txt_score.text = "完美!" + str_point;
        }

        getAudioCtrl().playMusic(AudioCtrl.MusicClipsEnum.length);

        if (is_win)
        {
            getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.FA_Win_Jingle_Loop);
        }
        else
        {
            getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.lose_1_1);
        }


        StartCoroutine(_EndTowerGame(4.4f,0.6f,false));
    }

    void cleanTowerGame()
    {
        getAudioCtrl().playMusic(AudioCtrl.MusicClipsEnum.en_game_room);
    }


    public void restartAll()
    {

        StartCoroutine(_EndTowerGame(0, 0, true));

        getGameFlowCtrl().initGameFlow();
    }


    public IEnumerator _EndTowerGame(float _delay_a, float _delay_b, bool _is_restart_all)
    {
        Debug.Log("_EndTowerGame...等待5秒");
        getTowerGameEnemyCtrl().killAll();
        m_is_stop_call_enemy_footman = true;

        sf_obj_skill_root.gameObject.SetActive(false);
        sf_obj_backage_btn.gameObject.SetActive(false);

        
        if (!_is_restart_all)
        {
            sf_img_score.gameObject.SetActive(true); // 五秒後由下面的Coroutine關0.0
        }



        getTowerBottomBoard().setBackToClassBtnActive(false);
        getPlayerCtrl().setEndGame();

        for (int i = 0; i < sf_quest_arr.Length; i++)
        {
            sf_quest_arr[i].clear();
            sf_quest_arr[i].gameObject.SetActive(false);
        }

        yield return new WaitForSeconds(_delay_a);
        sf_obj_enemy_castle_crash_fx.SetActive(true);
        yield return new WaitForSeconds(_delay_b);

        sf_anim_grid_ground.gameObject.SetActive(false);
        sf_img_score.gameObject.SetActive(false);
        sf_obj_buildings.gameObject.SetActive(false);

        cleanTowerGame();

        if (_is_restart_all)
        {
            getGameFlowCtrl().initGameFlow();           
        }
        else
        {
            getGameFlowCtrl().toNextFlow();
        }


        m_demo_mode = MyFinder.Instance.getMain().p_demo_mode;
        if (m_demo_mode != Main.demoEnum.loop_tower_game)
        {
            getTowerBottomBoard().setBackToClassBtnActive(true);
        }
        
        
        //sf_obj_back_to_class.SetActive(true);
        Debug.Log("to next flow!!!!");

    }




    /*
    IEnumerator _setEndGame()
    {
        Debug.Log("setEndGame!!!!");
        yield return new WaitForSeconds(5);
        getGameFlowCtrl().toNextFlow();
        //sf_obj_back_to_class.SetActive(true);
        Debug.Log("to next flow!!!!");


    }     
         */

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        setEndGame();
    //    }
    //}





    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        return m_AudioCtrl;
    }

    CoverCtrl m_CoverCtrl;
    CoverCtrl getCoverCtrl()
    {
        if (m_CoverCtrl == null) m_CoverCtrl = MyFinder.Instance.getCoverCtrl();
        return m_CoverCtrl;
    }

    GameFlowCtrl m_GameFlowCtrl = null;
    GameFlowCtrl getGameFlowCtrl()
    {
        if (m_GameFlowCtrl == null) m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();
        return m_GameFlowCtrl;

    }



    TowerBottomBoard m_TowerBottomBoard = null;
    TowerBottomBoard getTowerBottomBoard()
    {
        if (m_TowerBottomBoard == null) m_TowerBottomBoard = MyFinder.Instance.getTowerBottomBoard();
        return m_TowerBottomBoard;

    }

    PlayerCtrl m_PlayerCtrl = null;
    PlayerCtrl getPlayerCtrl()
    {

        if (m_PlayerCtrl == null) m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        return m_PlayerCtrl;

    }


    private void Update()
    {

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    StartCoroutine(_EndTowerGame());
        //}


        //if (!MyFinder.Instance.getMain().is_debug) return;
        //if (Input.GetKeyDown(KeyCode.H))
        //{
        //    getTowerGameSkillCtrl().gainSkill(SkillUnit.type.skill_heal);
        //}

        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    getTowerGameSkillCtrl().gainSkill(SkillUnit.type.skill_fireball);
        //}

    }
}
