using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 天空上的圓球
/// </summary>
public class TowerQuest : MonoBehaviour, IDebugOnTrigger
{
    enum uiEnum
    {
        timer,
        quest,
        ans1,
        ans2,
        ans3,
        ans4,
        length
    }

    [Header("通天線")]
    [SerializeField] GameObject sf_obj_line;
    [Header("Canvas")]
    [SerializeField] GameObject sf_obj_canvas;
    [SerializeField] GameObject[] sf_obj_ui_arr;
    [Header("Event")]
    [SerializeField] TowerQuestEvent sf_event_timer;
    [SerializeField] TowerQuestEvent sf_event_spell;
    [SerializeField] TowerQuestEvent[] sf_quest_event_arr;
    [Header("Fx")]
    [SerializeField] GameObject sf_obj_fx;
    [SerializeField] GameObject sf_obj_correct_fx;
    [SerializeField] GameObject sf_obj_wrong_fx;
    [SerializeField] GameObject sf_obj_show_fx;
    [SerializeField] GameObject sf_obj_ball_fx;
    Transform m_camera = null;
    public bool p_is_in_use = false;
    public int p_i_quest_index = -1;

    const int c_max_time = 31; // 題目給的秒數
    const float c_dist = 85.0f;
    bool m_is_show_ui = false;
    Vector3 m_enter_scale = new Vector3(1.5f, 1.5f, 1.5f);
    Vector3 m_exit_scale = new Vector3(1, 1, 1);
    int m_i_correct = -1;
    int i_timer_answered_wait = 4;
    TowerGameCtrl m_TowerGameCtrl;
    public void init(ref Transform _tr_camera, EnglishWords _en_quest, int _question_index)
    {
        sf_obj_show_fx.SetActive(true);
        if (m_TowerGameCtrl == null) m_TowerGameCtrl = MyFinder.Instance.getTowerGameCtrl();

        m_is_answered = false;

        // close ui
        m_camera = _tr_camera;
        m_is_show_ui = false;
        for (int i = 0; i < (int)uiEnum.length; i++)
        {
            if (i == (int)uiEnum.timer) continue;
            sf_obj_ui_arr[i].SetActive(false);
        }

        sf_obj_fx.transform.localScale = m_exit_scale;
        p_is_in_use = true;
        //sf_obj_fx.gameObject.SetActive(true);

        //// pos dir 先亂寫 666
        //Vector3 new_pos = MyFinder.Instance.getMyTools().CameraDistWorldPos(m_camera, _pos, c_dist, false, 0);
        //new_pos = new_pos + _fix_dir;
        ////Debug.LogError("題目的 new pos: "+new_pos);
        //this.gameObject.transform.position = new_pos;
        this.gameObject.transform.LookAt(m_camera);

        // 秀題目
        sf_event_spell.sf_txt.text = _en_quest.spell;
        p_i_quest_index = _question_index;

        for (int i = 0; i < sf_quest_event_arr.Length; i++)
        {
            sf_quest_event_arr[i].p_quest_index = _question_index;
            sf_quest_event_arr[i].init();
        }

        // 隨機選一格放正確
        //System.Random rnd_A = new System.Random();
        m_i_correct = UnityEngine.Random.Range(0, 4);
        //m_i_correct = rnd_A.Next(0, 4);
        //Debug.Log("m_i_correct = "+ m_i_correct);

        // 正確答案 從三格有的選一個
        List<string> ans_list = new List<string>();
        ans_list.Add(_en_quest.chinese_01);
        if (_en_quest.chinese_02 != "" && !_en_quest.chinese_02.Contains("{#LVL_"))
        {
            ans_list.Add(_en_quest.chinese_02);
        }

        if (_en_quest.chinese_03 != "" && !_en_quest.chinese_02.Contains("{#LVL_") && !_en_quest.chinese_03.Contains("{#LVL_"))
        {
            ans_list.Add(_en_quest.chinese_03);
        }

        //System.Random rnd_B = new System.Random();
        //int i_B = rnd_B.Next(0, ans_list.Count);
        int i_B = UnityEngine.Random.Range(0, ans_list.Count);

        sf_quest_event_arr[m_i_correct].sf_txt.text = ans_list[i_B];
        _en_quest.show_ans = ans_list[i_B];

        int i_wrong = 0;
        for (int i = 0; i < 4; i++)
        {
            //sf_quest_event_arr[i].p_is_clicked = false;
            if (i == m_i_correct) continue;
            sf_quest_event_arr[i].sf_txt.text =_en_quest.str_wrong_list[i_wrong];
            sf_quest_event_arr[i].p_quest_index = _question_index;
            i_wrong++;
        }

        sf_obj_correct_fx.SetActive(false);
        sf_obj_wrong_fx.SetActive(false);

        i_timer = c_max_time;
        clear();
        InvokeRepeating("playTimer",0,1);
        sf_obj_line.SetActive(true);
    }

    int i_timer = -1;
    bool m_is_answered = false;
    void playTimer()
    {
        i_timer--;
        sf_event_timer.sf_txt.text = i_timer + "秒";
        if (i_timer == 0)
        {
            if (!m_is_answered)
            {
                //m_is_answered = true;
                ////showFixedAns();
                //i_timer = i_timer_answered_wait;
                childClickAnswer(p_i_quest_index, 5); // 給一個絕對錯的答案 
                for (int i = 0; i < (int)uiEnum.length; i++) // 打開面板123
                {
                    if (i == (int)uiEnum.timer) continue;
                    sf_obj_ui_arr[i].SetActive(true);
                }
            }
            else
            {
                recycle();
                clear();
            }
        }        
    }


    public void clear()
    {
        CancelInvoke("playTimer");
    }



    void recycle()
    {
        p_is_in_use = false;
        this.gameObject.SetActive(false);
        //for (int i = 0; i < (int)uiEnum.length; i++)
        //{
        //    if (i == (int)uiEnum.timer) continue;
        //    sf_obj_ui_arr[i].SetActive(false);
        //}
    }

    public void childClickAnswer(int _i_quest, int _i_ans)
    {
        Debug.Log("childClickAnswer: _i_quest:" + _i_quest+ " ,_i_ans="+ _i_ans);
        if (m_is_answered) return;
        m_is_answered = true;

        // 按下答案後的處理

        bool is_correct = false;
        if (m_i_correct == _i_ans) is_correct = true;
        
        for (int i = 0; i < sf_quest_event_arr.Length; i++)
        {
            sf_quest_event_arr[i].p_is_clicked = true;
            sf_quest_event_arr[i].sf_txt.color = Color.black;
            if (i == m_i_correct)
            {
                if (is_correct)
                {
                    sf_quest_event_arr[i].sf_txt.color = Color.green;
                    sf_obj_correct_fx.SetActive(true);
                    getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.Ice_Reflect);
                    // award
                    getTowerGameSkillCtrl().playAward(sf_quest_event_arr[i].sf_parent_script.sf_obj_ball_fx.transform.position);
                }
                else
                {
                    sf_quest_event_arr[i].sf_txt.color = new Vector4(1, 0, 0.9222755f, 1);
                    sf_obj_wrong_fx.SetActive(true);
                    getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.Negative);
                }
            }
        }

        i_timer = i_timer_answered_wait;
        m_is_answered = true;

        // 回報 TowerGameCtrl
        m_TowerGameCtrl.reveiveAnswer(_i_quest, _i_ans, is_correct);
    }

    //void showFixedAns()
    //{
        
    //}


    public void onClick()
    {
        if (m_is_show_ui) return;
        m_is_show_ui = true;

        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.CGM2_Star_Rating_1);

        //sf_obj_canvas.gameObject.SetActive(true);
        for (int i = 0; i < (int)uiEnum.length; i++)
        {
            if (i == (int)uiEnum.timer) continue;
            sf_obj_ui_arr[i].SetActive(true);
        }

        sf_obj_line.SetActive(false);
    }

    public void onEnter()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.CR2_Cute_UI_1);
        sf_obj_fx.transform.localScale = m_enter_scale;

    }

    public void onExit()
    {
        sf_obj_fx.transform.localScale = m_exit_scale;
    }


    void IDebugOnTrigger.OnDebugClick()
    {
        onClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }


    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    TowerGameSkillCtrl m_TowerGameSkillCtrl = null;
    TowerGameSkillCtrl getTowerGameSkillCtrl()
    {
        if (m_TowerGameSkillCtrl == null)
        {
            m_TowerGameSkillCtrl = MyFinder.Instance.getTowerGameSkillCtrl();
        }

        return m_TowerGameSkillCtrl;
    }

}
