using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackageGridUnit : MonoBehaviour, IDebugOnTrigger
{
    public int p_index = -1;
    [SerializeField] Image sf_img_selected;
    [SerializeField] Image sf_img_item;
    [SerializeField] public Text sf_txt;


    public void onClick()
    {
        getTowerGameBackageCtrl();

    }

    public void onEnter()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.CR2_Cute_UI_1);
        sf_img_selected.gameObject.SetActive(true);

    }

    public void onExit()
    {
        sf_img_selected.gameObject.SetActive(false);
    }


    void IDebugOnTrigger.OnDebugClick()
    {
        onClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {

        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }


    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    TowerGameBackageCtrl m_TowerGameBackageCtrl = null;
    TowerGameBackageCtrl getTowerGameBackageCtrl()
    {
        if (m_TowerGameBackageCtrl == null)
        {
            m_TowerGameBackageCtrl = MyFinder.Instance.getTowerGameBackageCtrl();
        }

        return m_TowerGameBackageCtrl;
    }
    
}
