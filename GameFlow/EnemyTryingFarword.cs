using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy前方綁一個 Looping 上下擺動的偵測trigger 
/// 檢查前方一個移動空間單位內 有沒有enemy擋在前面
/// </summary>
public class EnemyTryingFarword : MonoBehaviour
{
    [Header("本體")]
    [SerializeField] EnemyUnit sf_enemy_unit;

    bool m_is_obs_this_time = false;
    public void beforeCheckForward()
    {
        m_is_obs_this_time = false;
    }

    public void afterCheckForward()
    {
        // 告知本體 前方檢查結果
        sf_enemy_unit.setTryingForwardBool(m_is_obs_this_time);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            m_is_obs_this_time = true;
        }

    }
}
