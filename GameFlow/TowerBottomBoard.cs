using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBottomBoard : MonoBehaviour
{
    [Header("Pos")]
    [SerializeField] float sf_forward_dist;
    [SerializeField] float sf_y_lock;
    [Header("Camera")]
    [SerializeField] Transform sf_tr_camera;
    [Header("BoardRoot")]
    [SerializeField] Transform sf_tr_board;
    [Header("backToClass")]
    [SerializeField] Transform sf_tr_back_to_class;
    Main.demoEnum m_demo_mode;

    public void init()
    {
        sf_tr_board.gameObject.SetActive(true);
        m_is_update = true;

        m_demo_mode = MyFinder.Instance.getMain().p_demo_mode;

        if (m_demo_mode == Main.demoEnum.loop_tower_game)
        {
            sf_tr_back_to_class.gameObject.SetActive(false);
        }
    }


    public void setBackToClassBtnActive(bool _is_active)
    {
        sf_tr_back_to_class.gameObject.SetActive(_is_active);
    }

    public void exit()
    {
        sf_tr_board.gameObject.SetActive(false);
        m_is_update = false;
    }


    bool m_is_update = false;
    Vector3 m_pos = new Vector3();
    public void myUpdate()
    {
        if (!m_is_update) return;
        sf_tr_board.LookAt(sf_tr_camera);
        m_pos = sf_tr_camera.position + sf_tr_camera.forward * sf_forward_dist;
        sf_tr_board.position = new Vector3(
                  m_pos.x
                , sf_y_lock
                , m_pos.z
            );
        //sf_tr_board.position = 
       
    }
}
