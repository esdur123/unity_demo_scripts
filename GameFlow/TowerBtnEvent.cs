using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerBtnEvent : MonoBehaviour, IDebugOnTrigger
{
    [Header("Type")]
    [SerializeField] btnTypeEnum sf_type;
    [Header("Text")]
    [SerializeField] public Text sf_txt;
    //[Header("TowerQuest")]
    //[SerializeField] public TowerQuest sf_parent_script;
    //[Header("iQuest")]
    //public int p_quest_index = -1;
    //[Header("iAns")]
    //public int p_ans_index = -1;
    //public bool p_is_clicked = false;

    //Vector4 c_color_original = new Vector4(0, 0, 0, 0.7f); // 背景原始顏色
    //Vector4 c_color_enter = new Vector4(1, 0.2964f, 0f, 0.7f); // 背景hover 顏色
    Vector4 c_color_enter_txt = new Vector4(1, 0.8855017f, 0, 1); // 文字hover 顏色

    //public void init()
    //{
    //    p_is_clicked = false;
    //    sf_txt.color = Color.white; ;
    //}

    public enum btnTypeEnum
    {
        restart_all,
        switch_360_image,
        length
    }

    public void onEnter()
    {
        //if (p_is_clicked) return;
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
        sf_txt.color = c_color_enter_txt;
    }

    public void onExit()
    {
        //if (p_is_clicked) return;
        sf_txt.color = Color.white;
    }

    public void onClick()
    {
        switch (sf_type)
        {
            case btnTypeEnum.restart_all:
                getTowerGameCtrl().restartAll();
                break;
            case btnTypeEnum.switch_360_image:
                getPortalCtrl().switch360Image();
                break;
            case btnTypeEnum.length:
                break;
            default:
                break;
        }
        
    }

    


    void IDebugOnTrigger.OnDebugClick()
    {
        onClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    GameFlowCtrl m_GameFlowCtrl = null;
    GameFlowCtrl getGameFlowCtrl()
    {
        if (m_GameFlowCtrl == null)
        {
            m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();
        }

        return m_GameFlowCtrl;
    }

    TowerGameCtrl m_TowerGameCtrl = null;
    TowerGameCtrl getTowerGameCtrl()
    {
        if (m_TowerGameCtrl == null)
        {
            m_TowerGameCtrl = MyFinder.Instance.getTowerGameCtrl();
        }

        return m_TowerGameCtrl;
    }

    PortalCtrl m_PortalCtrll = null;
    PortalCtrl getPortalCtrl()
    {
        if (m_PortalCtrll == null)
        {
            m_PortalCtrll = MyFinder.Instance.getPortalCtrl();
        }

        return m_PortalCtrll;
    }

}
