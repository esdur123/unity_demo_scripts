using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUnit : MonoBehaviour, IDebugOnTrigger
{

    [Header("Anim")]
    [SerializeField] Animator sf_anim;
    [Header("txt")]
    [SerializeField] Text sf_txt_count;
    [Header("Image")]
    [SerializeField] Image sf_img;
    [Header("cover")]
    [SerializeField] Image sf_img_cover;
    [Header("selected img")]
    [SerializeField] Image sf_img_selected;

    public int p_index;
    public type m_type = type.length;
    public enum type
    {
        empty,
        skill_fireball,
        skill_heal,
        //item_weapon,
        //item_glove,
        //item_armor,
        length
    }

    public void setEnpty()
    {
        m_type = type.empty;

    }

    public void onEnter()
    {
        if (m_type == type.empty) return;
        if (sf_anim.enabled) return;
        sf_img_selected.gameObject.SetActive(true);
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
    }

    public void onExit()
    {
        if (sf_anim.enabled) return;
        sf_img_selected.gameObject.SetActive(false);
    }

    public void onClick()
    {
        if (m_type== type.empty) return;
        if (getPlayerCtrl().isDeadOREndGame()) return;
        if (sf_anim.enabled) return;
        if (m_cur_skill_count == 0) return;

        addSkillCount(m_type , - 1);
        getTowerGameSkillCtrl().playSkill(m_type);
        sf_img_selected.gameObject.SetActive(false);
        sf_anim.enabled = true;        
    }

    void IDebugOnTrigger.OnDebugClick()
    {
        onClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }

    //const int c_init_skill_count = 0;
    public int m_cur_skill_count = 0;
    public void init()
    {
        initSkill();
    }

    public void initSkill()
    {
        m_type = type.empty;
        sf_anim.enabled = false;
        
        sf_img_cover.fillAmount = 0;              
        sf_img_selected.gameObject.SetActive(false);

        sf_img.color = new Vector4(1,1,1,0);

        sf_img_cover.fillAmount = 0;
        //addSkillCount(0);
        m_cur_skill_count = 0;
        sf_txt_count.text = "";
        //if (sf_txt_count.text == "0") sf_txt_count.text = "";        
    }

    public void showSkill(type _type, int _count, Sprite _sprite)
    {
        sf_anim.enabled = false;

        sf_img_cover.fillAmount = 0;
        sf_img_selected.gameObject.SetActive(false);

        sf_img.color = new Vector4(1, 1, 1, 1);
        sf_img.sprite = _sprite;

        //addSkillCount(0, c_init_skill_count);
        m_cur_skill_count = _count;
        sf_txt_count.text = _count.ToString();

        m_type = _type;


        //if (sf_txt_count.text == "0") sf_txt_count.text = "";
    }

    //public void addSkillCount(type _type, int _count)
    //{
    //    if (m_type != _type) return;

    //    m_cur_skill_count += _count;
    //    sf_txt_count.text = m_cur_skill_count.ToString();
    //}

    public void addSkillCount(type _type, int _count)
    {
        if (m_type != _type) return;

        //if (_force_count != -1)
        //{
        //    m_cur_skill_count = _force_count;
        //    sf_txt_count.text = _force_count.ToString();
        //    return;
        //}
        //Debug.LogError("m_cur_skill_cout=" + m_cur_skill_count);
        m_cur_skill_count += _count;
        if (m_cur_skill_count <= 0)
        {
            m_cur_skill_count = 0;
        }
        sf_txt_count.text = m_cur_skill_count.ToString();


        // �^�� skill ctrl�� m_cur_skill_list
        getTowerGameSkillCtrl().m_cur_skill_list[p_index].m_cur_skill_count = m_cur_skill_count;
        if (m_cur_skill_count == 0)
        {
            getTowerGameSkillCtrl().m_cur_skill_list.RemoveAt(p_index);
        }
        //Debug.LogError("m_cur_skill_cout="+ m_cur_skill_count);
        //if (m_cur_skill_count <= 0)
        //{
        //    initSkill();
        //}
        //    m_cur_skill_count = 0;
        //sf_txt_count.text = m_cur_skill_count.ToString();


    }




    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    public void afterAnimPlayFillAnomut()
    {
        sf_anim.enabled = false;

        if (m_cur_skill_count <= 0)
        {
            initSkill();
        }
    }


    TowerGameSkillCtrl m_TowerGameSkillCtrl = null;
    TowerGameSkillCtrl getTowerGameSkillCtrl()
    {
        if (m_TowerGameSkillCtrl == null) m_TowerGameSkillCtrl = MyFinder.Instance.getTowerGameSkillCtrl();
        return m_TowerGameSkillCtrl;
    }

    PlayerCtrl m_PlayerCtrl = null;
    PlayerCtrl getPlayerCtrl()
    {
        if (m_PlayerCtrl == null) m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        return m_PlayerCtrl;
    }
}
