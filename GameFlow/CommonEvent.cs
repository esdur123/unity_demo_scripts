using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonEvent : MonoBehaviour
{
    [Header("Anim")]
    [SerializeField] Animator sf_anim;

    public void closeObj()
    {
        this.gameObject.SetActive(false);
    }

    Animator m_anim;
    public void disableAnim()
    {
        if (sf_anim == null) sf_anim = this.gameObject.GetComponent<Animator>();
        if (sf_anim == null) return;
        sf_anim.enabled = false;
    }

    public void toNextFlow()
    {
        this.gameObject.SetActive(false);            
        MyFinder.Instance.getGameFlowCtrl().toNextFlow();
    }

    //public void disableAnimator()
    //{
    //    sf_anim.enabled = false;
    //}
}
