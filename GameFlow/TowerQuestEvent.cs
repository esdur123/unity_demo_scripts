using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerQuestEvent : MonoBehaviour, IDebugOnTrigger
{
    [Header("Text")]
    [SerializeField] public Text sf_txt;
    [Header("TowerQuest")]
    [SerializeField] public TowerQuest sf_parent_script;    
    [Header("iQuest")]
    public int p_quest_index = -1;
    [Header("iAns")]
    public int p_ans_index = -1;
    public bool p_is_clicked = false;

    Vector4 c_color_original = new Vector4(0, 0, 0, 0.7f); // 背景原始顏色
    Vector4 c_color_enter = new Vector4(1, 0.2964f, 0f, 0.7f); // 背景hover 顏色
    Vector4 c_color_enter_txt = new Vector4(1, 0.8855017f, 0, 1); // 文字hover 顏色

    public void init()
    {
        p_is_clicked = false;
        sf_txt.color = Color.white; ;
    }

    public void onEnter()
    {
        if (p_is_clicked) return;
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
        sf_txt.color = c_color_enter_txt;
    }

    public void onExit()
    {
        if (p_is_clicked) return;
        sf_txt.color = Color.white; ;
    }

    public void onClick()
    {
        if (getPlayerCtrl().isDeadOREndGame()) return;
        if (p_is_clicked) return;
        p_is_clicked = true;
        sf_parent_script.childClickAnswer(p_quest_index, p_ans_index);
    }

    void IDebugOnTrigger.OnDebugClick()
    {
        onClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    PlayerCtrl m_PlayerCtrl = null;
    PlayerCtrl getPlayerCtrl()
    {
        if (m_PlayerCtrl == null)
        {
            m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        }

        return m_PlayerCtrl;
    }

    
}
