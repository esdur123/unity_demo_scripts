using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerGameSkillCtrl : MonoBehaviour
{

    [Header("Skill")]
    [SerializeField] SkillUnit[] sf_unit_skill;
    [Header("FireRain")]
    [SerializeField] GameObject sf_obj_firerain;
    [Header("Heal")]
    [SerializeField] GameObject sf_obj_heal;
    [Header("Sprite")]
    [SerializeField] Sprite[] sf_sprite; // 順序請對應skill type

    public void init()
    {
        //Debug.LogError("init skills");
        m_cur_skill_list.Clear();
        sf_obj_firerain.SetActive(false);
        for (int i = 0; i < sf_unit_skill.Length; i++)
        {
            sf_unit_skill[i].init();
        }

        
        gainSkill(SkillUnit.type.skill_heal);
        //gainSkill(SkillUnit.type.skill_heal);
        //gainSkill(SkillUnit.type.skill_fireball);
        //gainSkill(SkillUnit.type.skill_fireball);
        //gainSkill(SkillUnit.type.skill_fireball);
    }

    public List<SkillUnit> m_cur_skill_list = new List<SkillUnit>();
    public void gainSkill(SkillUnit.type _type)
    {
        // 如果已經存在此skill
        bool is_found_same = false;
        for (int i = 0; i < m_cur_skill_list.Count; i++)
        {
            
            if (sf_unit_skill[i].m_type != _type) continue;
            //Debug.LogError("1111111 存在此skill" + _type);
            m_cur_skill_list[i].m_cur_skill_count++;
            is_found_same = true;
            break;
            //break;
        }

        // 如不存在此skill
        if (!is_found_same)
        {
            //Debug.LogError("1111111 如不存在此skill" + _type);
            SkillUnit unit = new SkillUnit();
            unit.m_type = _type;
            unit.m_cur_skill_count = 1;
            m_cur_skill_list.Add(unit);
        }

        //for (int i = 0; i < m_cur_skill_list.Count; i++)
        //{
        //    Debug.Log("@@: ["+i+"] "+m_cur_skill_list[i].m_type+", count="+ m_cur_skill_list[i].m_cur_skill_count);
        //}
   


        // 刷新視覺,  用 sf_unit_skill(視覺)  對比 m_cur_skill_list(內存)
        for (int i = 0; i < sf_unit_skill.Length; i++)
        {
            //if (i >= m_cur_skill_list.Count) continue;
            
            //    continue;
            //// 內存有東西
            if (i < m_cur_skill_list.Count)
            {
                // 外觀原本是空的
                if (sf_unit_skill[i].m_type == SkillUnit.type.empty)
                {
                    //Debug.LogError("["+i+"] 外觀原本是空的 type"+ _type+"++++");
                    sf_unit_skill[i].showSkill(
                              _type
                            , 1
                            , getSkillSprite(_type)
                        );

                }
                else // 外觀原本就有東西
                {
                    if (sf_unit_skill[i].m_type == _type) // 找相同的
                    {
                        sf_unit_skill[i].addSkillCount(_type, 1);
                        //Debug.LogError("[" + i + "] 外觀原本就有東西 type" + _type + ", OOOO");
                    }
                    
                    //sf_unit_skill[i].m_cur_skill_count++;
                }                
            }
            else // 內存沒東西 視覺要刷空
            {
                //Debug.LogError("[" + i + "] 內存沒東西 視覺要刷空 type" + _type);
                sf_unit_skill[i].init();
            }
        }




    }


    Sprite getSkillSprite(SkillUnit.type _type)
    {
        return sf_sprite[(int)_type];
    }

    public void clear()
    {
        sf_obj_heal.SetActive(false);
        sf_obj_firerain.SetActive(false);
    }

    public void playSkill(SkillUnit.type _type)
    {
        switch (_type)
        {
            case SkillUnit.type.skill_fireball:
                playFireBall();
                break;
            case SkillUnit.type.skill_heal:
                playHeal();
                break;
            default:
                break;
        }
    }


    void playFireBall()
    {
        // 畫面        
        sf_obj_firerain.SetActive(true);

        // 傷害
        getTowerGameEnemyCtrl().aoeFireball();
    }


    void playHeal()
    {

        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.Positive_Holy2);
        // 666
        sf_obj_heal.SetActive(true);

        getPlayerCtrl().skillHeal();
        //Debug.LogError("play heal");
        //// 畫面        
        //sf_obj_firerain.SetActive(true);

        //// 傷害
        //getTowerGameEnemyCtrl().aoeFireball();
    }


    public void playAward(Vector3 _quest_pos)
    {
        // 目前只有一招XD
        StartCoroutine(playIceProjectile(_quest_pos));
    }


    IEnumerator playIceProjectile(Vector3 _quest_pos)
    {
        //List<Vector3> pos_list = new List<Vector3>();
        //yield return new WaitForSeconds(4);

        // 先看場上有多少敵人
        List<EnemyUnit> cur_enemy_list = getTowerGameEnemyCtrl().getCurEnemyList();
        for (int i = 0; i < cur_enemy_list.Count; i++)
        {
            getFxCtrl().fireBullet(
                   FxCtrl.enumTypeBullet.dark_blue_ball
                 , _quest_pos
                 , cur_enemy_list[i].p_tr_hit_point.position
                 , false
                 , false
              );
        }

        // 產生相對的冰球 fireBullet
        yield break;

    }


    TowerGameEnemyCtrl m_TowerGameEnemyCtrl = null;
    TowerGameEnemyCtrl getTowerGameEnemyCtrl()
    {
        if (m_TowerGameEnemyCtrl == null) m_TowerGameEnemyCtrl = MyFinder.Instance.getTowerGameEnemyCtrl();
        return m_TowerGameEnemyCtrl;
    }

    PlayerCtrl m_PlayerCtrl = null;
    PlayerCtrl getPlayerCtrl()
    {
        if (m_PlayerCtrl == null) m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        return m_PlayerCtrl;
    }

    FxCtrl m_fxCtrl = null;
    FxCtrl getFxCtrl()
    {
        if (m_fxCtrl == null)
        {
            m_fxCtrl = MyFinder.Instance.getFxCtrl();
        }
        return m_fxCtrl;
    }

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }
}
