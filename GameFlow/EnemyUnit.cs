using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUnit : MonoBehaviour, IDebugOnTrigger
{
    [Header("Anim")]
    public Animator m_Animator;
    string m_ClipName;
    AnimatorClipInfo[] m_CurrentClipInfo;
    float m_CurrentClipLength;

    [Header("Others")]
    public bool is_in_use = false;
    public int index = -1;
    public float cur_hp = 0;
    public float max_hp = 0;
    public Vector3 target_pos = new Vector3();
    public SelectedEffectOutline.NoriSelectEffectOutline select_script = null;
    Rigidbody p_rigi = null;
    public Collider col = null;
    public TowerGameEnemyCtrl.EnemyTypeEnum type_enemy = TowerGameEnemyCtrl.EnemyTypeEnum.length;
    public GameObject obj = null;
    //public bool is_update = false;
    public SelectedEffectOutline.NoriSelectEffectOutline p_select_effect = null;
    TowerGameEnemyCtrl m_TowerGameEnemyCtrl = null;
    AudioCtrl m_AudioCtrl = null;
    public Slider p_slider = null;
    public Transform p_tr_hit_point = null;
    PlayerCtrl m_PlayerCtrl;
    float m_move_speed = 10;
    [SerializeField] Text sf_debug_txt;
    CoverCtrl m_CoverCtrl = null;
    [SerializeField] GameObject sf_obj_show_hide_fx;
    [Header("前方空間測試")]
    [SerializeField] GameObject sf_obj_col_forward;
    public void init()
    {
        p_rigi = gameObject.GetComponent<Rigidbody>();
        m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        m_CoverCtrl = MyFinder.Instance.getCoverCtrl();
        m_TowerGameEnemyCtrl = MyFinder.Instance.getTowerGameEnemyCtrl();
        //rigi.velocity = new Vector3(0,0,-2);
        m_Animator.Play(fsmState.Idle.ToString());
        //m_Animator.SetBool(animEvents.triggerCloseToHero.ToString(), false);
        col.enabled = true;
        m_Animator.enabled = true;
        cur_hp = max_hp;
        m_is_close_to_player = false;
        setSlider();
        m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
        //m_trigger_enemy_list.Clear();
        sf_debug_txt.text = "0";
        sf_obj_show_hide_fx.SetActive(true);

        m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.FA_Cute_Jump,0.5f);

        m_is_try_forward_found_enemy = false;
        //sf_obj_col_forward.SetActive(false);
        //m_is_trying_forward = false;


        //Debug.LogError("start "+index.ToString());
        //initAnim();
    }

    public void recycle()
    {
        sf_obj_show_hide_fx.SetActive(false);
        m_is_close_to_player = false;
        m_is_dead = false;
        col.enabled = false;
        m_Animator.enabled = false;
        setRigiZero();
        p_slider.gameObject.SetActive(true);
        m_TowerGameEnemyCtrl.recycle(index);
    }

    public bool getIsDead()
    {
        return m_is_dead;
    }


    public void onEnter()
    {
        //Debug.Log("+++onEnter");
        p_select_effect.onEnter();
        m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.woosh_1_2);
    }

    public void onExit()
    {
        //Debug.Log("+++onExit");
        p_select_effect.onExit();
    }

    FxCtrl m_fxCtrl = null;
    FxCtrl getFxCtrl()
    {
        if (m_fxCtrl == null)
        {
            m_fxCtrl = MyFinder.Instance.getFxCtrl();
        }
        return m_fxCtrl;
    }

    PrimaryButtonWatcher m_PrimaryButtonWatcher = null;
    PrimaryButtonWatcher getPrimaryButtonWatcher()
    {
        if (m_PrimaryButtonWatcher == null)
        {
            m_PrimaryButtonWatcher = MyFinder.Instance.getPrimaryButtonWatcher();
        }
        return m_PrimaryButtonWatcher;
    }

    public void onClick()
    {
        // MyFinder.Instance.getUIDebugCtrl().showDebugTxt("onClick 0");

        if (m_PlayerCtrl.isDeadOREndGame()) return;

        getFxCtrl().fireBullet(
               FxCtrl.enumTypeBullet.redProject01
             , getPrimaryButtonWatcher().getCilckHandPoint()
             , p_tr_hit_point.position
             , false
             , true
          );
       // MyFinder.Instance.getUIDebugCtrl().showDebugTxt("onClick 1");
    }

    void IDebugOnTrigger.OnDebugClick()
    {
        onClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }


    //LayerMask mask;// = 1 << LayerMask.NameToLayer("Bullet");
    void OnCollisionEnter(Collision collision)
    {
        // 目前跟子彈用Trigger
        return;
    }


    void OnTriggerExit(Collider other)
    {
        //if (other.tag == "enemy")
        //{
        //    int i_enemy = other.gameObject.GetComponent<EnemyUnit>().index;
        //    //m_trigger_enemy_list.Remove(i_enemy);
        //    removeFromTriggerEnemyList(i_enemy);
        //    //m_i_cur_trigger_enemy--;
        //    sf_debug_txt.text = m_trigger_enemy_list.Count.ToString();
        //    //Debug.LogError("--m_i_cur_trigger_enemy=" + m_trigger_enemy_list.Count.ToString() + ", " + this.gameObject.name);
        //}
    }


    //public void removeFromTriggerEnemyList(int _index)
    //{
    //    m_trigger_enemy_list.Remove(_index);
    //}



    bool m_is_dead = false;
    //List<int> m_trigger_enemy_list = new List<int>();
    void OnTriggerEnter(Collider other) 
    {
        // 目前只有bullet 和警戒線(enemy) 會進來
        //Debug.LogError(other.gameObject.name);
        if (m_is_dead) return;
        if (other.tag == "attackLine")
        {
            m_is_close_to_player = true;            
            return;
        }

        //if (other.tag == "enemy")
        //{
        //    int i_enemy = other.gameObject.GetComponent<EnemyUnit>().index;
        //    m_trigger_enemy_list.Add(i_enemy);
        //    sf_debug_txt.text = m_trigger_enemy_list.Count.ToString();
        //    //Debug.LogError("++m_i_cur_trigger_enemy=" + m_i_cur_trigger_enemy + ", " + this.gameObject.name);
        //}

        if (other.tag == "bullet")
        {
            m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.FA_Funny_Impact_1_1); //Pauze_Game_Arcade FA_Funny_Impact_1_3

            FxMover script = other.gameObject.GetComponent<FxMover>();
            if (script == null) return;

            m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
            float dmg = m_PlayerCtrl.getBulletDmg(type_enemy, script.type_bullet);
            
            setRigiZero();

            m_is_dead = receiveDamage(dmg); // 傷害

            if (!m_is_dead)
            {
                m_Animator.SetTrigger(setTrigger.triggerHit.ToString());
            }
            else
            {
                //m_TowerGameEnemyCtrl.addKilledEnemy(1);
                sf_obj_show_hide_fx.SetActive(true);
                triggerDead();
                //m_Animator.SetTrigger(setTrigger.triggerDead.ToString());
            }
        }
    }

    public void triggerHit()
    {
        if (!m_is_dead)
        {                       
            m_Animator.SetTrigger(setTrigger.triggerHit.ToString());
            m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
            setRigiZero();
        }
    }

    public void triggerDead()
    {
        m_TowerGameEnemyCtrl.addKilledEnemy(1);
        m_Animator.SetTrigger(setTrigger.triggerDead.ToString());
    }


    public bool receiveDamage(float _dmg)
    {
        
        bool is_dead = false;
        cur_hp -= _dmg;
        if (cur_hp < 0) cur_hp = 0;
        setSlider();

        //Debug.LogError("max_hp=" + max_hp + ",cur_hp=" + cur_hp + ", _dmg=" + _dmg);


        if (cur_hp == 0)
        {
            is_dead = true;
        }

        return is_dead;
    }

    void setSlider()
    {
        
        p_slider.value = cur_hp / max_hp;
        if (cur_hp == 0) p_slider.gameObject.SetActive(false);
    }

    public void myUpdate()
    {
        //updateFSM();
    }


    //public enum animEvents
    //{
    //    triggerHit,
    //    length
    //}

    public enum fsmState
    {
        Idle,
        Walk,
        Attack01,
        Victory,
        GetHit,
        Death,
        length
    }

    //fsmState m_cur_fsm_state = fsmState.length;
    //public void changeFSMState(fsmState _to_state)
    //{

    //}

    string getCurUnityStateName()
    {
        string state_name = "";
        m_CurrentClipInfo = this.m_Animator.GetCurrentAnimatorClipInfo(0);
        state_name = m_CurrentClipInfo[0].clip.name;
        Debug.Log("[" + index + "] 現在動作" + state_name);
        return state_name;
    }

    public void eventDamage()
    {
        m_PlayerCtrl.receiveDamage(TowerGameEnemyCtrl.EnemyTypeEnum.footman);
        //m_CoverCtrl.playBloody();
        //Debug.LogError("eventDamage");
    }

    // 動作Event事件
    public enum setInt
    {
        nextInt,
        length
    }

    public enum setTrigger
    {
        triggerHit,
        triggerDead,
        length
    }

    public enum nextInt
    {
        idle_to_walk,
        idle_to_victory,
        idle_to_attack,
        walk_to_victory,
        walk_to_idle,
        attack_to_victory,
        victory_to_attack,
        victory_to_idle,
        length
    }

    void setRigiZero()
    {
        p_rigi.velocity = Vector3.zero;
        p_rigi.angularVelocity = Vector3.zero;
    }

    //bool m_is_trying_forward = false;
    //void tryingForwardCol()
    //{
    //    if (m_is_trying_forward) return;
    //    m_is_trying_forward = true;

    //    sf_obj_col_forward.SetActive(true);
    //}

    public void setTryingForwardBool(bool _is_forward_enemy)
    {
        m_is_try_forward_found_enemy = _is_forward_enemy;
    }

    bool m_is_close_to_player = false;
    const float c_walk_speed = 5;
    bool m_is_try_forward_found_enemy = false;
    public void IdleAfterEvent()
    {        
        m_Animator.SetInteger(setInt.nextInt.ToString(), -1);

        if (!m_is_close_to_player)
        {
            // 跟enemy黏太近  666
            if (m_is_try_forward_found_enemy)// (m_trigger_enemy_list.Count > 0)
            {
                m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.idle_to_victory);
                return;
            }

            m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.idle_to_walk);
            p_rigi.velocity = this.gameObject.transform.forward * c_walk_speed;
        }
        else
        {
            // 靠近玩家            
            setRigiZero();
            int i_rand = UnityEngine.Random.Range(0, 3);
            if (i_rand == 0)
            {
                m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.idle_to_victory);
            }
            else
            {
                m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.idle_to_attack);
            }
        }

        // int i_rand = UnityEngine.Random.Range(0, );
    }

    public void WalkAfterEvent()
    {
        m_Animator.SetInteger(setInt.nextInt.ToString(), -1);

        // 跟enemy黏太近 666
        if (m_is_try_forward_found_enemy)// (m_trigger_enemy_list.Count > 0)
        {
            m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.walk_to_victory);
            setRigiZero();
            return;
        }

        if (m_is_close_to_player)
        {
            m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.walk_to_idle);
            setRigiZero();
            return;
        }

        int i_rand = UnityEngine.Random.Range(0, 10);
        if (i_rand > 8)
        {            
            m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.walk_to_victory);
            setRigiZero();
            //Debug.LogError("+++++++++ walk_to_victory,v= " + p_rigi.velocity);
        }
    }

    public void HitAfterEvent()
    {
        m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
        setRigiZero();
        //m_Animator.SetTrigger(setTrigger.triggerHit.ToString());
    }

    public void DeathAfterEvent()
    {
        
        m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
        recycle();
        //m_Animator.SetTrigger(setTrigger.triggerHit.ToString());
    }

    public void VictoryAfterEvent()
    {
        m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
        if (m_is_close_to_player)
        {
            m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.victory_to_attack);
        }
        else
        {
            m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.victory_to_idle);
        }
    }

    public void AttackAfterEvent()
    {
        m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
        setRigiZero();
        int i_rand = UnityEngine.Random.Range(0, 10);
        if (i_rand > 7)
        {
            m_Animator.SetInteger(setInt.nextInt.ToString(), (int)nextInt.attack_to_victory);
        }
        // m_Animator.SetInteger(setInt.nextInt.ToString(), -1);
    }

}
