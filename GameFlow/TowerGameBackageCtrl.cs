using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerGameBackageCtrl : MonoBehaviour
{
    [Header("BackageRoot")]
    [SerializeField] GameObject sf_obj_backage;
    [Header("背包格子")]
    [SerializeField] BackageGridUnit[] sf_grid_arr;
    [Header("已裝備")]
    [SerializeField] Image[] sf_img_equip_arr;

    public enum itemGridEnum
    {
        weapon_0,
        weapon_1,
        weapon_2,
        glove_0,
        glove_1,
        glove_2,
        armor_0,
        armor_1,
        armor_2,
    }

    [Header("weapon Sprite")]
    [SerializeField] Sprite[] sf_img_weapon_arr;
    [Header("glove Sprite")]
    [SerializeField] Sprite[] sf_img_glove_arr;
    [Header("armor Sprite")]
    [SerializeField] Sprite[] sf_img_armor_arr;



    public void init()
    {
        sf_obj_backage.SetActive(true);        
    }

    public void clear()
    {
        //Debug.LogError("backage clear");
        // 還要先清空背包
        sf_obj_backage.SetActive(false);
    }


    public void clickGrid(int _index)
    {

    }


}
