﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCtrl : MonoBehaviour
{
    [Header("buildings")]    
    [SerializeField] Transform[] sf_tr_buildings; // 要依照 buildings_order_enum 的順序拉進來
    public enum buildings_order_enum
    {        
        classroom,
        theater,
        english_game,
        tower_game_flow,
        length
    }


    Vector3 m_hide_pos = new Vector3(0, 3000, 0);
    Vector3 m_show_pos = new Vector3(0, 0, 0);
    BuildingTheaterCtrl m_BuildingTheaterCtrl = null;
    BuildingEnGameCtrl m_BuildingEnGameCtrl = null;
    BuildingClassroomCtrl m_BuildingClassroomCtrl = null;
    AudioCtrl m_AudioCtrl = null;
    DirCtrl m_DirCtrl = null;
    GameFlowCtrl m_GameFlowCtrl = null;
    bool m_is_init = false;
    public void init()
    {

        m_BuildingTheaterCtrl = MyFinder.Instance.getBuildingTheaterCtrl();
        m_BuildingEnGameCtrl = MyFinder.Instance.getBuildingEnGameCtrl();
        m_CoverCtrl = MyFinder.Instance.getCoverCtrl();
        m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        m_DirCtrl = MyFinder.Instance.getDirCtrl();
        m_BuildingClassroomCtrl = MyFinder.Instance.getBuildingClassroomCtrl();
        m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();

        for (int i = 0; i < sf_tr_buildings.Length; i++)
        {
            sf_tr_buildings[i].localPosition = m_hide_pos;
        }

        m_is_init = true;
    }



    // 透明->黑 (淡入)
    CoverCtrl m_CoverCtrl = null;
    buildings_order_enum m_cur_enum_from = buildings_order_enum.length;
    buildings_order_enum m_cur_enum_to = buildings_order_enum.length;
    public void fromBuildingToAnother(buildings_order_enum _from_enum, buildings_order_enum _to_enum)
    {
        if (!m_is_init) return;

        m_cur_enum_from = _from_enum;
        m_cur_enum_to = _to_enum;

        Debug.Log("_from=" + _from_enum + ",to=" + _to_enum);

        // 進/出教室 要init的事情
        m_BuildingClassroomCtrl.checkInOut(true, m_cur_enum_from, m_cur_enum_to);

        // 進/出電影院 要處理youtube player開關
        m_BuildingTheaterCtrl.checkInOut(true, m_cur_enum_from, m_cur_enum_to);
   
        // 進/出英語遊戲 要init的事情
        m_BuildingEnGameCtrl.checkInOut(true, m_cur_enum_from, m_cur_enum_to);

        // 進/出塔防game flow要做的事
        m_GameFlowCtrl.checkInOut(true, m_cur_enum_from, m_cur_enum_to);
        
        // 這邊改變cover fadein結束之後要做的事
        m_CoverCtrl.changeFadeinToType(CoverCtrl.FadeinToTypeEnum.from_building_to_another, m_cur_enum_to);
    
        m_CoverCtrl.playFadeIn();
        
    }

    // 黑->透明 (淡出)
    public void showFadeinToBuilding()
    {
        StartCoroutine(_showFadeinToBuilding());
    }


    IEnumerator _showFadeinToBuilding()
    {
        
        if (!sf_tr_buildings[(int)m_cur_enum_to].gameObject.activeSelf)
        {
            sf_tr_buildings[(int)m_cur_enum_to].gameObject.SetActive(true);
        }
        sf_tr_buildings[(int)m_cur_enum_to].localPosition = m_show_pos;
        sf_tr_buildings[(int)m_cur_enum_from].localPosition = m_hide_pos;

        // 進出電影院 要處理youtube player開關
        m_BuildingTheaterCtrl.checkInOut(false, m_cur_enum_from, m_cur_enum_to);

        // 各地音效都在這控制
        playAudio(m_cur_enum_to);

        // 淡出
        m_CoverCtrl.playFadeOut();

        yield break;       
    }

    public void showBuilding(buildings_order_enum to_enum)
    {
        playAudio(to_enum);

        if (!sf_tr_buildings[(int)to_enum].gameObject.activeSelf)
        {
            sf_tr_buildings[(int)to_enum].gameObject.SetActive(true);
        }
        sf_tr_buildings[(int)to_enum].localPosition = m_show_pos;
        if (to_enum == buildings_order_enum.classroom)
        {
            m_DirCtrl.closeDir();            
        }
    }


    public void clickEntrance(buildings_order_enum _from_enum, buildings_order_enum _to_enum)
    {
        fromBuildingToAnother(_from_enum, _to_enum);
    }


    void playAudio(buildings_order_enum _to_enum)
    {
        switch (_to_enum)
        {
            case buildings_order_enum.classroom:
                m_AudioCtrl.playMusic(AudioCtrl.MusicClipsEnum.classroom);
                break;
            case buildings_order_enum.theater:
                m_AudioCtrl.playMusic(AudioCtrl.MusicClipsEnum.length); // 不播放背景音樂的意思
                break;
            case buildings_order_enum.english_game:
                m_AudioCtrl.playMusic(AudioCtrl.MusicClipsEnum.en_game_room);
                break;
            case buildings_order_enum.tower_game_flow:
                m_AudioCtrl.playMusic(AudioCtrl.MusicClipsEnum.en_game_room);
                break;
            case buildings_order_enum.length:
                break;
            default:
                break;
        }
    }
}
