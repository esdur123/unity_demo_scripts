﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingClassroomCtrl : MonoBehaviour
{
    DirCtrl m_DirCtrl = null;
    ClickEffect m_ClickEffect = null;
    MySystem m_mySystem = null;
    Main.buildModeEnum m_playmode;
    public void checkInOut(bool _is_fadein, BuildingCtrl.buildings_order_enum _from_enum, BuildingCtrl.buildings_order_enum _to_enum)
    {
        if (m_DirCtrl == null) m_DirCtrl = MyFinder.Instance.getDirCtrl();
        if (m_ClickEffect == null) m_ClickEffect = MyFinder.Instance.getClickEffect();
        if (m_mySystem == null) m_mySystem = MyFinder.Instance.getMySystem();
        m_playmode = MyFinder.Instance.getMain().p_playmode;

        Debug.Log("_is_fadein=" + _is_fadein);
        if (_is_fadein == true && _to_enum == BuildingCtrl.buildings_order_enum.classroom)
        {
            Debug.Log("進入 教室");
            m_DirCtrl.closeDir();
            m_ClickEffect.setUpdate(false);
            m_mySystem.changeSkybox(MySystem.skyboxEnum.default_skybox);

            //if (m_playmode == Main.buildModeEnum.fake_ar || m_playmode == Main.buildModeEnum.unity_editor)
            if (m_playmode == Main.buildModeEnum.fake_ar)
            {
                m_mySystem.unLockCamera();
            }
            
            return;
        }

    }



}
