﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BuildingEvents : MonoBehaviour, IDebugOnTrigger
{
    [Header("type")]
    public colorTxtEnum p_color_type = colorTxtEnum.background;
    public testingEnum p_test_type = testingEnum.regular;

    [Header("from")]
    public BuildingCtrl.buildings_order_enum m_from_type;
    [Header("to")]
    public BuildingCtrl.buildings_order_enum m_to_type;
    BuildingCtrl m_BuildingCtrl = null;
    [Header("Image")]
    [SerializeField] Image sf_img_bg;
    [SerializeField] Text sf_txt;
    [Header("OpenItem")]
    [SerializeField] GameObject sf_obj_item;

    public enum colorTxtEnum
    {
        background,
        text,
        length
    }

    public enum testingEnum
    {
        regular,
        openItem,
        length
    }

    void getBuildingCtrl()
    {
        if (m_BuildingCtrl == null)
        {
            m_BuildingCtrl = MyFinder.Instance.getBuildingCtrl();
        }
    }


    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    Vector4 c_color_original = new Vector4(0, 0, 0, 0.7f); // 背景原始顏色
    Vector4 c_color_enter = new Vector4(1, 0.2964f, 0f, 0.7f); // 背景hover 顏色
    Vector4 c_color_enter_txt = new Vector4(0, 0.9669428f, 1, 1); // 文字hover 顏色

    
    public void onEnter()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
        if (sf_img_bg == null) return;

        if(p_color_type== colorTxtEnum.background) sf_img_bg.color = c_color_enter;
        if (p_color_type == colorTxtEnum.text) sf_txt.color = c_color_enter_txt;

    }


    public void onExit()
    {
        if (sf_img_bg == null) return;

        if (p_color_type == colorTxtEnum.background) sf_img_bg.color = c_color_original;
        if (p_color_type == colorTxtEnum.text) sf_txt.color = Color.white; ;

        //getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
    }


    // 看換場警 要不要融合成一個檔案?
    //int m_text_int = 0;
    bool m_is_item_open = true;
    public void onClick()
    {
        
        if (p_test_type== testingEnum.regular)
        {
            getBuildingCtrl();
            m_BuildingCtrl.clickEntrance(m_from_type, m_to_type);
            getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_click);
        }

        if (p_test_type == testingEnum.openItem)
        {
            getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.CSG_Coin_02);
            m_is_item_open = !m_is_item_open;
            if (m_is_item_open)
            {
                sf_obj_item.SetActive(true);
            }
            else
            {
                sf_obj_item.SetActive(false);
            }
            //MyFinder.Instance.getTestCtrl().testUpDown(true);
        }

        //if (p_test_type == testingEnum.debug_1)
        //{
        //    MyFinder.Instance.getTestCtrl().testUpDown(false);
        //}

    }

    public void QuitGame()
    {
        MyFinder.Instance.getMySystem().quitGame();
    }

    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        string obj_name = this.gameObject.name;
        switch (obj_name)
        {
            case "txt_classroom_quit":
                QuitGame();
                break;
            default:
                onClick();
                break;
        }        
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }


}
