using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingClassroomCharEvent : MonoBehaviour, IDebugOnTrigger
{
    [Header("Anim")]
    [SerializeField] Animator sf_anim;

    public enum classroomCharEnum
    {
        none,
        cat,
        police,
        sport_girl,
        sit_boy,
        wave_boy,
        girl_Latifa,
        girl_Rin,
        robert_jammo,
        length
    }
    public classroomCharEnum char_enum = classroomCharEnum.length;


    AudioCtrl m_AudioCtrl = null;
    public void onClick()
    {
        //// 動作
        //if (char_enum == classroomCharEnum.cat) sf_anim.SetTrigger("Trigger_Walk");
        //if (char_enum == classroomCharEnum.police) sf_anim.SetTrigger("Trigger_Click_01");
        //if (char_enum == classroomCharEnum.sport_girl) sf_anim.SetTrigger("Trigger_Click_01");
        //if (char_enum == classroomCharEnum.sit_boy) sf_anim.SetTrigger("Trigger_Click_01");
        //if (char_enum == classroomCharEnum.wave_boy) sf_anim.SetTrigger("Trigger_Click_01");
        //if (char_enum == classroomCharEnum.girl_Latifa) sf_anim.SetTrigger("Trigger_Click_01");
        //if (char_enum == classroomCharEnum.girl_Rin) sf_anim.SetTrigger("Trigger_Click_01");
        //if (char_enum == classroomCharEnum.robert_jammo) sf_anim.SetTrigger("Trigger_Click_01");

        //// 音效
        //if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        //m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.CR2_Cute_UI_1);
    }

    public void onEnter()
    {
        // 動作
        if (char_enum == classroomCharEnum.cat) sf_anim.SetTrigger("Trigger_Walk");
        if (char_enum == classroomCharEnum.police) sf_anim.SetTrigger("Trigger_Click_01");
        if (char_enum == classroomCharEnum.sport_girl) sf_anim.SetTrigger("Trigger_Click_01");
        if (char_enum == classroomCharEnum.sit_boy) sf_anim.SetTrigger("Trigger_Click_01");
        if (char_enum == classroomCharEnum.wave_boy) sf_anim.SetTrigger("Trigger_Click_01");
        if (char_enum == classroomCharEnum.girl_Latifa) sf_anim.SetTrigger("Trigger_Click_01");
        if (char_enum == classroomCharEnum.girl_Rin) sf_anim.SetTrigger("Trigger_Click_01");
        if (char_enum == classroomCharEnum.robert_jammo) sf_anim.SetTrigger("Trigger_Click_01");

        // 音效
        if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.CR2_Cute_UI_1);
    }

    public void onExit()
    {

    }

    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        onClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }
}
