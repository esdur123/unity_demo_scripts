﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnGameSelectChar : MonoBehaviour
{
    [Header("Chars")]
    [SerializeField] Transform[] sf_tr_chars;
    [Header("Root")]
    [SerializeField] Transform sf_tr_root;
    [Header("DirLookCenter")]
    [SerializeField] Transform sf_tr_center;
    [SerializeField] Transform sf_tr_center_play;
    [Header("Events")]
    [SerializeField] EnGameSelectEvent[] sf_events;
    [Header("已選擇的人物")]
    [SerializeField] Transform[] sf_tr_selected_chars;
    [SerializeField] Animator[] sf_anim_selected_chars;
    string[] m_char_name_arr = new string[] { "安妮", "小黃", "優姊", "小鯨", "亞瑟", "機器人" };
    [Header("SelectedInfo")]
    [SerializeField] Transform sf_tr_selected_info_root;
    [SerializeField] Text sf_tr_selected_info_name;
    [SerializeField] Text sf_tr_selected_info_lvl;
    [SerializeField] Text sf_tr_selected_info_time;
    [Header("TopHeader")]
    [SerializeField] Text sf_txt_top_header;
    [SerializeField] Image sf_img_top_header;
    [SerializeField] Text sf_txt_top_header_game;
    [SerializeField] Image sf_img_top_header_game;
    [Header("LvLSelectRoot")]
    [SerializeField] Transform sf_tr_root_lvl_select;
    [Header("LvlTimeRoot")]
    [SerializeField] Transform sf_tr_root_lvl_time;
    [Header("SelectSavePos")]
    [SerializeField] Transform sf_tr_save_select;
    [Header("ReadySavePos")]
    [SerializeField] Transform sf_tr_save_ready;
    [Header("AnimSelected")]
    [SerializeField] public Animator sf_anim_selected;
    [Header("TxtLvl")]
    [SerializeField] Image[] sf_img_lvl_arr;
    [Header("TxtTime")]
    [SerializeField] Image[] sf_img_time_arr;
    [Header("ReadyCanvasRoot")]
    [SerializeField] Transform sf_tr_root_ready;
    [Header("imgReadyBtn")]
    [SerializeField] Image sf_img_ready;
    [Header("AnimReady")]
    [SerializeField] Animator sf_anim_ready;
    [Header("AnimCanvas")]
    [SerializeField] Animator sf_anim_canvas;
    [Header("AnswerRoot")]
    [SerializeField] Transform sf_tr_ans;
    [Header("Timer")]
    [SerializeField] Image sf_img_timer;
    [SerializeField] Text sf_txt_timer;
    [Header("計算按鈕")]
    [SerializeField] GameObject sf_obj_calu;
    [Header("切換場景")]
    [SerializeField] GameObject sf_obj_env_switch;
    [Header("回教室")]
    [SerializeField] GameObject sf_obj_btn_back_classroom;

    enum selectFlowEnum
    {
        char_select,
        lvl_select,
        time_select,
        ready,
        length
    }
    selectFlowEnum m_cur_flow = selectFlowEnum.length;

    string c_choose_char_txt_top_header = "1.請選擇人物";
    string c_choose_lvl_txt_top_header  = "2.請選擇難度";
    string c_choose_time_txt_top_header = "3.請選擇時間";
    string c_choose_ready_txt_top_header = "準備開始";

    public enum CharEnum
    {
        girl_rin, 
        duck,
        unity_chan,
        whale,
        boy_satomi,
        hp_gundan,
        length
    }

    public enum triggerEnum
    {
        trigger_correct,
        trigger_wrong,
        length
    }

    // 這邊 每次進入英語遊戲時 初始化
    DirCtrl m_DirCtrl = null;
    CSVReader m_CSVReader = null;
    EnGamePlayCtrl m_EnGamePlayCtrl = null;
    AudioCtrl m_AudioCtrl = null;
    BuildingEnGameCtrl m_BuildingEnGameCtrl = null;
    MySystem m_MySystem = null;
    Main.buildModeEnum m_playmode;
    Main.demoEnum m_demo_mode;
    public void init(bool _is_reset)
    {
        if (m_DirCtrl == null) m_DirCtrl = MyFinder.Instance.getDirCtrl();
        if (m_CSVReader == null) m_CSVReader = MyFinder.Instance.getCSVReader();
        if (m_EnGamePlayCtrl == null) m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
        if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        if (m_BuildingEnGameCtrl == null) m_BuildingEnGameCtrl = MyFinder.Instance.getBuildingEnGameCtrl();
        if (m_MySystem == null) m_MySystem = MyFinder.Instance.getMySystem();
        m_playmode = MyFinder.Instance.getMain().p_playmode;

        m_demo_mode = MyFinder.Instance.getMain().p_demo_mode;

        if (m_demo_mode== Main.demoEnum.loop_en_game)
        {
            sf_obj_btn_back_classroom.SetActive(false);
        }

        // 真正的init 不是重新選擇
        if (!_is_reset)
        {
            sf_tr_root.gameObject.SetActive(false);
        }

        // 重新選擇能做的事
        for (int i = 0; i < sf_tr_chars.Length; i++)
        {
            sf_tr_chars[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < sf_events.Length; i++)
        {
            sf_events[i].showSelected(false);
        }

        for (int i = 0; i < sf_tr_selected_chars.Length; i++)
        {
            sf_tr_selected_chars[i].gameObject.SetActive(false);
        }

        m_i_cur_selected_char = -1;
        sf_txt_top_header.text = c_choose_char_txt_top_header;
        m_cur_flow = selectFlowEnum.char_select;
        sf_tr_root_lvl_select.gameObject.SetActive(false);
        m_select_lvl = -1;
        sf_anim_selected.gameObject.SetActive(true);
        sf_anim_selected.Play("enGameUnselectedRoot");
        sf_tr_selected_info_root.gameObject.SetActive(false);
        sf_tr_selected_info_name.text = "角色";
        sf_tr_selected_info_lvl.text = "難度";
        sf_tr_selected_info_time.text = "時間";
        m_select_time = -1;
        sf_tr_root_lvl_time.gameObject.SetActive(false);
        //sf_anim_ready.enabled = true;
        sf_anim_ready.Play("enGameRootHome");
        sf_anim_canvas.Play("enGameRootCanvasHome");
        sf_obj_calu.gameObject.SetActive(false);
        sf_obj_env_switch.SetActive(true);

        for (int i = 0; i < sf_img_lvl_arr.Length; i++)
        {
            sf_img_lvl_arr[i].color = new Vector4(0, 1, 0.7019608f, 0.466f);
        }

        for (int i = 0; i < sf_img_time_arr.Length; i++)
        {
            sf_img_time_arr[i].color = new Vector4(1, 0.7490196f, 0, 0.466f);
        }

        sf_img_ready.color = new Vector4(1, 0, 0.879868f, 0.466f);
        sf_tr_root_ready.gameObject.SetActive(false);

        m_DirCtrl.setTarget(sf_tr_center);

        sf_img_top_header.gameObject.SetActive(true);
        sf_img_top_header_game.gameObject.SetActive(false);
        //m_cur_quest_list = new List<EnglishWords>();
        sf_tr_ans.gameObject.SetActive(false);
        sf_txt_timer.text = "";
        sf_img_timer.gameObject.SetActive(false);
    }


    // 重選(回到 選擇角色)
    public void resetToSelectChar()
    {
        //if (m_playmode == Main.buildModeEnum.fake_ar || m_playmode == Main.buildModeEnum.unity_editor)
        if (m_playmode == Main.buildModeEnum.fake_ar)
        {
            m_MySystem.LockCamera(MySystem.lockCameraEnum.ar_en_game_select_char);
        }
        
        Debug.Log("重選(回到 選擇角色)");
        m_EnGamePlayCtrl.cancelCounting();
        if (m_cur_flow == selectFlowEnum.char_select) return;

        //Debug.Log("resetToSelectChar");
        //m_cur_flow = selectFlowEnum.char_select;


        // 這邊還有很多XD
        init(true);

        m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.btn_click);
        m_AudioCtrl.playMusic(AudioCtrl.MusicClipsEnum.en_game_room);


    }

    public void initSelectChar(EnGameSelectSet _set)
    {
        //if (m_playmode == Main.buildModeEnum.fake_ar || m_playmode == Main.buildModeEnum.unity_editor)
        if (m_playmode == Main.buildModeEnum.fake_ar)
        {
            m_MySystem.LockCamera(MySystem.lockCameraEnum.ar_en_game_select_char);
        }
        //Debug.LogError("init EnGameSelectChar)");
        //Debug.Log("****init EnGameSelectChar***");
        sf_tr_root.gameObject.SetActive(true);
        m_DirCtrl.setTarget(sf_tr_center);
    }



    public void showSelectedAll(bool _is_active)
    {
        for (int i = 0; i < sf_events.Length; i++)
        {
            sf_events[i].showSelected(_is_active);
        }
    }

    // 十級 從0-9
    int m_select_lvl = -1;
    public void chooseLvl(int i_lvl) 
    {        
        m_select_lvl = i_lvl;
        Debug.Log("m_select_lvl=" + m_select_lvl);

        sf_txt_top_header.text = c_choose_time_txt_top_header;
        m_cur_flow = selectFlowEnum.time_select;
        sf_tr_root_lvl_select.gameObject.SetActive(false);
        string chinese_lvl = "";
        if (i_lvl == 0) chinese_lvl = "難度一";
        if (i_lvl == 1) chinese_lvl = "難度二";
        if (i_lvl == 2) chinese_lvl = "難度三";
        if (i_lvl == 3) chinese_lvl = "難度四";
        if (i_lvl == 4) chinese_lvl = "難度五";
        if (i_lvl == 5) chinese_lvl = "難度六";
        if (i_lvl == 6) chinese_lvl = "難度七";
        if (i_lvl == 7) chinese_lvl = "難度八";
        if (i_lvl == 8) chinese_lvl = "難度九";
        if (i_lvl == 9) chinese_lvl = "難度十";
        //if (i_lvl == -1) chinese_lvl = "漸進式";

        string is_flexiable = "固定難度";
        if(m_EnGamePlayCtrl.p_is_flexiable) is_flexiable = "漸進式";
        chinese_lvl = chinese_lvl + "\n" + is_flexiable;

        sf_tr_selected_info_lvl.text = chinese_lvl;

        if (i_lvl != -1)
        {
            sf_tr_root_lvl_time.gameObject.SetActive(true);
        }
        //else
        //{
        //    sf_tr_root_ready.gameObject.SetActive(true);
        //    sf_tr_selected_info_time.text = "無限";
        //}
        
    }

    
    // 按下'遊戲開始'
    public void chooseReady()
    {
        Debug.Log("開始遊戲!!!");
        sf_tr_root_ready.gameObject.SetActive(false);
        sf_anim_ready.Play("enGameRootReady");
        sf_anim_canvas.Play("enGameRootCanvasReady"); // 666
        m_DirCtrl.setTarget(sf_tr_center_play);
        sf_anim_selected.gameObject.SetActive(false);
        StartCoroutine(showSelectedCharLater());
        sf_img_top_header.gameObject.SetActive(false);
        //if (m_playmode == Main.buildModeEnum.fake_ar || m_playmode == Main.buildModeEnum.unity_editor)
        if (m_playmode == Main.buildModeEnum.fake_ar)
        {
            m_MySystem.LockCamera(MySystem.lockCameraEnum.ar_en_game_play);
        }

        // 開始遊戲
        m_EnGamePlayCtrl.init(m_select_lvl, m_select_time);
        sf_obj_calu.gameObject.SetActive(true);
        sf_obj_env_switch.SetActive(false);
    }

    IEnumerator showSelectedCharLater()
    {
        m_DirCtrl.forceCheckingBool(true, 2.1f);
        yield return new WaitForSeconds(2);
        sf_anim_selected.gameObject.SetActive(true);
        sf_anim_selected.Play("enGameCharPlayRoot");
        sf_img_top_header_game.gameObject.SetActive(true);
        sf_img_timer.gameObject.SetActive(true);
        sf_txt_timer.text = "本題時間";
        yield return new WaitForSeconds(2);
        m_DirCtrl.forceCheckingBool(true, 1.3f);
    }

    // 難度 10 15
    int m_select_time = -1;
    Vector3 m_char_ready_pos_delta = new Vector3(350,0,250);
    Vector3 m_canvas_ready_pos_delta = new Vector3(350, 0, 250);
    public void chooseTime(int i_time)
    {
        m_select_time = i_time;
        Debug.Log("m_select_time=" + i_time);


        //sf_tr_root_lvl_select.gameObject.SetActive(false);
        string chinese_time = "";
        if (i_time == 10) chinese_time = "十分鐘";
        if (i_time == 15) chinese_time = "十五分鐘";
        sf_tr_selected_info_time.text = chinese_time;

        sf_tr_root_lvl_time.gameObject.SetActive(false);

        // showReady
        sf_txt_top_header.text = c_choose_ready_txt_top_header;
        m_cur_flow = selectFlowEnum.ready;
        sf_anim_selected.enabled = true;
        sf_anim_selected.Play("enGameSelectedRoot");

        sf_tr_root_ready.gameObject.SetActive(true);
        //sf_tr_selected_chars[m_i_cur_selected_char].localPosition += m_char_ready_pos_delta;
        //sf_tr_selected_info_root.localPosition = sf_tr_save_ready.localPosition;

    }

 

    public void showLvl()
    {
        showSelectedAll(false);
        sf_txt_top_header.text = c_choose_lvl_txt_top_header;
        m_cur_flow = selectFlowEnum.lvl_select;
        sf_tr_root_lvl_select.gameObject.SetActive(true);
    }

    //public int getCurCharIndex()
    //{
    //    return m_i_cur_selected_char;
    //}

    public void setCurCharTrigger(triggerEnum _trigger_enum)
    {
        sf_anim_selected_chars[m_i_cur_selected_char].SetTrigger(_trigger_enum.ToString());
    }

    int m_i_cur_selected_char = -1;
    public void selectChar(CharEnum _enum)
    {
        showSelectedAll(false);
        for (int i = 0; i < sf_tr_chars.Length; i++)
        {
            sf_tr_chars[i].gameObject.SetActive(false);
        }

        m_i_cur_selected_char = (int)_enum;
        sf_tr_selected_chars[m_i_cur_selected_char].gameObject.SetActive(true);
        sf_tr_selected_info_root.gameObject.SetActive(true);
        sf_anim_selected.enabled = true;
        sf_anim_selected.Play("enGameUnselectedRoot");
        //sf_tr_selected_info_root.localPosition = sf_tr_save_select.localPosition;
        sf_tr_selected_info_name.text = m_char_name_arr[(int)_enum];
    }


    //EnGamePlayCtrl m_EnGamePlayCtrl = null;
    //EnGamePlayCtrl getEnGamePlayCtrl()
    //{
    //    if (m_EnGamePlayCtrl == null) m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
    //    return m_EnGamePlayCtrl;
    //}
}
