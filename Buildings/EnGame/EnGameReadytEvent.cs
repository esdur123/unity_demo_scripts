﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnGameReadytEvent : MonoBehaviour, IDebugOnTrigger
{
    [Header("Img")]
    [SerializeField] Image sf_img_bg;

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }


    public void colorEnterTxt()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
        sf_img_bg.color = new Vector4(1, 0, 0.879868f, 0.466f);
    }

    public void colorExitTxt()
    {
        sf_img_bg.color = new Vector4(0, 1, 0.1538355f, 0.466f);
        
    }

    EnGameSelectChar m_EnGameSelectChar = null;
    public void clickReady()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.en_game_start);
        if (m_EnGameSelectChar == null) m_EnGameSelectChar = MyFinder.Instance.getEnGameSelectChar();
        m_EnGameSelectChar.chooseReady();
    }

    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        clickReady();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        colorEnterTxt();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        colorExitTxt();
    }
}
