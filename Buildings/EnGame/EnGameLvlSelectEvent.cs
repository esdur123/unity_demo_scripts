﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnGameLvlSelectEvent : MonoBehaviour, IDebugOnTrigger
{
    public int p_Lvl;
    [Header("Txt")]
    [SerializeField] Text sf_txt;
    [Header("Img")]
    [SerializeField] Image sf_img_bg;
    [Header("Type")]
    [SerializeField] EnGamePlayCtrl.enGameType m_type;
    
    

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    public void colorEnterTxt()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
        sf_img_bg.color = new Vector4(1, 0, 0.333f, 0.466f);
    }

    public void colorExitTxt()
    {
        sf_img_bg.color = new Vector4(0,1, 0.7019608f, 0.466f);
    }

    EnGameSelectChar m_EnGameSelectChar = null;
    bool m_is_flex = false;
    public void clickLvl()
    {
        if (m_EnGameSelectChar == null) m_EnGameSelectChar = MyFinder.Instance.getEnGameSelectChar();
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_click);

        if (m_type == EnGamePlayCtrl.enGameType.regular)
        {
            m_EnGameSelectChar.chooseLvl(p_Lvl);
        }
        else
        {            
            switchFlexable();
        }
           
    }

    void switchFlexable()
    {
        m_is_flex = !m_is_flex;
        getEnGamePlayCtrl().p_is_flexiable = m_is_flex;
        if (m_is_flex)
        {
            sf_txt.text = "漸進式"; 
            sf_txt.color = new Vector4(0.2235294f, 0.2313726f, 1, 0.729f);
        }
        else
        {
            sf_txt.text = "固定難度";
            sf_txt.color = new Vector4(0.2235294f, 0.2313726f, 1, 0.729f);
        }

    }

    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        clickLvl();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        colorEnterTxt();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        colorExitTxt();
    }

    EnGamePlayCtrl m_EnGamePlayCtrl=null;
    EnGamePlayCtrl getEnGamePlayCtrl()
    {
        if (m_EnGamePlayCtrl == null) m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
        return m_EnGamePlayCtrl;
    }
}
