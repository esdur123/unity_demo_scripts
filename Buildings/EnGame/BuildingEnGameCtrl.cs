﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnGameSelectSet
{
    public GameObject obj = null;
    public Vector3 pos = new Vector3();
}


public class BuildingEnGameCtrl : MonoBehaviour
{
    GameFlowCtrl m_GameFlowCtrl = null;
    EnGameSelectChar m_EnGameSelectChar = null;
    EnGamePlayCtrl m_EnGamePlayCtrl = null;
    MySystem m_mySystem = null;
    [Header("SeiFi EnBuilding")]
    [SerializeField] public GameObject sf_obj_en_seifi;
    [Header("SeiFi dungon")]
    [SerializeField] public GameObject sf_obj_en_dungon;
    public enum EnvEnum
    {
        sei_fi,
        blue_dungon,
        length
    }


    public void init()
    {
        if (m_GameFlowCtrl == null)
        {
            m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();
        }

        if (m_EnGameSelectChar == null)
        {
            m_EnGameSelectChar = MyFinder.Instance.getEnGameSelectChar();
        }

        if (m_EnGamePlayCtrl == null)
        {
            m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
        }

        if (m_mySystem == null) m_mySystem = MyFinder.Instance.getMySystem();
    }


    public void checkInOut(bool _is_fadein, BuildingCtrl.buildings_order_enum _from_enum, BuildingCtrl.buildings_order_enum _to_enum)
    {        
        if (_is_fadein == true && _to_enum == BuildingCtrl.buildings_order_enum.english_game)
        {
            Debug.Log("進入 英語遊戲");
            m_EnGameSelectChar.init(false);

            // 製作一份臨時的flow
            m_GameFlowCtrl.readEnGameGameFlow();
            m_GameFlowCtrl.startGameFlow();


            return;
        }


        if (_from_enum == BuildingCtrl.buildings_order_enum.english_game)
        {
            m_EnGamePlayCtrl.cancelCounting();
        }
    }

    Vector3 m_hide_pos = new Vector3(0, 3000, 0);
    Vector3 m_show_pos = new Vector3(0, 0, 0);
    public bool p_is_scifi_env = true; // 一開始是太空艙
    public void switchEnv()
    {
        p_is_scifi_env = !p_is_scifi_env;
        if (p_is_scifi_env)
        {
            sf_obj_en_seifi.transform.localPosition  = m_show_pos;
            sf_obj_en_dungon.transform.localPosition = m_hide_pos;
            m_mySystem.changeSkybox(MySystem.skyboxEnum.default_skybox);
            m_mySystem.sf_obj_360.SetActive(false);
        }
        else
        {
            sf_obj_en_seifi.transform.localPosition = m_hide_pos;
            sf_obj_en_dungon.transform.localPosition = m_show_pos;
            m_mySystem.changeSkybox(MySystem.skyboxEnum.blue_sky);
            m_mySystem.sf_obj_360.SetActive(false);
        }
    }
}
