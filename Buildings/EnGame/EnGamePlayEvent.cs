﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnGamePlayEvent : MonoBehaviour, IDebugOnTrigger
{
    [Header("i Ans")]
    public int m_answer;
    [Header("Img")]
    [SerializeField] public Image sf_img_bg;
    [SerializeField] public Text  sf_txt;
    public void colorEnterTxt()
    {
        
        if (m_EnGamePlayCtrl == null) m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
        if (m_EnGamePlayCtrl.getAnswerStopState()) return;
        sf_img_bg.color = new Vector4(0.2235f, 0.5203f, 1, 0.7882353f);
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.woosh_1_2);
    }

    public void colorExitTxt()
    {
        if (m_EnGamePlayCtrl == null) m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
        if (m_EnGamePlayCtrl.getAnswerStopState()) return;
        sf_img_bg.color = new Vector4(0, 0, 0, 0.7882353f);
    }

    EnGamePlayCtrl m_EnGamePlayCtrl = null;
    public void clickAnswer()
    {
        if (m_EnGamePlayCtrl == null) m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
        m_EnGamePlayCtrl.clickAnswer(m_answer);        
    }

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        clickAnswer();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        colorEnterTxt();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        colorExitTxt();
    }
}
