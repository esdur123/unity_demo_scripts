﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnGamePlayCtrl : MonoBehaviour
{
    [Header("Top Header")]
    [SerializeField] Text sf_txt_header;

    [Header("AnswerRoot")]
    [SerializeField] Transform sf_tr_ans;
    
    [Header("Timer")]
    [SerializeField] Image sf_img_timer;
    [SerializeField] Text sf_txt_timer;

    [Header("難度(提示)")]
    [SerializeField] Image sf_img_hint;
    [SerializeField] Text sf_txt_hint;

    [Header("總時間")]
    [SerializeField] Image sf_img_fulltimer;
    [SerializeField] Text sf_txt_fulltimer;
    int m_full_timer = 0;
    int m_hold_fulltimer = 0;

    [Header("答題板")]
    [SerializeField] EnGamePlayEvent[] sf_event;
    [Header("計算結果")]
    [SerializeField] Image sf_img_final_board;
    [SerializeField] Text sf_txt_final_board;

    // 漸進式
    public bool p_is_flexiable = false;
    CSVReader.FliexibleLvlEnum m_cur_fliex_enum = CSVReader.FliexibleLvlEnum.length;

    public enum enGameType
    {
        regular,
        flexiable,
        length
    }

    // Start is called before the first frame update
    List<EnglishWords> m_cur_quest_list = new List<EnglishWords>();
    List<EnglishWords> m_flex_quest_list = new List<EnglishWords>();
    CSVReader m_CSVReader = null;
    AudioCtrl m_AudioCtrl = null;
    int m_play_time = 0;
    int m_cur_time = 0;
    int m_time_before = 4;
    int m_cur_quest_i = -1;
    bool m_is_first = true;
    int time_total = 600;
    const int c_time_total_10_min = 600;
    const int c_time_total_15_min = 900;
    int m_lvl = 0;
    public void init(int _lvl, int _time)
    {
        m_lvl = _lvl;        
        m_is_show_end = false;
        sf_txt_fulltimer.text = "總時間";
        sf_txt_timer.text = "本題時間";
        //m_flex_set_count = 1;
        i_correct_count = 0;
        m_is_first = true;
        CancelInvoke("countingTime");
        m_cur_quest_i = -1;
        m_cur_time = 0;
        m_play_time = _time;
        sf_txt_header.text = "準備開始";
        if (m_CSVReader == null) m_CSVReader = MyFinder.Instance.getCSVReader();

        m_cur_quest_list = new List<EnglishWords>();
        m_flex_quest_list = new List<EnglishWords>();
        //m_all_quest_list = new List<EnglishWords>();
        //m_all_quest_list = m_CSVReader.getAllEnList();

        //int quest_count = 0;
        if (m_play_time == 10)
        {
            time_total = c_time_total_10_min;
            m_hold_fulltimer = time_total;
            m_full_timer = time_total + 2;
            //quest_count = 20;
        }

        if (m_play_time == 15)
        {
            time_total = c_time_total_15_min;
            m_hold_fulltimer = time_total;
            m_full_timer = time_total + 2;
            //quest_count = 30;
        }

        if (p_is_flexiable) // 漸進式
        {
            if (_lvl == 0) m_cur_fliex_enum = CSVReader.FliexibleLvlEnum.f_1_1;
            if (_lvl == 1) m_cur_fliex_enum = CSVReader.FliexibleLvlEnum.f_2_1;
            if (_lvl == 2) m_cur_fliex_enum = CSVReader.FliexibleLvlEnum.f_3_1;
            if (_lvl == 3) m_cur_fliex_enum = CSVReader.FliexibleLvlEnum.f_4_1;
            if (_lvl == 4) m_cur_fliex_enum = CSVReader.FliexibleLvlEnum.f_5_1;
            if (_lvl == 5) m_cur_fliex_enum = CSVReader.FliexibleLvlEnum.f_6_1;
            m_cur_quest_list = m_CSVReader.getFlexibleQuestList(m_cur_fliex_enum, null);
            //m_flex_quest_list = copyEnList(m_cur_quest_list);

            //sf_img_final_board.gameObject.SetActive(false);

        }
        else // 非漸進式
        {
            sf_img_hint.gameObject.SetActive(false);
            // m_cur_quest_list = m_CSVReader.getLvlQuest(_lvl, quest_count, false);
            m_cur_quest_list = m_CSVReader.getLvlCycleList(_lvl,0,CSVReader.FliexibleLvlEnum.length,true);
            //Debug.LogError(m_cur_quest_list.Count);
        }

        sf_img_fulltimer.gameObject.SetActive(true);
        sf_img_hint.gameObject.SetActive(true);
        sf_txt_hint.text = "本題難度";// "_lvl="+ _lvl;
        sf_img_final_board.gameObject.SetActive(false);
        //m_same_lvl_quest_list = m_CSVReader.getLvlEnList(); // 同等級的題庫
        Debug.Log("m_cur_quest_list.Count=" + m_cur_quest_list.Count);

        InvokeRepeating("countingTime", 4.0f , 1.0f);

        if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        m_AudioCtrl.playMusic(AudioCtrl.MusicClipsEnum.en_game_play);
    }


    public void cancelCounting()
    {
        sf_txt_header.text = "";
        m_cur_quest_i = -1;
        m_cur_time = 0;
        sf_img_timer.gameObject.SetActive(false);
        sf_txt_timer.text = "本題時間";
        CancelInvoke("countingTime");
    }

    Vector4 m_color_correct = new Vector4(0, 0.404f, 0.229f, 0.6588f);
    Vector4 m_color_original = new Vector4(0, 0, 0, 0.7882353f);
    Vector4 m_color_fixed = new Vector4(1, 0.7490196f,0, 0.6588f);
    bool m_is_stop_answer = false;
    public void clickAnswer(int _i_click)
    {
        if (m_clicked)
        {
            Debug.Log("已經答過了");
            return;
        }

        m_cur_quest_list[m_cur_quest_i].is_answered = true;

        m_is_stop_answer = true;

        if (m_clicked == false)
        {
            m_clicked = true;
        }

        if (m_i_correct == _i_click)
        {
            for (int i = 0; i < sf_event.Length; i++)
            {               
                if (i == _i_click)
                {
                    sf_event[i].sf_img_bg.color = m_color_correct;
                    sf_event[i].sf_txt.text = m_cur_quest_list[m_cur_quest_i].spell + "\n" + m_cur_quest_list[m_cur_quest_i].show_ans;
                }
                else
                {
                    sf_event[i].sf_txt.text = "";
                }
            }

            //Debug.Log("正確");
            m_cur_quest_list[m_cur_quest_i].is_correct = true;
            //Debug.LogError("答對:count="+ m_cur_quest_list.Count+",i="+ m_cur_quest_i+","+ m_cur_quest_list[m_cur_quest_i].is_correct);
            i_correct_count++;
            m_cur_time = 3; // 給看n秒答案
            m_full_timer += 3; // 總時間補上去
            showFullTimer(m_hold_fulltimer);
            m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.special_2_2);
            tempFxCorrect();
        }
        else
        {
            m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.lose_1_1);
            showFixedAnswer();            
        }

        addThisQuestToFlexList();
    }


    [Header("Temp答對")]
    [SerializeField] GameObject[] sf_obj_temp_correct_fx;
    void tempFxCorrect()
    {
        for (int i = 0; i < sf_obj_temp_correct_fx.Length; i++)
        {
            sf_obj_temp_correct_fx[i].SetActive(true);
        }

        getEnGameSelectChar().setCurCharTrigger(
                EnGameSelectChar.triggerEnum.trigger_correct
            );
    }

    [Header("Temp答錯")]
    [SerializeField] GameObject[] sf_obj_temp_error_fx;
    void tempFxError()
    {
        for (int i = 0; i < sf_obj_temp_error_fx.Length; i++)
        {
            sf_obj_temp_error_fx[i].SetActive(true);
        }

        getEnGameSelectChar().setCurCharTrigger(
                EnGameSelectChar.triggerEnum.trigger_wrong
            );
    }

    int i_correct_count = 0;
    void showFixedAnswer()
    {
        for (int i = 0; i < sf_event.Length; i++)
        {
            if (i == m_i_correct)
            {

                sf_event[i].sf_img_bg.color = m_color_fixed;
                sf_event[i].sf_txt.text = m_cur_quest_list[m_cur_quest_i].spell + "\n" + m_cur_quest_list[m_cur_quest_i].show_ans;
            }
            else
            {
                sf_event[i].sf_img_bg.color = m_color_original;
                sf_event[i].sf_txt.text = "";
            }
        }

        //i_error_count++;
        Debug.Log("錯誤");
        m_cur_time = 4; // 給看n秒答案
        m_full_timer += 4; // 總時間補上去
        showFullTimer(m_hold_fulltimer);
        tempFxError();
    }

    void countingTime()
    {
        m_cur_time--;
        m_full_timer --; // 總時間--
        if (m_full_timer < m_hold_fulltimer)
        {
            m_hold_fulltimer--;
            if (m_hold_fulltimer == 0)
            {
                //Debug.LogError("m_hold_fulltimer == 0");
                showEndPoint();
            }
        }
        
        showFullTimer(m_hold_fulltimer);

        if (m_cur_time < 0)
        {
            if (!m_clicked)
            {
                if (m_is_first)
                {
                    m_is_first = false;
                    m_clicked = true;
                    Debug.Log("-------------- 第一次近來 -----------");
                    return;
                }

                m_clicked = true;
                showFixedAnswer();
                return;
            }

            m_cur_time = 30;


  
            m_cur_quest_i++; // 一開始是-1            
            toNextQuest();
        }

        string str_pre = "第" + (m_flex_quest_list.Count+1) + "題";
        if (m_cur_quest_i == -1)
        {
            str_pre = "";
        }
        sf_txt_timer.text = str_pre + "\n剩餘時間: " + m_cur_time + "秒";
    }


    //int m_flex_set_count = 1;
    void addThisQuestToFlexList()
    {
        if (m_cur_quest_i == -1)
        {
            return;
        }

        //Debug.LogError("count="+ m_cur_quest_list.Count+",i="+ m_cur_quest_i+","+ m_cur_quest_list[m_cur_quest_i].is_correct);
        // 先把剛才答過的問題 加到flex總和List裡面
        //Debug.Log("+++++把剛才答過的問題 加到flex總和List裡面");
        EnglishWords new_en = new EnglishWords();
        new_en.lvl = m_cur_quest_list[m_cur_quest_i].lvl;
        new_en.spell = m_cur_quest_list[m_cur_quest_i].spell;
        new_en.word_type = m_cur_quest_list[m_cur_quest_i].word_type;
        new_en.chinese_01 = m_cur_quest_list[m_cur_quest_i].chinese_01;
        new_en.chinese_02 = m_cur_quest_list[m_cur_quest_i].chinese_02;
        new_en.chinese_03 = m_cur_quest_list[m_cur_quest_i].chinese_03;
        new_en.show_ans = m_cur_quest_list[m_cur_quest_i].show_ans;
        new_en.original_order = m_cur_quest_list[m_cur_quest_i].original_order;
        new_en.is_answered = m_cur_quest_list[m_cur_quest_i].is_answered;
        new_en.is_online = m_cur_quest_list[m_cur_quest_i].is_online;
        new_en.is_correct = m_cur_quest_list[m_cur_quest_i].is_correct;
        new_en.fliex_lvl = m_cur_quest_list[m_cur_quest_i].fliex_lvl;
        new_en.str_wrong_list.Clear();
        for (int j = 0; j < m_cur_quest_list[m_cur_quest_i].str_wrong_list.Count; j++)
        {
            new_en.str_wrong_list.Add(m_cur_quest_list[m_cur_quest_i].str_wrong_list[j]);
        }

        m_flex_quest_list.Add(new_en);

        //Debug.LogError("總累計長度: "+m_flex_quest_list.Count+",對錯:"+ m_cur_quest_list[m_cur_quest_i].is_correct);


        //if (p_is_flexiable && m_cur_quest_i >= m_cur_quest_list.Count) // 如果漸進式這輪答完了

        // 如果題目答完了
        if (m_cur_quest_i == m_cur_quest_list.Count-1)
        {
            if (p_is_flexiable)
            {
                // 漸進式 再根據這次成績 跟系統要下一份漸進考題
                //Debug.LogError("漸進式 再根據這次成績 跟系統要下一份漸進考題");
                m_cur_quest_list = m_CSVReader.getFlexibleQuestList(m_cur_fliex_enum, m_cur_quest_list);
                m_cur_quest_i = 0;
                m_cur_fliex_enum = m_cur_quest_list[0].fliex_lvl;
            }
            else
            {
                // 固定難度 則再要一次打亂順序的固定難度考題
                //Debug.LogError("固定難度 則再要一次打亂順序的固定難度考題");
                m_cur_quest_list = m_CSVReader.getLvlCycleList(m_lvl, 0, CSVReader.FliexibleLvlEnum.length, true);
                m_cur_quest_i = 0;
            }

        }        
    }

    string iCalcPointStr(int _correct, int _total)
    {
        float _final = 100 * (float.Parse(_correct.ToString()) / float.Parse(_total.ToString()));
        return _final.ToString("0.00");
    }


    bool m_is_show_end = false;
    public void showEndPoint()
    {
        if (m_cur_quest_i < 1) return;

        if (m_is_show_end) return;
        m_is_show_end = true;


        sf_tr_ans.gameObject.SetActive(false);
        cancelCounting();
        //int correct = m_cur_quest_list.Count - i_error_count;
        float final_point = 100*(float.Parse(i_correct_count.ToString())/ float.Parse(m_flex_quest_list.Count.ToString()));
        final_point = Mathf.Round(final_point);

        string str_time_left = showFullTimer(time_total-m_hold_fulltimer);// time_total

        sf_txt_header.text = final_point + "分 (" + i_correct_count + "/" + m_flex_quest_list.Count.ToString() + ")";
        if (p_is_flexiable) sf_txt_header.text = "共"+m_flex_quest_list.Count+"題 花費時間 " + str_time_left;
        if (str_time_left == "0") sf_txt_header.text = "本次結果";

        sf_img_fulltimer.gameObject.SetActive(false);
        sf_img_hint.gameObject.SetActive(false);
        sf_img_final_board.gameObject.SetActive(true);

        // 計算綜合分數  1級(100%) (10/30)
        string[] str_pct_arr = new string[] { "", "", "", "", "", "" };
        int[] i_correct_arr = new int[] { 0, 0, 0, 0, 0, 0 };
        int[] i_total_arr = new int[] { 0, 0, 0, 0, 0, 0 };
        for (int i = 0; i < m_flex_quest_list.Count; i++)
        {
            if (!m_flex_quest_list[i].is_answered) continue;

            if (m_flex_quest_list[i].lvl == 0)
            {
                i_total_arr[0]++;
                if (m_flex_quest_list[i].is_correct) i_correct_arr[0]++;
            }

            if (m_flex_quest_list[i].lvl == 1)
            {
                i_total_arr[1]++;
                if (m_flex_quest_list[i].is_correct) i_correct_arr[1]++;
            }

            if (m_flex_quest_list[i].lvl == 2)
            {
                i_total_arr[2]++;
                if (m_flex_quest_list[i].is_correct) i_correct_arr[2]++;
            }

            if (m_flex_quest_list[i].lvl == 3)
            {
                i_total_arr[3]++;
                if (m_flex_quest_list[i].is_correct) i_correct_arr[3]++;
            }

            if (m_flex_quest_list[i].lvl == 4)
            {
                i_total_arr[4]++;
                if (m_flex_quest_list[i].is_correct) i_correct_arr[4]++;
            }

            if (m_flex_quest_list[i].lvl == 5)
            {
                i_total_arr[5]++;
                if (m_flex_quest_list[i].is_correct) i_correct_arr[5]++;
            }
        }

        string _txt = "";
        for (int i = 0; i < 6; i++)
        {
            if (i_total_arr[i] == 0) continue;
            _txt += (i+1).ToString()+ "級正確率 "+ iCalcPointStr(i_correct_arr[i], i_total_arr[i])+"% (" + i_correct_arr[i] + "/" + i_total_arr[i] + ")\n";
        }

        sf_txt_final_board.text = _txt;

        m_AudioCtrl.playMusic(AudioCtrl.MusicClipsEnum.CGM2_Happy_Ktties_Short_Loop);

        getEnGameSelectChar().sf_anim_selected.Play("soot_selected_show_score");
        Debug.Log("-------------結束-------------");
    }

    int m_i_correct = -1;
    bool m_clicked = false;
    void toNextQuest()
    {
        Debug.Log("*********  toNextQuest: m_cur_quest_i="+ m_cur_quest_i);
        //if (m_cur_quest_i >= m_cur_quest_list.Count)
        //{
        //    Debug.LogError("m_cur_quest_i >= m_cur_quest_list.Count");
        //    showEndPoint();
        //    return;
        //}

        //if (p_is_flexiable)
        //{
            sf_txt_hint.text = (m_cur_quest_list[m_cur_quest_i].lvl + 1).ToString() + "級";//"["+m_flex_set_count+"]flv:"+ m_cur_quest_list[m_cur_quest_i].fliex_lvl+",lv" + (m_cur_quest_list[m_cur_quest_i].lvl+1).ToString();
        //}

        //Debug.LogError("sf_txt_hint.text ");

        if (!sf_tr_ans.gameObject.activeSelf) sf_tr_ans.gameObject.SetActive(true);
        m_clicked = false;

        for (int i = 0; i < sf_event.Length; i++)
        {
            sf_event[i].sf_img_bg.color = m_color_original;
        }

        sf_txt_header.text = m_cur_quest_list[m_cur_quest_i].spell;

        for (int i = 0; i < sf_event.Length; i++)
        {
            sf_event[i].sf_txt.text = "";
        }

        // 隨機選一格放正確
        //System.Random rnd_A = new System.Random();
        //m_i_correct = rnd_A.Next(0, 4);
        m_i_correct = UnityEngine.Random.Range(0, 4);

        // 正確答案 從三格有的選一個
        List<string> ans_list = new List<string>();
        ans_list.Add(m_cur_quest_list[m_cur_quest_i].chinese_01);
        if (m_cur_quest_list[m_cur_quest_i].chinese_02 != ""&& !m_cur_quest_list[m_cur_quest_i].chinese_02.Contains("{#LVL_"))
        {           
            ans_list.Add(m_cur_quest_list[m_cur_quest_i].chinese_02);
        }

        if (m_cur_quest_list[m_cur_quest_i].chinese_03 != "" && !m_cur_quest_list[m_cur_quest_i].chinese_03.Contains("{#LVL_") && !m_cur_quest_list[m_cur_quest_i].chinese_02.Contains("{#LVL_"))
        {
            ans_list.Add(m_cur_quest_list[m_cur_quest_i].chinese_03);
        }

        //System.Random rnd_B = new System.Random();
        //int i_B = rnd_B.Next(0, ans_list.Count);
        int i_B = UnityEngine.Random.Range(0, ans_list.Count);
        sf_event[m_i_correct].sf_txt.text = ans_list[i_B];
        m_cur_quest_list[m_cur_quest_i].show_ans = ans_list[i_B];

        // 另外三個隨機找錯的
        //System.Random rnd2 = new System.Random();
        //List<int> i_trap_list = new List<int>();
        //do
        //{
        //    int number = rnd2.Next(0, m_same_lvl_quest_list.Count);
        //    int dup = i_trap_list.IndexOf(number);
        //    if (dup < 0 && m_same_lvl_quest_list[number].original_order!= m_cur_quest_list[m_cur_quest_i].original_order)
        //    {
        //        i_trap_list.Add(number);
        //    }
        //} while (i_trap_list.Count < 3);

        int i_list_index = 0;
        for (int i = 0; i < sf_event.Length; i++)
        {
            if (sf_event[i].sf_txt.text != "") continue;

            //int ii = i_trap_list[i_list_index];
            // 這邊放三個壞的 p_is_cheat
            string q_txt = m_cur_quest_list[m_cur_quest_i].str_wrong_list[i_list_index];
            if (getMain().is_cheat_en_game)
            {
                q_txt = "";
            }

            sf_event[i].sf_txt.text = q_txt;
            i_list_index++;
        }

        m_is_stop_answer = false;
    }

    public bool getAnswerStopState()
    {
        return m_is_stop_answer;
    }

    
    string showFullTimer(int _timer)
    {
        if (_timer <= 0) _timer = 0;

        float min = Mathf.Floor( float.Parse(_timer.ToString()) / 60);
        if (min == 60) min = 0;
        string str_min = int.Parse(min.ToString()).ToString();
        int second = _timer % 60;
        if (second == 60) second = 0;
        string str_secoud = second.ToString();
        if (second < 10) str_secoud = "0" + str_secoud;

        string str_fulltimer = str_min + ":"+ str_secoud;
        sf_txt_fulltimer.text = str_fulltimer;

        return str_fulltimer;
    }

    Main m_main = null;
    Main getMain()
    {
        if (m_main == null) m_main = MyFinder.Instance.getMain();
        return m_main;
    }


    EnGameSelectChar m_EnGameSelectChar = null;
    EnGameSelectChar getEnGameSelectChar()
    {
        if (m_EnGameSelectChar == null) m_EnGameSelectChar = MyFinder.Instance.getEnGameSelectChar();
        return m_EnGameSelectChar;
    }
    

}
