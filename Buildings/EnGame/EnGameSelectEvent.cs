﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnGameSelectEvent : MonoBehaviour, IDebugOnTrigger
{
    [Header("Arrow")]
    [SerializeField] Transform sf_tr_arrow;
    [Header("Name")]
    [SerializeField] GameObject sf_obj_name;
    [Header("CharEnum")]
    public EnGameSelectChar.CharEnum m_char_enum;

    EnGameSelectChar m_EnGameSelectChar = null;

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }


    public void onEnter()
    {
        if (m_EnGameSelectChar == null) m_EnGameSelectChar = MyFinder.Instance.getEnGameSelectChar();


        m_EnGameSelectChar.showSelectedAll(false);
        showSelected(true);
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.select_char);
    }

    public void onExit()
    {
        showSelected(false);
    }

    public void chooseChar()
    {
        if (m_EnGameSelectChar == null) m_EnGameSelectChar = MyFinder.Instance.getEnGameSelectChar();
        m_EnGameSelectChar.showLvl();
        m_EnGameSelectChar.selectChar(m_char_enum);
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.choose_char);
    }

    public void showSelected(bool _is_active)
    {
        sf_obj_name.SetActive(_is_active);
        sf_tr_arrow.gameObject.SetActive(_is_active);
    }

    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        Debug.Log("chooseChar: ");
        chooseChar();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        Debug.Log("onEnter: ");
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        onExit();
    }
}
