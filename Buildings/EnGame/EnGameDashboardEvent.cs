﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class 
EnGameDashboardEvent : MonoBehaviour, IDebugOnTrigger
{
    [Header("Txt")]
    [SerializeField] Text sf_txt;
    public mainBoardEnum m_enmu;
    public enum mainBoardEnum
    {
        back_to_classroom,
        re_select,
        show_end_point,
        change_env,
        length
    }

    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null) m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        return m_AudioCtrl;
    }

    EnGameSelectChar m_EnGameSelectChar = null;
    EnGameSelectChar getEnGameSelectChar()
    {
        if (m_EnGameSelectChar == null) m_EnGameSelectChar = MyFinder.Instance.getEnGameSelectChar();
        return m_EnGameSelectChar;
    }

    BuildingEvents m_BuildingEvents = null;
    BuildingEvents getBuildingEvents()
    {
        if (m_BuildingEvents == null) m_BuildingEvents = this.gameObject.GetComponent<BuildingEvents>();
        return m_BuildingEvents;
    }

    EnGamePlayCtrl m_EnGamePlayCtrl = null;
    EnGamePlayCtrl getEnGamePlayCtrl()
    {
        if (m_EnGamePlayCtrl == null) m_EnGamePlayCtrl = MyFinder.Instance.getEnGamePlayCtrl();
        return m_EnGamePlayCtrl;
    }

    BuildingEnGameCtrl m_BuildingEnGameCtrl = null;
    BuildingEnGameCtrl getBuildingEnGameCtrl()
    {
        if (m_BuildingEnGameCtrl == null) m_BuildingEnGameCtrl = MyFinder.Instance.getBuildingEnGameCtrl();
        return m_BuildingEnGameCtrl;
    }


    public void txtColorEnter()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
        sf_txt.color = Color.yellow;
    }

    public void txtColorExit()
    {
        sf_txt.color = Color.white;
    }

    public void switchEnv()
    {
        getBuildingEnGameCtrl().switchEnv();
    }

    public void showEndPoint()
    {
        getEnGamePlayCtrl().showEndPoint();
    }

    // debug 要用的

    //MySystem m_mySystem = null;
    void IDebugOnTrigger.OnDebugClick()
    {
        
        switch (m_enmu)
        {
            case mainBoardEnum.back_to_classroom:
                getBuildingEvents().onClick();
                break;
            case mainBoardEnum.re_select:
                getEnGameSelectChar().resetToSelectChar();
                break;
            case mainBoardEnum.show_end_point:
                showEndPoint();
                break;
            case mainBoardEnum.change_env:
                switchEnv();
                break;
            case mainBoardEnum.length:
                break;
            default:
                break;
        }        
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        txtColorEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
        txtColorExit();
    }
}
