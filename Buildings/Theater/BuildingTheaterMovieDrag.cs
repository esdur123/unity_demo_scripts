using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class BuildingTheaterMovieDrag : MonoBehaviour
{

    //public GameObject mv_obj;
    public VideoClip shipin1, shipin2;
    public VideoPlayer videoPlayer;

    public Button play_button;   //播放按鈕
    public Button pause_button;  //暫停按鈕
    public Text mv_length_text;   //顯示視訊的長度
    public Slider slider_kuajin;
    private bool isDrag;
    private int hour, minute, second;  // 時  分  秒
    // Start is called before the first frame update

    public void init()
    {
        Debug.Log("slidet init");
        slider_kuajin.gameObject.SetActive(true);
        slider_kuajin.value = 0;
        slider_kuajin.gameObject.SetActive(false);
    }


    public void pauseHide()
    {
        slider_kuajin.gameObject.SetActive(false);
    }


    public void playShow()
    {
        slider_kuajin.gameObject.SetActive(true);
    }

    public void reStart()
    {
        slider_kuajin.gameObject.SetActive(true);
        slider_kuajin.value = 0;
        //Debug.Log(shipin1.length);
        ////按鈕1
        //button[0].onClick.AddListener(delegate
        //{
        //    Photoenlable(0);
        //    if (mv_obj.activeSelf == false)
        //    {
        //        OnorOff_MVjiemian(true);
        //    }

        //    videoPlayer.clip = shipin1;

        //});
        ////按鈕2
        //button[1].onClick.AddListener(delegate
        //{
        //    Photoenlable(1);
        //    if (mv_obj.activeSelf == false)
        //    {
        //        OnorOff_MVjiemian(true);
        //    }
        //    videoPlayer.clip = shipin2;

        //});
        ////按鈕3
        //button[2].onClick.AddListener(delegate
        //{
        //    Photoenlable(2);
        //});

    }
    //開啟或者關閉視訊介面
    //public void OnorOff_MVjiemian(bool tra)
    //{
    //    mv_obj.SetActive(tra);
    //}
    //視訊播放
    //public void MV_Play()
    //{
    //    play_button.gameObject.SetActive(false);
    //    pause_button.gameObject.SetActive(true);
    //    videoPlayer.Play();
    //}
    ////視訊暫停
    //public void MV_Pause()
    //{
    //    play_button.gameObject.SetActive(true);
    //    pause_button.gameObject.SetActive(false);
    //    videoPlayer.Pause();
    //}
    //顯示視訊的時間
    public void ShiPin_Length()
    {
        hour = (int)videoPlayer.time / 3600;
        minute = ((int)videoPlayer.time - hour * 3600) / 60;
        second = (int)videoPlayer.time - hour * 3600 - minute * 60;
        mv_length_text.text = string.Format("{0:D2}:{1:D2}:{2:D2}", hour, minute, second);
    }
    /// 修改視訊播放進度  
    private void ChangeVideoPlayTime()
    {
        if (isDrag == true)
        {
            videoPlayer.time = slider_kuajin.value * videoPlayer.clip.length;
        }
    }

    //修改進度條
    public void XiuGaiJinDuTiao()
    {
        if (!isDrag)
        {
            slider_kuajin.value = (float)(videoPlayer.time / videoPlayer.clip.length);
        }
    }
    //開始拖拽
    public void OnDragdrop()
    {
        isDrag = true;
        videoPlayer.Pause();
    }
    //結束拖拽
    public void OnEndDrag()
    {
        isDrag = false;
        videoPlayer.time = slider_kuajin.value * videoPlayer.clip.length;
        videoPlayer.Play();

    }
    // Update is called once per frame
    public void myUpdate()
    {
        if (videoPlayer.clip == null) return;
        if (isDrag) return;

        ChangeVideoPlayTime();
        XiuGaiJinDuTiao();
        ShiPin_Length();
        

    }
}