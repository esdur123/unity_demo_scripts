﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


public class BuildingTheaterCtrl : MonoBehaviour
{
    [Header("Youtube主插件")]
    [SerializeField] GameObject sf_obj_youtube_system;

    [Header("Youtube撥放器")]
    [SerializeField] LightShaft.Scripts.YoutubePlayer sf_youtube_player;

    [Header("舊撥放器")]
    [SerializeField] VideoPlayer sf_vp;
    [SerializeField] Transform sf_tr_vp;
    VideoClip m_clip = null;
    [SerializeField] GameObject sf_obj_vp_play;
    [SerializeField] GameObject sf_obj_vp_pause;

    [Header("長度控制")]
    [SerializeField] BuildingTheaterMovieDrag sf_movie_length_ctrl;

    bool is_use_clip = true;
    public void init()
    {
        sf_obj_youtube_system.SetActive(false);
    }


    public void checkInOut(bool _is_fadein,BuildingCtrl.buildings_order_enum _from_enum, BuildingCtrl.buildings_order_enum _to_enum)
    {
 
        if (_is_fadein && _from_enum == BuildingCtrl.buildings_order_enum.theater)
        {
    
            // 聲音先變小
            sf_youtube_player.videoPlayer.Pause();
            return;
        }


        // 離開電影院
        if (_from_enum == BuildingCtrl.buildings_order_enum.theater)
        {
        
            if (is_use_clip)
            {
   
                m_is_pause = true;
         
                sf_vp.Pause();
                //clip_player.gameObject.SetActive(false);
       
                return;
            }
       
            closeYoutubePlayer();
        }


        // 進入電影院
        if (_to_enum == BuildingCtrl.buildings_order_enum.theater)
        {
  
            if (is_use_clip)
            {
          
                sf_vp.gameObject.SetActive(true);
                sf_movie_length_ctrl.init();
                return;
            }

            

            initYoutubePlayer();
        }
    }

    public void clickVpRestart()
    {
        m_is_pause = false;
        sf_obj_vp_play.SetActive(false);
        sf_obj_vp_pause.SetActive(true);
        sf_vp.Stop();
        sf_vp.Play();

        sf_movie_length_ctrl.reStart();
    }

    bool m_is_pause = true;
    public void clickVpPlay()
    {
        m_is_pause = !m_is_pause;
        if (m_is_pause)
        {
            Debug.Log("Pause");
            sf_obj_vp_play.SetActive(true);
            sf_obj_vp_pause.SetActive(false);
            sf_vp.Pause();
            sf_movie_length_ctrl.pauseHide();
        }
        else
        {
            Debug.Log("Play");
            sf_obj_vp_play.SetActive(false);
            sf_obj_vp_pause.SetActive(true);
            sf_vp.Play();
            sf_movie_length_ctrl.playShow();
        }
        
    }

    // 電影院初始化 就會打開youtube player的update
    public void initYoutubePlayer()
    {
      
        sf_obj_youtube_system.SetActive(true);
        //Debug.Log("init BuildingTheaterCtrl");
        //sf_youtube_player.setMyUrl("https://youtu.be/pdaFJ-v4hlk");
        string[] url_arr = new string[] {
                 "https://youtu.be/89E4CYQnaAM"
                ,"https://youtu.be/sxUksYTqSSc"
                ,"https://youtu.be/C9Qie0RFSLQ"
            };
        
        sf_youtube_player.initPlayer(url_arr);
        
    }


    public void closeYoutubePlayer()
    {
        
        sf_obj_youtube_system.SetActive(false);
       
    }


    //public void init2()
    //{
    //    string[] url_arr = new string[] {
    //             "https://youtu.be/jW6AxaKjEmE"
    //            ,"https://youtu.be/fjoDrWvjpno"
    //            ,"https://youtu.be/QNbspIlO-UI"
    //        };
    //    sf_youtube_player.initPlayer(url_arr);
    //}


    Vector3 m_original_sacle = new Vector3();
    public void initRatio()
    {
        m_original_sacle = sf_tr_vp.localScale;
        
        sf_vp.url = "https://docs.google.com/uc?export=download&id=1wBRPh4D6neRk_lxyuWumWComFqK_o7p2";
        m_clip = sf_vp.clip;
        float clip_width = m_clip.width;
        float clip_height = m_clip.height;
        //Debug.Log("clip_width=" + width + ",clip_height=" + height);

        float clip_ratio = clip_height / clip_width;
        float player_ratio = m_original_sacle.z / m_original_sacle.x;
        //Debug.Log("clip_ratio="+ clip_ratio+ ", player_ratio="+ player_ratio);

        Vector3 new_scale = Vector3.one;

        if(player_ratio<= clip_ratio)
        {
            float new_width = m_original_sacle.z * clip_width / clip_height;
            new_scale = new Vector3(
                      new_width // 對應比例的寬
                    , m_original_sacle.y
                    , m_original_sacle.z // 高鎖死
                );
        }
        else
        {
            float new_height = m_original_sacle.x * clip_height / clip_width;
            new_scale = new Vector3(
                      m_original_sacle.x // 寬鎖死
                    , m_original_sacle.y
                    , new_height // 對應比例的高
                );
        }

        sf_tr_vp.localScale = new_scale;


        sf_vp.Pause();
    }


    public void myUpdate()
    {
        sf_movie_length_ctrl.myUpdate();
    }

}
