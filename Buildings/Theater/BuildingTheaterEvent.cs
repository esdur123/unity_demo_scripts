using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingTheaterEvent : MonoBehaviour, IDebugOnTrigger
{
    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }

    BuildingTheaterCtrl m_BuildingTheaterCtrl = null;
    public void clickVpPlay()
    {
        if (m_BuildingTheaterCtrl == null) m_BuildingTheaterCtrl = MyFinder.Instance.getBuildingTheaterCtrl();
        m_BuildingTheaterCtrl.clickVpPlay();
    }

    public void clickVpRestart()
    {
        if (m_BuildingTheaterCtrl == null) m_BuildingTheaterCtrl = MyFinder.Instance.getBuildingTheaterCtrl();
        m_BuildingTheaterCtrl.clickVpRestart();
    }

    public void onEnter()
    {
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.btn_hover);
    }


    // debug �n�Ϊ�
    void IDebugOnTrigger.OnDebugClick()
    {
        string obj_name = this.gameObject.name;
        switch (obj_name)
        {
            case "btn_theater_clip_play":
                clickVpPlay();
                break;
            case "btn_theater_clip_pause":
                clickVpPlay();
                break;
            case "btn_theater_clip_restart":
                clickVpRestart();
                break;
            default:
               
                break;
        }

    }

    void IDebugOnTrigger.OnDebugEnter()
    {
        onEnter();
    }

    void IDebugOnTrigger.OnDebugExit()
    {
    }
}
