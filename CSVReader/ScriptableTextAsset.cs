using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public sealed class ScriptableTextAsset : ScriptableObject
{
    [SerializeField]
    byte[] m_Bytes;

    public string text => Encoding.UTF8.GetString(m_Bytes);

    public byte[] bytes => (byte[])m_Bytes.Clone();

    public static ScriptableTextAsset CreateFromString(string text)
    {
        var asset = CreateInstance<ScriptableTextAsset>();
        asset.m_Bytes = Encoding.UTF8.GetBytes(text);
        return asset;
    }

    public static ScriptableTextAsset CreateFromBytes(byte[] bytes)
    {
        var asset = CreateInstance<ScriptableTextAsset>();
        asset.m_Bytes = (byte[])bytes.Clone();
        return asset;
    }

    public static ScriptableTextAsset CreateFromTextAsset(TextAsset text)
    {
        return CreateFromBytes(text.bytes);
    }

    public override string ToString()
    {
        return text;
    }
}
