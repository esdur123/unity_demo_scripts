﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Networking;
using System.Text;

/// <summary>
/// C:\Users\Nori\AppData\LocalLow\NoriCompany\SchoolVR_02a
/// </summary>

public class EnglishWords
{    
    public int lvl = -1;
    public string spell = "";
    public string word_type = "";
    public string chinese_01 = "";
    public string chinese_02 = "";
    public string chinese_03 = "";
    public string show_ans = "";
    public int original_order = -1;
    public List<string> str_wrong_list = new List<string>();
    public bool is_answered = false;
    public bool is_online = false;
    public bool is_correct = false;
    public CSVReader.FliexibleLvlEnum fliex_lvl = CSVReader.FliexibleLvlEnum.length;
}

public class CSVReader : MonoBehaviour 
{
    [Header("GameFlow")]
    [SerializeField] TextAsset sf_csv_gameflow;
    [Header("EnglishWords")]
    [SerializeField] TextAsset sf_csv_en_game;


    List<GameFlowUnit> m_game_flow_list = new List<GameFlowUnit>();
    GameFlowUnit m_cur_game_flow_unit = null;
    DialogueUnit m_dialogue_unit = null;
    public IEnumerator ReadCSV(bool _is_web_csv, string _web_gameflow_csv_url, string _web_en_words_csv_url)
    {
        yield return readGameFlowCSV(_is_web_csv, _web_gameflow_csv_url);
        //yield return readGameFlowCSV(false, Application.persistentDataPath + p_local_gameflow_path+ p_local_gameflow_filename);
        yield return readEnglishGameCSV(_is_web_csv, _web_en_words_csv_url);
        //yield return readEnglishGameCSV(false, Application.persistentDataPath + p_local_engame_path + p_local_engame_filename);
        yield break;
    }


    public void load2DTexture()
    {
        for (int i = 0; i < m_game_flow_list.Count; i++)
        {
            if (!(m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.init
                || m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.portal))
            {
                continue;
            }
      
            if (m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.init)
            {
                Texture2D my_texture = new Texture2D(1820,906, TextureFormat.RGBA32, false);
                byte[] byte_arr = MyFinder.Instance.getMyTools().loadImage(
                        m_game_flow_list[i].init_set.img_url
                    );
                my_texture.LoadImage(byte_arr);
                m_game_flow_list[i].init_set.texture360 = my_texture;

            }

            if (m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.portal)
            {
                Texture2D my_texture = new Texture2D(1820, 906, TextureFormat.RGBA32, false);
                byte[] byte_arr = MyFinder.Instance.getMyTools().loadImage(
                        m_game_flow_list[i].portal_set.img_url
                    );
                my_texture.LoadImage(byte_arr);
                m_game_flow_list[i].portal_set.texture360 = my_texture;

            }

        }

    }



    public IEnumerator waitWebTexture()
    {

        for (int i = 0; i < m_game_flow_list.Count; i++)
        {
            if (!(   m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.init
                || m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.portal))
            {
                continue;
            }

            UnityWebRequest www = null;

            if (m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.init)
            {
                //Debug.Log(m_game_flow_list[i].init_set.img_url);
                www = UnityWebRequestTexture.GetTexture(m_game_flow_list[i].init_set.img_url);

                MyFinder.Instance.getUIDebugCtrl().showDebugTxt("[init] "+m_game_flow_list[i].init_set.img_url, 0);

                List<string> my_debug_list = new List<string>();
                my_debug_list.Add("[init] " + m_game_flow_list[i].init_set.img_url);
                my_debug_list.Add("[getLocalPath] " + MyFinder.Instance.getMyTools().getLocalPath());
                getMyTools().SaveCSVToDeviceLocalText(
                     my_debug_list
                   , MyFinder.Instance.getMyTools().getLocalPath()//Application.persistentDataPath + "/"
                   , "debug01.txt"
                );
                //Debug.LogError(m_game_flow_list[i].init_set.img_url);
                //Debug.LogError("gameflow: "+m_game_flow_list[i].init_set.img_url);
            }

            if (m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.portal)
            {
                www = UnityWebRequestTexture.GetTexture(m_game_flow_list[i].portal_set.img_url);
                MyFinder.Instance.getUIDebugCtrl().showDebugTxt("[portal] " + m_game_flow_list[i].portal_set.img_url, 0);
                //Debug.LogError(m_game_flow_list[i].portal_set.img_url);
                //Debug.LogError("engame: " + m_game_flow_list[i].init_set.img_url);
                List<string> my_debug_list = new List<string>();
                my_debug_list.Add("[portal] " + m_game_flow_list[i].portal_set.img_url);
                my_debug_list.Add("[getLocalPath] " + MyFinder.Instance.getMyTools().getLocalPath());
                getMyTools().SaveCSVToDeviceLocalText(
                     my_debug_list
                   , MyFinder.Instance.getMyTools().getLocalPath()//Application.persistentDataPath + "/"
                   , "debug02.txt"
                );
            }
            
            
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                //Debug.LogError("圖片 讀取失敗");

                // 圖片是從網路上撈到Local再讀取的, 如果失敗那就代表local的圖片有問題, 直接10秒後退出
                MyFinder.Instance.getMySystem().showAutoExitHint("圖片 讀取失敗", 100);

                //MyFinder.Instance.getUIDebugCtrl().showDebugTxt(MyFinder.Instance.getMyTools().getLocalPath(),0);
                
                //MyFinder.Instance.getMain().is_web_csv = false;
                //yield break;
            }
            else
            {
                Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                //byte[]
                if (m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.init)
                {
                    m_game_flow_list[i].init_set.texture360 = myTexture;
                }

                if (m_game_flow_list[i].type == GameFlowUnit.GemeFlowTypeEnum.portal)
                {
                    m_game_flow_list[i].portal_set.texture360 = myTexture;
                }
            }
        }

        yield break;
    }


    List<string> m_csv_gameflow_row_data = new List<string>();
    //public string p_local_gameflow_path = "/";//"/school_vr/gameflow/";
    public string p_local_gameflow_filename = "gameflow.txt";
    IEnumerator readGameFlowCSV(bool _is_web_csv, string _web_gameflow_csv_url)
    {
        m_csv_gameflow_row_data = new List<string>();
        // sf_csv_gameflow.text.Split(new string[] { ",", "\n" }, StringSplitOptions.None);
        string[] data = new string[] { };


        if (_is_web_csv)
        {
            UnityWebRequest www = UnityWebRequest.Get(_web_gameflow_csv_url);
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {
                //Debug.Log(www.error);
                //MyFinder.Instance.getMySystem().showAutoExitHint("塔防CSV 網路讀取失敗", 10);
                MyFinder.Instance.getUIDebugCtrl().showBeforeInfoInitTxt("塔防CSV 讀取本地端");
                MyFinder.Instance.getMain().is_web_csv = false;
                yield break;            
            }
            else
            {
                // Show results as text
                //Debug.Log(www.downloadHandler.text);

                // Or retrieve results as binary data
                byte[] results = www.downloadHandler.data;
                //File.AppendAllText(Application.persistentDataPath+"/nori/aaa.csv", BitConverter.ToString(results));
                var converted_data = ScriptableTextAsset.CreateFromBytes(results);
                data = converted_data.text.Split(new string[] { ",", "\n" }, StringSplitOptions.None);
            }
        }
        else
        {
            //　讀取存在Local的txt文字檔 (我們自己寫入的)
            List<string> _temp_list = new List<string>();
            string[] txt_str_arr = File.ReadAllLines(_web_gameflow_csv_url);
            for (int i = 0; i < txt_str_arr.Length; i++)
            {
                if (txt_str_arr[i].Contains(","))
                {
                    string[] _split_arr = txt_str_arr[i].Split(new string[] { ",", "\n" }, StringSplitOptions.None);
                    for (int j = 0; j < _split_arr.Length; j++)
                    {
                        if (_split_arr[j] != "")
                        {
                            _temp_list.Add(_split_arr[j]);
                        }
                    }
                }
                else
                {
                    if (txt_str_arr[i] != "")
                    {
                        _temp_list.Add(txt_str_arr[i]);
                    }
                }
            }

            data = new string[_temp_list.Count];
            for (int i = 0; i < _temp_list.Count; i++)
            {
                data[i] = _temp_list[i];
            }

            // Editor檔案直接拉進來
            //data = sf_csv_gameflow.text.Split(new string[] { ",", "\n" }, StringSplitOptions.None);
        }


        string str_image_name_start = "img_";
        int index_texture = 0;
        string str_image_name_end = ".jpg";
        GameFlowUnit.GemeFlowTypeEnum csv_type = GameFlowUnit.GemeFlowTypeEnum.none;
        for (int i = 0; i < data.Length; i++)
        {

            //Debug.Log(i+ "　data = "+ data[i]);
            if (data[i].Contains("{#Comment#}"))
            {
                Debug.Log("{#Comment#}");
                continue;
            }

            // 匯出CSV用的
            string[] rowDataTemp = new string[2];

            // 因為CSV是照順序 所以dialogue的就從上往下順序跑

            /// init
            if (data[i] == "{#Init_Start#}")
            {
                // 匯出CSV用的
                //string[] rowDataTemp = new string[2];
                rowDataTemp[0] = "{#Init_Start#}";

                m_cur_game_flow_unit = new GameFlowUnit();
                m_cur_game_flow_unit.init_set = new InitSet();
                csv_type = GameFlowUnit.GemeFlowTypeEnum.init;
                m_cur_game_flow_unit.type = GameFlowUnit.GemeFlowTypeEnum.init;
            }

            if (csv_type == GameFlowUnit.GemeFlowTypeEnum.init)
            {
                if (data[i] == "{#Image_Url#}")
                {
                    // 匯出CSV用的
                    rowDataTemp[0] = "{#Image_Url#}";
                    string url = data[i + 1];
                    url = url.Replace("\r", "");

                    // 直接下載到local
                    string local_path = "";

                    
                    local_path = MyFinder.Instance.getMyTools().getLocalPath()
                                        + str_image_name_start + index_texture.ToString() + str_image_name_end;

                    yield return MyFinder.Instance.getMyTools().dolwloadImageToLocal(
                                url
                            , local_path
                        );
               

                    index_texture++;

                    rowDataTemp[1] = local_path;
                    m_cur_game_flow_unit.init_set.img_url = local_path;                   
                }

                if (data[i] == "{#Init_End#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Init_End#}";


                    m_game_flow_list.Add(m_cur_game_flow_unit);
                    csv_type = GameFlowUnit.GemeFlowTypeEnum.none;
                }
            }

            /// Dialogue
            if (data[i] == "{#Dialogue_Start#}")
            {
                // 匯出CSV用的
                //string[] rowDataTemp = new string[2];
                rowDataTemp[0] = "{#Dialogue_Start#}";

                m_cur_game_flow_unit = new GameFlowUnit();
                csv_type = GameFlowUnit.GemeFlowTypeEnum.dialogue;
                m_cur_game_flow_unit.type = GameFlowUnit.GemeFlowTypeEnum.dialogue;
                m_cur_game_flow_unit.dialogue_sets = new DialogueSets();
                m_cur_game_flow_unit.dialogue_sets.all_char_name_list = new List<string>();
                m_cur_game_flow_unit.dialogue_sets.unit_list = new List<DialogueUnit>();

            }

            if (csv_type == GameFlowUnit.GemeFlowTypeEnum.dialogue)
            {

                if (data[i] == "{#Position#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Position#}";
                    rowDataTemp[1] = data[i + 1];

                    string[] _arr_pos = data[i + 1].Split('#');
                    m_cur_game_flow_unit.dialogue_sets.pos = new Vector3(
                              float.Parse(_arr_pos[0])
                            , float.Parse(_arr_pos[1])
                            , float.Parse(_arr_pos[2])
                        );
                }

                if (data[i] == "{#First_All_Show#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#First_All_Show#}";
                    rowDataTemp[1] = data[i + 1];


                    string[] _first_show_all_arr = data[i + 1].Split('#');
                    bool _is_show = false;
                    if (_first_show_all_arr[0] == "true") _is_show = true;
                    if (_first_show_all_arr[0] == "false") _is_show = false;

                    m_cur_game_flow_unit.dialogue_sets.first_all_show = _is_show;
                }

                if (data[i] == "{#Names#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Names#}";
                    rowDataTemp[1] = data[i + 1];

                    string[] _arr_names = data[i + 1].Split('#');
                    for (int j = 0; j < _arr_names.Length; j++)
                    {
                        if (_arr_names[j] == "\r") continue;
                        if (_arr_names[j] == "") continue;
                        if (_arr_names[j] == "\n") continue;
                        Debug.Log("@@@@@@-> " + _arr_names[j]);
                        m_cur_game_flow_unit.dialogue_sets.all_char_name_list.Add(_arr_names[j]);
                    }
                }

                if (data[i] == "{#Attend#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Attend#}";
                    rowDataTemp[1] = data[i + 1];


                    m_dialogue_unit = new DialogueUnit(); // attend的時候new一個
                    m_dialogue_unit.i_show_char_list = new List<int>();
                    string[] _arr_attend = data[i + 1].Split('#');

                    for (int j = 0; j < _arr_attend.Length; j++)
                    {
                        if (_arr_attend[j] == "\r") continue;
                        string _show_name = _arr_attend[j];
                        for (int k = 0; k < m_cur_game_flow_unit.dialogue_sets.all_char_name_list.Count; k++)
                        {
                            if (m_cur_game_flow_unit.dialogue_sets.all_char_name_list[k] != _show_name) continue;
                            //Debug.Log("add "+_show_name + " = "+k);
                            m_dialogue_unit.i_show_char_list.Add(k);
                            // 這邊很怪 CSV 的attend 一定要 aaa#bb#ccc# 最後補一個#號才正確0.0!? =>因為CSV字尾自動補 \r
                        }
                    }
                }

                if (data[i] == "{#Speaker#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Speaker#}";
                    rowDataTemp[1] = data[i + 1];


                    string[] _arr_speaker = data[i + 1].Split('#');
                    for (int k = 0; k < m_cur_game_flow_unit.dialogue_sets.all_char_name_list.Count; k++)
                    {
                        if (m_cur_game_flow_unit.dialogue_sets.all_char_name_list[k] != _arr_speaker[0]) continue;

                        m_dialogue_unit.i_speaker = k;
                    }
                }

                if (data[i] == "{#FirstContent#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#FirstContent#}";
                    rowDataTemp[1] = data[i + 1];


                    // 承 上面Attend的 m_dialogue_unit
                    string[] _arr_first_content = data[i + 1].Split('#');
                    m_cur_game_flow_unit.dialogue_sets.first_content = _arr_first_content[0];
                }

                if (data[i] == "{#Content#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Content#}";
                    rowDataTemp[1] = data[i + 1];


                    // 承 上面Attend的 m_dialogue_unit
                    m_dialogue_unit.content = data[i + 1];
                    // 加一個對話近來
                    m_cur_game_flow_unit.dialogue_sets.unit_list.Add(m_dialogue_unit);
                }

                if (data[i] == "{#Dialogue_End#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Dialogue_End#}";


                    m_game_flow_list.Add(m_cur_game_flow_unit);
                    csv_type = GameFlowUnit.GemeFlowTypeEnum.none;
                }
            }

            /// Portal
            if (data[i] == "{#Portal_Start#}")
            {
                // 匯出CSV用的
                //string[] rowDataTemp = new string[2];
                rowDataTemp[0] = "{#Portal_Start#}";


                m_cur_game_flow_unit = new GameFlowUnit();
                m_cur_game_flow_unit.portal_set = new PortalSet();
                csv_type = GameFlowUnit.GemeFlowTypeEnum.portal;
                m_cur_game_flow_unit.type = GameFlowUnit.GemeFlowTypeEnum.portal;
            }

            if (csv_type == GameFlowUnit.GemeFlowTypeEnum.portal)
            {
                if (data[i] == "{#Position#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Position#}";
                    rowDataTemp[1] = data[i + 1];


                    // 承 上面Attend的 m_dialogue_unit
                    string[] _arr_pos = data[i + 1].Split('#');
                    m_cur_game_flow_unit.portal_set.pos = new Vector3(
                              float.Parse(_arr_pos[0])
                            , float.Parse(_arr_pos[1])
                            , float.Parse(_arr_pos[2])
                        );
                }

                if (data[i] == "{#Image_Url#}")
                {
                    // 匯出CSV用的
                    rowDataTemp[0] = "{#Image_Url#}";
                    string url = data[i + 1];
                    url = url.Replace("\r", "");

                    // 直接下載到local
                    string local_path = MyFinder.Instance.getMyTools().getLocalPath()
                                        + str_image_name_start + index_texture.ToString() + str_image_name_end;

                    yield return MyFinder.Instance.getMyTools().dolwloadImageToLocal(
                              url
                            , local_path
                        );
                    index_texture++;

                    m_cur_game_flow_unit.portal_set.img_url = local_path;
                    rowDataTemp[1] = local_path;
                    
                }

                if (data[i] == "{#Portal_End#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Portal_End#}";


                    m_game_flow_list.Add(m_cur_game_flow_unit);
                    csv_type = GameFlowUnit.GemeFlowTypeEnum.none;
                }
            }

            /// tower_game 66666666666666666666
            if (data[i] == "{#Tower_Start#}")
            {
                // 匯出CSV用的
                //string[] rowDataTemp = new string[2];
                rowDataTemp[0] = "{#Tower_Start#}";
                rowDataTemp[1] = data[i + 1];


                m_cur_game_flow_unit = new GameFlowUnit();
                m_cur_game_flow_unit.tower_game_set = new TowerGameSet();
                csv_type = GameFlowUnit.GemeFlowTypeEnum.tower_game;
                m_cur_game_flow_unit.type = GameFlowUnit.GemeFlowTypeEnum.tower_game;
            }

            if (csv_type == GameFlowUnit.GemeFlowTypeEnum.tower_game)
            {
                if (data[i] == "{#Position#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Position#}";
                    rowDataTemp[1] = data[i + 1];


                    string[] _arr_pos = data[i + 1].Split('#');
                    m_cur_game_flow_unit.tower_game_set.pos = new Vector3(
                              float.Parse(_arr_pos[0])
                            , float.Parse(_arr_pos[1])
                            , float.Parse(_arr_pos[2])
                        );
                }

                if (data[i] == "{#Level#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Level#}";
                    rowDataTemp[1] = data[i + 1];


                    string[] _arr_tower_lvl = data[i + 1].Split('#');
                    m_cur_game_flow_unit.tower_game_set.Level = int.Parse(_arr_tower_lvl[0]);
                }

                if (data[i] == "{#Questions#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Questions#}";
                    rowDataTemp[1] = data[i + 1];


                    string[] _arr_tower_q = data[i + 1].Split('#');
                    m_cur_game_flow_unit.tower_game_set.questions = int.Parse(_arr_tower_q[0]);
                }

                if (data[i] == "{#Tower_End#}")
                {
                    // 匯出CSV用的
                    //string[] rowDataTemp = new string[2];
                    rowDataTemp[0] = "{#Tower_End#}";


                    m_game_flow_list.Add(m_cur_game_flow_unit);
                    csv_type = GameFlowUnit.GemeFlowTypeEnum.none;
                }
            }


            // 匯出CSV用的
            for (int j = 0; j < rowDataTemp.Length; j++)
            {
                m_csv_gameflow_row_data.Add(rowDataTemp[j]);
            }
            
        }


        if (_is_web_csv)
        {
            getMyTools().SaveCSVToDeviceLocalText(
                 m_csv_gameflow_row_data
               , MyFinder.Instance.getMyTools().getLocalPath()//Application.persistentDataPath + "/"
               , p_local_gameflow_filename
            );
        }

        yield break;
    }




    public List<GameFlowUnit> getCurGameFlow()
    {
        return m_game_flow_list;
    }


    //int[] m_lvl_count_arr = new int[] { 0,0,0,0,0,0};
    public List<EnglishWords> m_lvl_1_en_list = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_2_en_list = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_3_en_list = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_4_en_list = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_5_en_list = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_6_en_list = new List<EnglishWords>();
    List<string> m_csv_engame_row_data = new List<string>();
    public List<EnglishWords> m_all_en_list = new List<EnglishWords>();
    //public string p_local_engame_path = "/";// "/school_vr/en_game/";
    public string p_local_engame_filename = "engame.txt";
    IEnumerator readEnglishGameCSV(bool _is_web_csv, string _web_en_words_csv_url)
    {
        m_csv_engame_row_data = new List<string>();
        //string[] data = sf_csv_en_game.text.Split(new string[] { ",", "\n" }, StringSplitOptions.None);
        string[] data = null;
        if (_is_web_csv)
        {
            UnityWebRequest www = UnityWebRequest.Get(_web_en_words_csv_url);
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {
                //Debug.Log(www.error);
                //MyFinder.Instance.getMySystem().showAutoExitHint("英語CSV 網路讀取失敗", 10);
                MyFinder.Instance.getUIDebugCtrl().showBeforeInfoInitTxt("英語CSV 讀取本地端");
                MyFinder.Instance.getMain().is_web_csv = false;
                yield break;
            }
            else
            {
                // Show results as text
                //Debug.Log(www.downloadHandler.text);

                // Or retrieve results as binary data
                byte[] results = www.downloadHandler.data;
                var converted_data = ScriptableTextAsset.CreateFromBytes(results);
                data = converted_data.text.Split(new string[] { ",", "\n" }, StringSplitOptions.None);
            }
        }
        else
        {
            //　讀取存在Local的txt文字檔 (我們自己寫入的)
            List<string> _temp_list = new List<string>();
            string[] txt_str_arr = File.ReadAllLines(_web_en_words_csv_url);
            for (int i = 0; i < txt_str_arr.Length; i++)
            {
                if (txt_str_arr[i].Contains(","))
                {
                    string[] _split_arr = txt_str_arr[i].Split(new string[] { ",", "\n" }, StringSplitOptions.None);
                    for (int j = 0; j < _split_arr.Length; j++)
                    {
                        if (_split_arr[j] != "")
                        {
                            _temp_list.Add(_split_arr[j]);
                        }
                    }
                }
                else
                {
                    if (txt_str_arr[i] != "")
                    {
                        _temp_list.Add(txt_str_arr[i]);
                    }
                }
            }

            data = new string[_temp_list.Count];
            for (int i = 0; i < _temp_list.Count; i++)
            {
                data[i] = _temp_list[i];
            }

            // Editor檔案直接拉進來
            //data = sf_csv_en_game.text.Split(new string[] { ",", "\n" }, StringSplitOptions.None);
        }


        m_all_en_list   = new List<EnglishWords>();
        m_lvl_1_en_list = new List<EnglishWords>();
        m_lvl_2_en_list = new List<EnglishWords>();
        m_lvl_3_en_list = new List<EnglishWords>();
        m_lvl_4_en_list = new List<EnglishWords>();
        m_lvl_5_en_list = new List<EnglishWords>();
        m_lvl_6_en_list = new List<EnglishWords>();
        
        for (int i = 0; i < data.Length; i++)
        {

            if (!(
                   data[i] == "{#LVL_1#}" 
                || data[i] == "{#LVL_2#}" 
                || data[i] == "{#LVL_3#}" 
                || data[i] == "{#LVL_4#}" 
                || data[i] == "{#LVL_5#}" 
                || data[i] == "{#LVL_6#}"
               ))
            {
                continue;
            }

            //if (data[i + 1] == "")
            //{
            //    Debug.LogError("spell empty:"+ data[i + 3]);
            //}

            EnglishWords en = new EnglishWords();
            en.spell      = strFilter(data[i + 1]);
            en.word_type  = strFilter(data[i + 2]);
            en.chinese_01 = strFilter(data[i + 3]);
            en.chinese_02 = strFilter(data[i + 4]);
            en.chinese_03 = strFilter(data[i + 5]);
            if (en.chinese_02 == "")
            {
                en.chinese_03 = "";
            }

            // 匯出CSV用的
            string[] rowDataTemp = new string[6];
            rowDataTemp[0] = data[i];
            rowDataTemp[1] = en.spell;
            rowDataTemp[2] = en.word_type;
            rowDataTemp[3] = en.chinese_01;
            rowDataTemp[4] = en.chinese_02;
            rowDataTemp[5] = en.chinese_02;

            if (_is_web_csv)
            {
                for (int j = 0; j < rowDataTemp.Length; j++)
                {
                    m_csv_engame_row_data.Add(rowDataTemp[j]); // 匯出CSV用的
                }
            }


            if (data[i] == "{#LVL_1#}")
            {
                en.lvl = 0;
                m_lvl_1_en_list.Add(en);
            }
            if (data[i] == "{#LVL_2#}")
            {
                en.lvl = 1;
                m_lvl_2_en_list.Add(en);
            }
            if (data[i] == "{#LVL_3#}")
            {
                en.lvl = 2;
                m_lvl_3_en_list.Add(en);
            }
            if (data[i] == "{#LVL_4#}")
            {
                en.lvl = 3;
                m_lvl_4_en_list.Add(en);
            }
            if (data[i] == "{#LVL_5#}")
            {
                en.lvl = 4;
                m_lvl_5_en_list.Add(en);
            }
            if (data[i] == "{#LVL_6#}")
            {
                en.lvl = 5;
                m_lvl_6_en_list.Add(en);
            }

            en.original_order = m_all_en_list.Count;
            m_all_en_list.Add(en);

            // 不重複


        }

        // 存入local檔
        if (_is_web_csv)
        {
            getMyTools().SaveCSVToDeviceLocalText(
                 m_csv_engame_row_data
               , MyFinder.Instance.getMyTools().getLocalPath()
               , p_local_engame_filename
            );
        }


        // 每次開機就打亂順序
        m_all_en_list   = getMyTools().ShuffleEnglishWordsList(m_all_en_list);
        m_lvl_1_en_list = getMyTools().ShuffleEnglishWordsList(m_lvl_1_en_list);
        m_lvl_2_en_list = getMyTools().ShuffleEnglishWordsList(m_lvl_2_en_list);
        m_lvl_3_en_list = getMyTools().ShuffleEnglishWordsList(m_lvl_3_en_list);
        m_lvl_4_en_list = getMyTools().ShuffleEnglishWordsList(m_lvl_4_en_list);
        m_lvl_5_en_list = getMyTools().ShuffleEnglishWordsList(m_lvl_5_en_list);
        m_lvl_6_en_list = getMyTools().ShuffleEnglishWordsList(m_lvl_6_en_list);

        // 另創六組 有附上3組錯誤(當考題)的
        createEnListWith3WrongQuest();

        yield break;
        //getLvlQuest(0,20);
    }


    /// <summary>
    /// 有附答案的考題
    /// </summary>
    public List<EnglishWords> m_lvl_1_en_list_with_3_wrongs = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_2_en_list_with_3_wrongs = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_3_en_list_with_3_wrongs = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_4_en_list_with_3_wrongs = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_5_en_list_with_3_wrongs = new List<EnglishWords>();
    public List<EnglishWords> m_lvl_6_en_list_with_3_wrongs = new List<EnglishWords>();
    void createEnListWith3WrongQuest()
    {
        m_lvl_1_en_list_with_3_wrongs = getLvlQuest(0, 0, true);
        m_lvl_2_en_list_with_3_wrongs = getLvlQuest(1, 0, true);
        m_lvl_3_en_list_with_3_wrongs = getLvlQuest(2, 0, true);
        m_lvl_4_en_list_with_3_wrongs = getLvlQuest(3, 0, true);
        m_lvl_5_en_list_with_3_wrongs = getLvlQuest(4, 0, true);
        m_lvl_6_en_list_with_3_wrongs = getLvlQuest(5, 0, true);
    }

    

    MyTools m_MyTools = null;
    MyTools getMyTools()
    {
        if (m_MyTools == null) m_MyTools = MyFinder.Instance.getMyTools();
        return m_MyTools;
    }


    string strFilter(string _str)
    {
        string s = _str;
        if (s == "{#LVL_1#}") s = "";
        if (s == "{#LVL_2#}") s = "";
        if (s == "{#LVL_3#}") s = "";
        if (s == "{#LVL_4#}") s = "";
        if (s == "{#LVL_5#}") s = "";
        if (s == "{#LVL_6#}") s = "";
        return s;
    }

    public List<EnglishWords> getLvlEnList()
    {
        return m_lvl_list;
    }

    public List<EnglishWords> getAllEnList()
    {
        return m_all_en_list;
    }

    int m_single_quest_select_i = 3; // 一題有幾個錯的
    List<EnglishWords> m_lvl_list = new List<EnglishWords>();
    public List<EnglishWords> getLvlQuest(int _lvl, int _test_count, bool _is_get_lvl_all)
    {
        //Debug.LogError("_lvl="+ _lvl);
        m_lvl_list = new List<EnglishWords>();
        List<EnglishWords> result_list = new List<EnglishWords>();
        if (_lvl == 0) m_lvl_list = m_lvl_1_en_list;
        if (_lvl == 1) m_lvl_list = m_lvl_2_en_list;
        if (_lvl == 2) m_lvl_list = m_lvl_3_en_list;
        if (_lvl == 3) m_lvl_list = m_lvl_4_en_list;
        if (_lvl == 4) m_lvl_list = m_lvl_5_en_list;
        if (_lvl == 5) m_lvl_list = m_lvl_6_en_list;

        
        int test_count = _test_count;
        if (_is_get_lvl_all) 
        {
            test_count = m_lvl_list.Count;
        }

        System.Random rnd = new System.Random();
        List<int> i_list = new List<int>();
        do
        {
            int number = rnd.Next(0, m_lvl_list.Count);
            int dup = i_list.IndexOf(number);
            if (dup < 0)
            {
                i_list.Add(number);
            }
        } while (i_list.Count < test_count);


        // 每題給三個錯誤答案 i_list 就是目前挑出來的考題
        //Debug.LogError("i_list.Count =" + i_list.Count);
        for (int i = 0; i < i_list.Count; i++)
        {
            EnglishWords en = m_lvl_list[i_list[i]];
            en.str_wrong_list.Clear();

            
            // 找出目前這題 相同字首+相同lvl 的List
            //string the_first_world = en.spell.Substring(0, 1).ToUpper();
            //List<int> i_same_lvl_first_world_list = new List<int>();
            //for (int j = 0; j < m_lvl_list.Count; j++)
            //{
            //    if (m_lvl_list[j].spell.ToLower() == en.spell.ToLower()) continue;
            //    if (the_first_world != m_lvl_list[j].spell.Substring(0, 1).ToUpper()) continue;
            //    i_same_lvl_first_world_list.Add(j);
            //}

            // 另外三個隨機找錯的
            List<int> j_trap_list = new List<int>();
            do
            {
                int number2 = UnityEngine.Random.Range(0, m_lvl_list.Count);
                int dup2 = j_trap_list.IndexOf(number2);

                if (dup2 < 0
                     && number2 != i_list[i]

                   )
                {

                    j_trap_list.Add(number2);
                }
            } while (j_trap_list.Count < 3);
            //do
            //{
            //    int number2 = UnityEngine.Random.Range(0, i_same_lvl_first_world_list.Count);
            //    int dup2 = j_trap_list.IndexOf(number2);

            //    if (dup2 < 0)
            //    {
            //        j_trap_list.Add(i_same_lvl_first_world_list[number2]);
            //    }
            //} while (j_trap_list.Count < 3);


            for (int k = 0; k < j_trap_list.Count; k++) // 給三個錯的
            {

                int ii = j_trap_list[k];

                //Debug.Log("j_trap_list[" + k+"] = "+ ii);
                //Debug.Log("k=" + k + ",ii=" + ii + ",m_lvl_listCount=" + m_lvl_list.Count);
                m_lvl_list[ii].is_answered = false;
                m_lvl_list[ii].is_online = false;
                m_lvl_list[ii].is_correct = false;
                en.str_wrong_list.Add(m_lvl_list[ii].chinese_01);
                //i_list_index++;
            }


            //Debug.Log("SPELL: "+en.spell);

            result_list.Add(en);
        }



        //for (int i = 0; i < result_list.Count; i++)
        //{
        //    Debug.Log("[" + i + "] **spell: " + result_list[i].spell);
        //    Debug.Log("[" + i + "] **ans: " + result_list[i].show_ans);
        //    Debug.Log("[" + i + "] **w0: " + result_list[i].str_wrong_list[0]);
        //    Debug.Log("[" + i + "] **w1: " + result_list[i].str_wrong_list[1]);
        //    Debug.Log("[" + i + "] **w2: " + result_list[i].str_wrong_list[2]);
        //}


        return result_list;
    }







    /// <summary>
    /// 動態出題 要有自己的init 和 clear?
    /// 1-1 = 1*8                   up:全對
    /// 2-1 = 1*2 + 2*6 keep: 1*2對 up:全對
    /// 3-1 = 2*2 + 3*6 keep: 2*2對 up:全對
    /// 4-1 = 3*2 + 4*6 keep: 3*2對 up:全對
    /// 5-1 = 4*2 + 5*6 keep: 4*2對 up:全對
    /// 6-1 = 5*2 + 6*6 keep: 5*2對 up:全對
    /// </summary>
    public enum FliexibleLvlEnum
    {
        f_1_1,
        f_2_1,
        f_3_1,
        f_4_1,
        f_5_1,
        f_6_1,
        length
    }


    //public int p_flex_onetime_count = 8; // 8題為一組 666
    public List<EnglishWords> getFlexibleQuestList(FliexibleLvlEnum _flex_lvl, List<EnglishWords> _last_result_list)
    {
        //Debug.LogError(_flex_lvl);
        FliexibleLvlEnum new_lvl = _flex_lvl;
        if (_last_result_list != null) // 沒有上一次的成績 那就給指定的lvl開始
        {
            //new_lvl = "評分後面 重新定義..";
            int[] arr = analyzeLastFlexibleQuest(_last_result_list);
            switch (new_lvl)
            {
                case FliexibleLvlEnum.f_1_1: // 1-1 = 1*8       keep: --    up:全對
                    if (arr[0] == 8) new_lvl = FliexibleLvlEnum.f_2_1;
                    break;
                case FliexibleLvlEnum.f_2_1: // 2-1 = 1*2 + 2*6 keep: 1*2對 up:全對
                    if (arr[0] < 2) new_lvl = FliexibleLvlEnum.f_1_1;
                    if (arr[0] == 2 && arr[1] == 6) new_lvl = FliexibleLvlEnum.f_3_1;
                    break;
                case FliexibleLvlEnum.f_3_1: // 3-1 = 2*2 + 3*6 keep: 2*2對 up:全對
                    if (arr[1] < 2) new_lvl = FliexibleLvlEnum.f_2_1;
                    if (arr[1] == 2 && arr[2] == 6) new_lvl = FliexibleLvlEnum.f_4_1;
                    break;
                case FliexibleLvlEnum.f_4_1: // 4-1 = 3*2 + 4*6 keep: 3*2對 up:全對
                    if (arr[2] < 2) new_lvl = FliexibleLvlEnum.f_3_1;
                    if (arr[2] == 2 && arr[3] == 6) new_lvl = FliexibleLvlEnum.f_5_1;
                    break;
                case FliexibleLvlEnum.f_5_1: // 5-1 = 4*2 + 5*6 keep: 4*2對 up:全對
                    if (arr[3] < 2) new_lvl = FliexibleLvlEnum.f_4_1;
                    if (arr[3] == 2 && arr[4] == 6) new_lvl = FliexibleLvlEnum.f_6_1;
                    break;
                case FliexibleLvlEnum.f_6_1: // 6-1 = 5*2 + 6*6 keep: 5*2對 up:全對
                    if (arr[4] < 2) new_lvl = FliexibleLvlEnum.f_5_1;
                    break;
                case FliexibleLvlEnum.length:
                    break;
                default:
                    break;
            }
        }

        List<EnglishWords> new_list = new List<EnglishWords>();
        switch (new_lvl)
        {
            case FliexibleLvlEnum.f_1_1: // 1-1 = 1*8       keep: --    up:全對
                new_list = getLvlCycleList(0, 8, FliexibleLvlEnum.f_1_1, false);
                break;
            case FliexibleLvlEnum.f_2_1: // 2-1 = 1*2 + 2*6 keep: 1*2對 up:全對
                new_list = getLvlCycleList(0, 2, FliexibleLvlEnum.f_2_1, false);
                new_list.AddRange(getLvlCycleList(1, 6, FliexibleLvlEnum.f_2_1, false));
                break;
            case FliexibleLvlEnum.f_3_1: // 3-1 = 2*2 + 3*6 keep: 2*2對 up:全對
                new_list = getLvlCycleList(1, 2, FliexibleLvlEnum.f_3_1, false);
                new_list.AddRange(getLvlCycleList(2, 6, FliexibleLvlEnum.f_3_1, false));
                break;
            case FliexibleLvlEnum.f_4_1: // 4-1 = 3*2 + 4*6 keep: 3*2對 up:全對
                new_list = getLvlCycleList(2, 2, FliexibleLvlEnum.f_4_1, false);
                new_list.AddRange(getLvlCycleList(3, 6, FliexibleLvlEnum.f_4_1, false));
                break;
            case FliexibleLvlEnum.f_5_1: // 5-1 = 4*2 + 5*6 keep: 4*2對 up:全對
                new_list = getLvlCycleList(3, 2, FliexibleLvlEnum.f_5_1, false);
                new_list.AddRange(getLvlCycleList(4, 6, FliexibleLvlEnum.f_5_1, false));
                break;
            case FliexibleLvlEnum.f_6_1: // 6-1 = 5*2 + 6*6 keep: 5*2對 up:全對
                new_list = getLvlCycleList(4, 2, FliexibleLvlEnum.f_6_1, false);
                new_list.AddRange(getLvlCycleList(5, 6, FliexibleLvlEnum.f_6_1, false));
                break;
            case FliexibleLvlEnum.length:
                break;
            default:
                break;
        }

        new_list = getMyTools().ShuffleEnglishWordsList(new_list);
        return new_list;
    }


    //List<EnglishWords> addFlexLvl(FliexibleLvlEnum _lvl, ref List<EnglishWords> _list)
    //{
    //    for (int i = 0; i < _list.Count; i++)
    //    {
    //        _list[i].fliex_lvl = _lvl;
    //    }
    //    return _list;
    //}

    int[] analyzeLastFlexibleQuest(List<EnglishWords> _last_result_list)
    {
        int[] arr = new int[] { 0, 0, 0, 0, 0, 0 };
        for (int i = 0; i < _last_result_list.Count; i++)
        {
            int lvl = _last_result_list[i].lvl;
            if (_last_result_list[i].is_correct)
            {
                arr[lvl]++;
            }
        }

        return arr;
    }


    // 循環取得Lvl List
    int[] m_lvl_cur_recycle_index = new int[] { 0, 0, 0, 0, 0, 0 };
    public List<EnglishWords> getLvlCycleList(int _lvl, int _count, FliexibleLvlEnum _named_flex_lvl, bool is_all_list)
    {
        int lvl_index = _lvl;// - 1; // 我們要從0開始
        
        List<EnglishWords> _list = new List<EnglishWords>();
        if (_lvl == 0) _list = m_lvl_1_en_list_with_3_wrongs;
        if (_lvl == 1) _list = m_lvl_2_en_list_with_3_wrongs;
        if (_lvl == 2) _list = m_lvl_3_en_list_with_3_wrongs;
        if (_lvl == 3) _list = m_lvl_4_en_list_with_3_wrongs;
        if (_lvl == 4) _list = m_lvl_5_en_list_with_3_wrongs;
        if (_lvl == 5) _list = m_lvl_6_en_list_with_3_wrongs;
        _list = getLvlQuest(_lvl, 0, true);

        int count = _count;
        if (is_all_list) count = _list.Count - 1;

        int i_start = m_lvl_cur_recycle_index[lvl_index];
        int i_end = m_lvl_cur_recycle_index[lvl_index] + count;
        if (i_end >= _list.Count)
        {            
            _list = getMyTools().ShuffleEnglishWordsList(_list);
            i_start = 0;
            i_end = count;
        }

        
        //Debug.LogError("--------getLvlCycleList");
        List<EnglishWords> result_list = new List<EnglishWords>();
        for (int i = i_start; i < i_end; i++)
        {
            EnglishWords new_en = new EnglishWords();
            new_en.lvl = _list[i].lvl;
            new_en.spell = _list[i].spell;
            new_en.word_type = _list[i].word_type;
            new_en.chinese_01 = _list[i].chinese_01;
            new_en.chinese_02 = _list[i].chinese_02;
            new_en.chinese_03 = _list[i].chinese_03;
            new_en.show_ans = _list[i].show_ans;
            new_en.original_order = _list[i].original_order;
            new_en.is_answered = _list[i].is_answered;
            new_en.is_online = _list[i].is_online;
            new_en.is_correct = _list[i].is_correct;
            new_en.fliex_lvl = _named_flex_lvl;
            new_en.str_wrong_list.Clear();
            for (int j = 0; j < _list[i].str_wrong_list.Count; j++)
            {
                new_en.str_wrong_list.Add(_list[i].str_wrong_list[j]);
            }

            //Debug.LogError("要題目:"+new_en.lvl+","+ _named_flex_lvl);


            result_list.Add(new_en);
            m_lvl_cur_recycle_index[lvl_index]++;
        }

        
        return result_list;
    }

    /// <summary>
    /// 測試區
    /// </summary>
    int itest = 0;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Debug.Log("!!!!!!!!!!!!!!!!!!!!!! Space");
            //itest++;
            //checkTest();
            
        }        
    }

    List<EnglishWords> list = new List<EnglishWords>();
    void checkTest()
    {
        //Debug.LogError("#### itest = "+ itest);
        //if (itest == 1)
        //{
        //    list = getFlexibleQuestList(FliexibleLvlEnum.f_1_1, null);
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        //Debug.Log("["+i+"] lvl="+ list[i].lvl+",correct=["+ list[i].is_correct+ "] ,spell="+ list[i].spell+",wrong=" + list[i].str_wrong_list[0] +"|"+ list[i].str_wrong_list[1] +"|" +list[i].str_wrong_list[2]);
        //        Debug.Log("[" + i + "] lvl=" + list[i].lvl + ",correct=[" + list[i].is_correct + "] ,flexLvl =" + list[i].fliex_lvl + ", spell=" + list[i].spell + ",wrong=" + list[i].str_wrong_list[0] + "|" + list[i].str_wrong_list[1] + "|" + list[i].str_wrong_list[2]);
        //    }
        //}

        if (itest == 1)
        {
            List<EnglishWords> list = new List<EnglishWords>();
            for (int i = 0; i < 8; i++)
            {
                EnglishWords en = new EnglishWords();
                if (i < 2)
                {
                    en.lvl = 0;
                    en.is_correct = true;
                }
                else
                {
                    en.lvl = 1;
                    en.is_correct = true;
                }
                list.Add(en);
            }

            list = getFlexibleQuestList(FliexibleLvlEnum.f_2_1, list);
            for (int i = 0; i < list.Count; i++)
            {
                Debug.Log("[" + i + "] lvl=" + list[i].lvl + ",correct=[" + list[i].is_correct + "] ,flexLvl =" + list[i].fliex_lvl + ", spell=" + list[i].spell + ",wrong=" + list[i].str_wrong_list[0] + "|" + list[i].str_wrong_list[1] + "|" + list[i].str_wrong_list[2]);
            }
        }

    }


}

