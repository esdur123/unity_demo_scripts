﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.EventSystems;
//using Wave.XR.Sample;


public class Main : MonoBehaviour
{
    public enum buildModeEnum 
    {
        unity_editor,
        vive,
        fake_ar
    }

    public enum demoEnum
    {
        general,
        loop_en_game,
        loop_tower_game
    }

    [Header("DebugMode")]
    //public bool is_vive = true;
    public buildModeEnum p_playmode = buildModeEnum.unity_editor;
    public demoEnum p_demo_mode = demoEnum.general;
    //public bool is_lock_center_point = false;
    
    public bool is_cheat_en_game = false;
    public bool is_download_school_image_set = false;
    public bool is_debug_ui = true;
    //public bool is_debug_hand = true; // 就是兩隻手變成很好笑的方塊XD
    [Header("WebCSV")]
    public bool is_web_csv = true;
    public string p_gameflow_csv_url = "http://ce-studio.tw/schoolfiles/game_flow_01.csv";
    public string p_en_words_url = "http://ce-studio.tw/schoolfiles/english_02.csv";
    [Header("ViveEditor")]
    [SerializeField] GameObject[] sf_vive_auto;


    private void Awake()
    {
        MyFinder.Instance.getPrimaryButtonWatcher().myAwake();
        MyFinder.Instance.getGyroCtrl().myAwake();
    }

    private void OnEnable()
    {
        MyFinder.Instance.getPrimaryButtonWatcher().myOnEnable();
    }

    private void OnDisable()
    {
        MyFinder.Instance.getPrimaryButtonWatcher().myOnDisable();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(init());
    }

    bool m_is_inited = false;
    bool m_is_vive = false;
    IEnumerator init()
    {
        //MyFinder.Instance.getui().init();
        if (p_playmode == buildModeEnum.vive)
        {
            m_is_vive = true;
        }

        checkDebugMode();
        MyFinder.Instance.getUIDebugCtrl().init();
        MyFinder.Instance.getUIDebugCenterRay().init();
        MyFinder.Instance.getCoverCtrl().init();
        MyFinder.Instance.getMySystem().init();

        // 讀取網路CSV 失敗會改 is_web_csv為false 往下走
        yield return MyFinder.Instance.getCSVReader().ReadCSV(
                  is_web_csv
                , p_gameflow_csv_url
                , p_en_words_url
            );

        if (!is_web_csv)
        {
            yield return MyFinder.Instance.getCSVReader().ReadCSV(
                      is_web_csv
                    , MyFinder.Instance.getMyTools().getLocalPath() + MyFinder.Instance.getCSVReader().p_local_gameflow_filename
                    , MyFinder.Instance.getMyTools().getLocalPath() + MyFinder.Instance.getCSVReader().p_local_engame_filename
                );            
        }


        // 撈取學校圖組 (展覽前先在家撈好 )
        //if (is_download_school_image_set)
        //{ 
        //    yield return MyFinder.Instance.getPortalCtrl().downloadLocalImage();
        //}

        // 讀取已存好的學校圖組
        MyFinder.Instance.getPortalCtrl().loadLocalImage();


        // 把圖片讀下來(讀local)
        //yield return MyFinder.Instance.getCSVReader().waitWebTexture();

        MyFinder.Instance.getCSVReader().load2DTexture();
        // 把網路圖片存到Local + gameflow url改寫成local的路徑



        //MyFinder.Instance.getMySystem().init();

        MyFinder.Instance.getGyroCtrl().init();

        MyFinder.Instance.getFxCtrl().init();

        MyFinder.Instance.getClickEffect().init();

        MyFinder.Instance.getDialogueCtrl().init();

        MyFinder.Instance.getDirCtrl().init();

        MyFinder.Instance.getPortalCtrl().init();

        MyFinder.Instance.getFlowInitCtrl().init();

        MyFinder.Instance.getBuildingCtrl().init();

        MyFinder.Instance.getBuildingTheaterCtrl().init(); // (尚未喔!)進入電影院之後 才會初始Youtube Player

        MyFinder.Instance.getAudioCtrl().init();

        MyFinder.Instance.getGameFlowCtrl().init(); // 只有init一些基本的東西, 真正init是 initGameFlow

        MyFinder.Instance.getBuildingEnGameCtrl().init();

        MyFinder.Instance.getPrimaryButtonWatcher().Init();

        MyFinder.Instance.getTowerGameEnemyCtrl().init();

        MyFinder.Instance.getCoverCtrl().playFadeOut(); // cover 淡出

        //MyFinder.Instance.getGameFlowCtrl().startGameFlow();

        switch (p_demo_mode)
        {
            case demoEnum.general:
                MyFinder.Instance.getBuildingCtrl().showBuilding(BuildingCtrl.buildings_order_enum.classroom);
                break;
            case demoEnum.loop_en_game:
                MyFinder.Instance.getBuildingCtrl().fromBuildingToAnother(
                          BuildingCtrl.buildings_order_enum.classroom
                        , BuildingCtrl.buildings_order_enum.english_game
                    );
                break;
            case demoEnum.loop_tower_game:
                MyFinder.Instance.getBuildingCtrl().fromBuildingToAnother(
                          BuildingCtrl.buildings_order_enum.classroom
                        , BuildingCtrl.buildings_order_enum.tower_game_flow
                    );
                break;
            default:
                MyFinder.Instance.getBuildingCtrl().showBuilding(BuildingCtrl.buildings_order_enum.classroom);
                break;
        }
        

        m_is_inited = true;
        yield break;           
    }


    // Update is called once per frame
    void Update()
    {
        if (!m_is_inited) return;

        // 這邊一定要改的 ..
        MyFinder.Instance.getGyroCtrl().myUpdate();
        MyFinder.Instance.getPrimaryButtonWatcher().myUpdate();
        MyFinder.Instance.getUIDebugCtrl().myUpdate();
        MyFinder.Instance.getUIDebugCenterRay().myUpdate();
        MyFinder.Instance.getClickEffect().myUpdate();
        MyFinder.Instance.getDialogueCtrl().myUpdate();
        MyFinder.Instance.getDirCtrl().myUpdate();
        MyFinder.Instance.getBuildingTheaterCtrl().myUpdate();
        MyFinder.Instance.getGameFlowCtrl().myUpdate();
        MyFinder.Instance.getTowerGameEnemyCtrl().myUpdate();
        MyFinder.Instance.getFxCtrl().myUpdate();
        MyFinder.Instance.getMySystem().myUpdate();

        // Check if the left mouse button was clicked
        //if (Input.GetMouseButtonDown(0))
        //{
        //    Debug.Log("playBloody");
        //    MyFinder.Instance.getCoverCtrl().playBloody();
        //    //// Check if the mouse was clicked over a UI element
        //    //if (EventSystem.current.IsPointerOverGameObject())
        //    //{
        //    //    MyFinder.Instance.getUIDebugCtrl().showDebugTxt("Clicked on the UI");
        //    //    //Debug.Log("Clicked on the UI");
        //    //}
        //}
    }

    //[SerializeField] BuildingEvents sf_bevent;
    //public void exitGame()
    //{
    //    Application.Quit();
    //}


    void checkDebugMode()
    {
        for (int i = 0; i < sf_vive_auto.Length; i++)
        {
            sf_vive_auto[i].SetActive(m_is_vive);
        }
    }

    public void testClick()
    {
        Debug.LogError("test!!!");
    }



}
