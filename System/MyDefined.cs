﻿
public static class MyDefined
{

    /// -----------
    /// 定義
    /// -----------

    // AStar 一般定義
    public const string ASTAR_TAG_OBSTACLE = "Obstacle"; // 障礙物tag name

    // AStar可行方向模式
    public const int ASTAR_WAYTYPE_FOUR = 4;  // AStar可行方向 網格4方向
    public const int ASTAR_WAYTYPE_EIGHT = 8; // AStar可行方向 斜向8方向

    // AStar路徑模式
    public const int ASTAR_OPTIMIZE_NONE = 0; // AStar路徑 無優化
    public const int ASTAR_OPTIMIZE_LINE_OF_SIGHT = 1; // AStar路徑 可視點優化


    /// -----------
    /// 設定
    /// -----------

    // Debug模式
    public const bool IS_DEBUG = true;

    // AStar 目前設定
    public const int ASTAR_CUR_WAYTYPE = ASTAR_WAYTYPE_EIGHT;
    public const int ASTAR_CUR_OPTIMIZE = ASTAR_OPTIMIZE_NONE;

    // PathFinding 網格設定
    public const int PATH_FINDING_GRIDS_ROWS = 30; // 網格Rows
    public const int PATH_FINDING_GRIDS_COLUMNS = 15; // 網格Columns
    public const float PATH_FINDING_GRIDS_CELL_SIZE = 1.0f; // 網格寬度

    // 轉向 (seekAngle)
    public const float ASTAR_ENEMY_SEEK_ANGEL_MAX = 20.0f;  // 敵人 每偵最大轉向
    public const float ASTAR_ENEMY_SEEK_ANGEL_SPEED = 7.0f;   // 敵人 轉速度
    public const float ASTAR_PLAYER_MOVE_SEEK_ANGEL_MAX = 20.0f;  // 主角 [移動] 每偵最大轉向
    public const float ASTAR_PLAYER_MOVE_SEEK_ANGEL_SPEED = 10.0f;   // 主角 [移動] 轉速度
    public const float ASTAR_PLAYER_ATTACK_SEEK_ANGEL_MAX = 40.0f;  // 主角 [攻擊] 每偵最大轉向
    public const float ASTAR_PLAYER_ATTACK_SEEK_ANGEL_SPEED = 14.0f;   // 主角 攻擊 轉速度

    // 物件池
    public const int OBJECT_POOL_MAX_SINGLE_FX = 20;  // 單個FX最大數量

    /// -----------
    /// 參數
    /// -----------

    // 子彈速度
    public const float FX_BULLET_SPEED_0 = 60.0f;
    public const float FX_BULLET_SPEED_1 = 80.0f;
    public const float FX_BULLET_SPEED_ICE = 60.0f;


}

