using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
//using NativeGalleryNamespace;
//using UnityEngine.UI;


public class MyTools : MonoBehaviour
{

    public Texture2D ChangeTexture2DFormat(ref Texture2D oldTexture, int width, int height, TextureFormat newFormat)
    {
        //Create new empty Texture
        Texture2D newTex = new Texture2D(width, height, newFormat, false);
        //Copy old texture pixels into new one
        newTex.SetPixels(oldTexture.GetPixels(),0);
        //Apply
        newTex.Apply();

        return newTex;

        //Texture2D result = new Texture2D(width, height, newFormat, false);
        //Color[] rpixels = result.GetPixels(0);
        //float incX = (1.0f / (float)width);
        //float incY = (1.0f / (float)height);
        //for (int px = 0; px < rpixels.Length; px++)
        //{
        //    rpixels[px] = oldTexture.GetPixelBilinear(incX * ((float)px % width), incY * ((float)Mathf.Floor(px / height)));
        //}
        //result.SetPixels(rpixels, 0);
        //result.Apply();
        //return result;
    }

    public void setLocalRotation(ref Transform _target_tr, Vector3 _new_local_rotation)
    {
        /// get cur rot 這個是原始rotation, 重點 不要刪喔!
        //Vector3 cur_rot = new Vector3(
        //          _target_tr.localRotation.eulerAngles.x
        //        , _target_tr.localRotation.eulerAngles.y
        //        , _target_tr.localRotation.eulerAngles.z
        //    );

        _target_tr.localRotation = Quaternion.Euler(
                 _new_local_rotation
            );
    }

    // 取得安卓SD卡路徑
    public string GetAndroidExternalFilesDir()
    {
        using (AndroidJavaClass unityPlayer =
               new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject context =
                   unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                // Get all available external file directories (emulated and sdCards)
                AndroidJavaObject[] externalFilesDirectories =
                                    context.Call<AndroidJavaObject[]>
                                    ("getExternalFilesDirs", (object)null);
                AndroidJavaObject emulated = null;
                AndroidJavaObject sdCard = null;
                for (int i = 0; i < externalFilesDirectories.Length; i++)
                {
                    AndroidJavaObject directory = externalFilesDirectories[i];
                    using (AndroidJavaClass environment =
                           new AndroidJavaClass("android.os.Environment"))
                    {
                        // Check which one is the emulated and which the sdCard.
                        bool isRemovable = environment.CallStatic<bool>
                                          ("isExternalStorageRemovable", directory);
                        bool isEmulated = environment.CallStatic<bool>
                                          ("isExternalStorageEmulated", directory);
                        if (isEmulated)
                            emulated = directory;
                        else if (isRemovable && isEmulated == false)
                            sdCard = directory;
                    }
                }
                // Return the sdCard if available
                if (sdCard != null)
                    return sdCard.Call<string>("getAbsolutePath");
                else
                    return emulated.Call<string>("getAbsolutePath");
            }
        }
    }

    public string GetAndroidInternalFilesDir()
    {
        string[] potentialDirectories = new string[]
        {
        "/mnt/sdcard",
        "/sdcard",
        "/storage/sdcard0",
        "/storage/sdcard1"
        };

        if (Application.platform == RuntimePlatform.Android)
        {
            for (int i = 0; i < potentialDirectories.Length; i++)
            {
                if (Directory.Exists(potentialDirectories[i]))
                {
                    return potentialDirectories[i];
                }
            }
        }
        return "";
    }


    // native image
    // https://blog.csdn.net/f_957995490/article/details/118153989
    // https://assetstore.unity.com/packages/tools/integration/native-gallery-for-android-ios-112630
    // https://forum.unity.com/threads/native-gallery-for-android-ios-open-source.519619/
    public string getNativeImageFullPath(string _folder_name, string _file_name)
    {
        string imagesFolderPath = Path.Combine(Application.persistentDataPath, _folder_name);
        string imagePath = Path.Combine(imagesFolderPath, _file_name);
        return imagePath;
    }


    public string getNativeFolderPath(string _folder_name)
    {
        string imagesFolderPath = Path.Combine(Application.persistentDataPath, _folder_name);
        return imagesFolderPath;
    }


    public void saveNativeImage(string _folder_name, string _file_name, byte[] _image_byte_arr)
    {
        string imagesFolderPath = Path.Combine(Application.persistentDataPath, _folder_name);
        string imagePath = Path.Combine(imagesFolderPath, _file_name);

        Directory.CreateDirectory(imagesFolderPath);
        File.WriteAllBytes(imagePath, _image_byte_arr);
        //NativeGallery.SaveImageToGallery( myTexture, "GalleryTest", "My img {0}.png" )
    }


    public byte[] loadNativeImage(string _folder_name, string _file_name)
    {
        string imagesFolderPath = Path.Combine(Application.persistentDataPath, _folder_name);
        string imagePath = Path.Combine(imagesFolderPath, _file_name);
        byte[] image_byte_arr = null;
        if (File.Exists(imagePath))
        {
            image_byte_arr = File.ReadAllBytes(imagePath);
        }

        return image_byte_arr;
    }


    public void deleteNativeImage(string _folder_name, string _file_name)
    {
        string imagesFolderPath = Path.Combine(Application.persistentDataPath, _folder_name);
        string imagePath = Path.Combine(imagesFolderPath, _file_name);
        if (File.Exists(imagePath))
        {
            File.Delete(imagePath);
        }           
    }


    public string[] listNativeImage(string _folder_name, string _file_name)
    {
        string imagesFolderPath = Path.Combine(Application.persistentDataPath, _folder_name);
        string imagePath = Path.Combine(imagesFolderPath, _file_name);
        string[] files = null;
        if (Directory.Exists(imagesFolderPath))
        {
            files = Directory.GetFiles(imagesFolderPath);
        }

        return files;
    }


    //Following method is used to retrive the relative path as device platform
    public string getLocalPath()
    {
        // 存在安卓SD卡
        //return GetAndroidExternalFilesDir() + "/";
        #if UNITY_EDITOR
                return Application.persistentDataPath + "/";
        #elif UNITY_ANDROID
                return GetAndroidExternalFilesDir() + "/";           
        #elif UNITY_IPHONE
                return Application.persistentDataPath + "/";
        #else
                return Application.dataPath  + "/";
        #endif
    }


    /// <summary>
    /// 匯出CVS到Local
    /// </summary>
    public void SaveCSVToDeviceLocalText(List<string> _row_data_list, string _path, string _file_name)
    {
        //Create Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(_path)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(_path));
        }

        //string[][] output = new string[_row_data_list.Count][];

        //for (int i = 0; i < output.Length; i++)
        //{
        //    output[i] = _row_data_list[i];
        //}

        //int length = output.GetLength(0);
        //string delimiter = ",";

        StringBuilder sb = new StringBuilder();

        //for (int index = 0; index < length; index++)
        //    sb.AppendLine(string.Join(delimiter, output[index]));
        for (int index = 0; index < _row_data_list.Count; index++)
        {
            string str = ","+_row_data_list[index];
            sb.AppendLine(string.Join(",", str));
        }

        string full_path = _path + _file_name;// Application.persistentDataPath + "/nori/a01.csv";

        StreamWriter outStream = System.IO.File.CreateText(full_path);
        outStream.WriteLine(sb);
        outStream.Close();
    }


    public void downloadImage(string url, string pathToSaveImage)
    {
        
        StartCoroutine(dolwloadImageToLocal(url, pathToSaveImage));
    }

    public IEnumerator dolwloadImageToLocal(string url, string savePath)
    {
        WWW www = new WWW(url);
        yield return www;

        //Check if we failed to send
        if (string.IsNullOrEmpty(www.error))
        {
            //UnityEngine.Debug.Log("dolwloadImageToLocal Success");

            //Save Image            
            saveImage(savePath, www.bytes);
        }
        else
        {
            //MyFinder.Instance.getUIDebugCtrl().showDebugTxt("下載失敗", 1);
            UnityEngine.Debug.Log("Error: " + www.error);
            Debug.LogWarning("Error: " + www.error);
        }
    }

    void saveImage(string path, byte[] imageBytes)
    {
        //Create Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }

        try
        {
            File.WriteAllBytes(path, imageBytes);
            Debug.LogWarning("Saved Data to: " + path.Replace("/", "\\"));
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
            Debug.LogWarning("Error: " + e.Message);
            //MyFinder.Instance.getUIDebugCtrl().showDebugTxt("儲存失敗", 0);
        }
    }

    public byte[] loadImage(string path)
    {
        byte[] dataByte = null;

        //Exit if Directory or File does not exist
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            Debug.LogWarning("Directory does not exist");
            return null;
        }

        if (!File.Exists(path))
        {
            Debug.Log("File does not exist");
            return null;
        }

        try
        {
            dataByte = File.ReadAllBytes(path);
            Debug.Log("Loaded Data from: " + path.Replace("/", "\\"));
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed To Load Data from: " + path.Replace("/", "\\"));
            Debug.LogWarning("Error: " + e.Message);
        }

        return dataByte;
    }



    public string RoundString(float _val, int _last_digi)
    {
        decimal decimalValue = decimal.Parse(_val.ToString());
        decimalValue = decimal.Round(decimalValue, _last_digi);
        string _str = decimalValue.ToString();
        return _str;
    }
  


    //打亂陣列順序 隨機排列
    public List<EnglishWords> ShuffleEnglishWordsList(List<EnglishWords> _arr)
    {
        List<EnglishWords> new_arr = _arr;
        //if (_arr == null) return;
        int len = new_arr.Count;//用變數記會快一點點點
        //Random rd = new Random();
        int r;//記下隨機產生的號碼
        EnglishWords tmp;//暫存用
        for (int i = 0; i < len - 1; i++)
        {
            r = UnityEngine.Random.Range(i, len); //取亂數，範圍從自己到最後，決定要和哪個位置交換，因此也不用跑最後一圈了
            //r = rd.Next(i, len);
            if (i == r) continue;
            tmp = new_arr[i];
            new_arr[i] = new_arr[r];
            new_arr[r] = tmp;
        }

        return new_arr;
    }


    //打亂陣列順序 隨機排列
    public int[] ShuffleIntArr(int[] _arr)
    {
        int[] new_arr = _arr;
        //if (_arr == null) return;
        int len = new_arr.Length;//用變數記會快一點點點
        //Random rd = new Random();
        int r;//記下隨機產生的號碼
        int tmp;//暫存用
        for (int i = 0; i < len - 1; i++)
        {
            r = UnityEngine.Random.Range(i, len); //取亂數，範圍從自己到最後，決定要和哪個位置交換，因此也不用跑最後一圈了
            //r = rd.Next(i, len);
            if (i == r) continue;
            tmp = new_arr[i];
            new_arr[i] = new_arr[r];
            new_arr[r] = tmp;
        }

        return new_arr;
    }


    //打亂陣列順序 隨機排列
    public Vector3[] ShuffleVector3Arr(Vector3[] _arr)
    {
        Vector3[] new_arr = _arr;
        //if (_arr == null) return;
        int len = new_arr.Length;//用變數記會快一點點點
        //Random rd = new Random();
        int r;//記下隨機產生的號碼
        Vector3 tmp;//暫存用
        for (int i = 0; i < len - 1; i++)
        {
            r = UnityEngine.Random.Range(i, len); //取亂數，範圍從自己到最後，決定要和哪個位置交換，因此也不用跑最後一圈了
            //r = rd.Next(i, len);
            if (i == r) continue;
            tmp = new_arr[i];
            new_arr[i] = new_arr[r];
            new_arr[r] = tmp;
        }

        return new_arr;
    }


    public Vector3 CameraDistWorldPos(Transform _camera, Vector3 _set_pos, float _dist, bool _is_lock_y, float _lock_y)
    {
        Vector3 dir =  _set_pos;// (_camera.position+_set_pos) - _camera.position;
        if (_is_lock_y) dir = new Vector3(dir.x, 0, dir.z);
        dir.Normalize();
        dir = new Vector3(dir.x, _lock_y, dir.z);
        //Debug.LogError(dir);
        Vector3 pos = _camera.position + dir * _dist;
        return pos;
    }


    /// 將GameObject轉向(Rotate)到目標
    public void RotateGameobjectToTraget(GameObject obj, Vector3 targetPos, bool isLockY)
    {
        Vector3 direction = targetPos - obj.transform.position;
        if (direction == Vector3.zero)
        {
            return;
        }

        if (isLockY)
        {
            direction = new Vector3(
                      direction.x
                    , obj.transform.position.y
                    , direction.z
                );
        }

        Quaternion rotation = Quaternion.LookRotation(direction);
        obj.transform.localRotation = Quaternion.Lerp(obj.transform.rotation, rotation, 1);
    }


    /// 面對指定座標的方向
    public void faceForward(Transform myTransform, Vector3 targetPos)
    {
        Vector3 vec = targetPos - myTransform.position;
        vec.Normalize();
        myTransform.forward = vec;
    }


    /// updating 轉向
    public bool seekAngle(Transform myTransform, Vector3 targetPos, float maxAngle, float maxSpeed)
    {

        Vector3 vec = targetPos - myTransform.position;
        vec.y = 0.0f;

        vec.Normalize();

        Vector3 v = myTransform.forward;

        v.Normalize();


        float fDot = Vector3.Dot(vec, v);

        float fRadian = Mathf.Acos(fDot);

        float fDegree = fRadian * Mathf.Rad2Deg;

        float fRot = fDegree;


        if (fRot > maxAngle)
        {
            fRot = maxAngle;
        }
        else if (fRot < 0.1f)
        {
            myTransform.forward = vec;
            return true; // 方向轉好了
        }

        int iSign = 1;
        Vector3 vCross = Vector3.Cross(v, vec);

        if (vCross.y < 0)
        {
            iSign = -1;
        }

        fRot *= iSign;

        if (float.IsNaN(fRot))
        {
            return false;
        }
        else
        {
            myTransform.Rotate(Vector3.up, fRot * Time.deltaTime * maxSpeed);
            return false;
        }

    }

}
