﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * 用static[Singleton]去找到戰鬥場景中的script, 但其它script不是static 
 **/
public class MyFinder : MonoBehaviour
{
    [Header("Main")]
    [SerializeField] GameObject sf_obj_main;
    [Header("Gyro")]
    [SerializeField] GyroCtrl sf_gyro;


    ClickEffect m_ClickEffect;
    DialogueCtrl m_DialogueCtrl;
    DirCtrl m_DirCtrl;
    PortalCtrl m_PortalCtrl;
    GameFlowCtrl m_GameFlowCtrl;
    CSVReader m_CSVReader;
    FlowInitCtrl m_FlowInitCtrl;
    CoverCtrl m_CoverCtrl;
    //GamePlayCtrl m_GamePlayCtrl;
    QuestionCtrl m_QuestionCtrl;
    //DefenceCtrl m_DefenceCtrl;
    BuildingCtrl m_BuildingCtrl;
    BuildingClassroomCtrl m_BuildingClassroomCtrl;
    BuildingTheaterCtrl m_BuildingTheaterCtrl;
    BuildingEnGameCtrl m_BuildingEnGameCtrl;
    EnGameSelectChar m_EnGameSelectChar;
    EnGamePlayCtrl m_EnGamePlayCtrl;
    UIDebugCtrl m_UIDebugCtrl;
    MySystem m_MySystem;
    AudioCtrl m_AudioCtrl;
    TowerBottomBoard m_TowerBottomBoard;
    TowerGameCtrl m_TowerGameCtrl;
    TestCtrl m_TestCtrl;
    PrimaryButtonWatcher m_PrimaryButtonWatcher;
    TowerGameEnemyCtrl m_TowerGameEnemyCtrl;
    PlayerCtrl m_PlayerCtrl;
    TowerGameBackageCtrl m_TowerGameBackageCtrl;
    TowerGameSkillCtrl m_TowerGameSkillCtrl;
    //GyroCtrl m_GyroCtrl;

    //[SerializeField] GameObject sf_obj_root_player; 

    MyTools m_MyTools;
    //NodeManager m_NodeManager;
    //AStar m_AStar;
    //BattleCamera m_CameraScript;
    //FSMPlayerService m_FSMPlayerService;
    //BattleEnemyService m_BattleEnemyService;
    FxCtrl m_FxCtrl;
    Main m_Main;
    UIDebugCenterRay m_UIDebugCenterRay;
    //BattleSaveLoad m_BattleSaveLoad;
    //BattleDebugService m_BattleDebugService;
    //BattlePetService m_BattlePetService;
    //JoystickLeft m_JoystickLeft;
    //JoystickToMoving m_JoystickToMoving;
    //BattleMain m_BattleMain;


    public static MyFinder Instance { get; private set; }
    void Awake()
    {
        //遊戲中這個物件只有一份(就是Singleton) 用static
        if (Instance == null)
        {
            Instance = this;
        }

        m_Main = sf_obj_main.GetComponent<Main>();

        // 掛在 main上
        m_ClickEffect = sf_obj_main.GetComponentInChildren<ClickEffect>();
        m_DialogueCtrl = sf_obj_main.GetComponentInChildren<DialogueCtrl>();
        m_DirCtrl = sf_obj_main.GetComponentInChildren<DirCtrl>();
        m_PortalCtrl = sf_obj_main.GetComponentInChildren<PortalCtrl>();
        m_GameFlowCtrl = sf_obj_main.GetComponentInChildren<GameFlowCtrl>();
        m_CSVReader = sf_obj_main.GetComponentInChildren<CSVReader>();
        m_FlowInitCtrl = sf_obj_main.GetComponentInChildren<FlowInitCtrl>();
        m_CoverCtrl = sf_obj_main.GetComponentInChildren<CoverCtrl>();

        //m_GamePlayCtrl = sf_obj_main.GetComponentInChildren<GamePlayCtrl>();
        m_QuestionCtrl = sf_obj_main.GetComponentInChildren<QuestionCtrl>();
        //m_DefenceCtrl = sf_obj_main.GetComponentInChildren<DefenceCtrl>();
        m_BuildingClassroomCtrl = sf_obj_main.GetComponentInChildren<BuildingClassroomCtrl>();
        m_BuildingTheaterCtrl = sf_obj_main.GetComponentInChildren<BuildingTheaterCtrl>();
        m_BuildingCtrl = sf_obj_main.GetComponentInChildren<BuildingCtrl>();
        m_BuildingEnGameCtrl = sf_obj_main.GetComponentInChildren<BuildingEnGameCtrl>();
        m_EnGameSelectChar = sf_obj_main.GetComponentInChildren<EnGameSelectChar>();
        m_EnGamePlayCtrl = sf_obj_main.GetComponentInChildren<EnGamePlayCtrl>();
        m_UIDebugCtrl = sf_obj_main.GetComponentInChildren<UIDebugCtrl>();
        m_MySystem = sf_obj_main.GetComponentInChildren<MySystem>();
        m_AudioCtrl = sf_obj_main.GetComponentInChildren<AudioCtrl>();
        m_TowerBottomBoard = sf_obj_main.GetComponentInChildren<TowerBottomBoard>();
        m_TowerGameCtrl = sf_obj_main.GetComponentInChildren<TowerGameCtrl>();
        m_TestCtrl = sf_obj_main.GetComponentInChildren<TestCtrl>();
        m_PrimaryButtonWatcher = sf_obj_main.GetComponentInChildren<PrimaryButtonWatcher>();
        m_TowerGameEnemyCtrl = sf_obj_main.GetComponentInChildren<TowerGameEnemyCtrl>();
        m_PlayerCtrl = sf_obj_main.GetComponentInChildren<PlayerCtrl>();
        m_TowerGameBackageCtrl = sf_obj_main.GetComponentInChildren<TowerGameBackageCtrl>();
        m_TowerGameSkillCtrl = sf_obj_main.GetComponentInChildren<TowerGameSkillCtrl>();
        m_UIDebugCenterRay = sf_obj_main.GetComponentInChildren<UIDebugCenterRay>();
        //m_GyroCtrl = sf_obj_main.GetComponentInChildren<GyroCtrl>();

        ///
        m_MyTools = sf_obj_main.GetComponentInChildren<MyTools>();
        //m_NodeManager = sf_obj_main.GetComponentInChildren<NodeManager>();
        //m_AStar = sf_obj_main.GetComponentInChildren<AStar>();
        //m_CameraScript = sf_obj_main.GetComponentInChildren<BattleCamera>();       
        //m_BattleEnemyService = sf_obj_main.GetComponentInChildren<BattleEnemyService>();
        m_FxCtrl = sf_obj_main.GetComponentInChildren<FxCtrl>();
        //m_BattleSaveLoad = sf_obj_main.GetComponentInChildren<BattleSaveLoad>();
        //m_BattleDebugService = sf_obj_main.GetComponentInChildren<BattleDebugService>();
        //m_BattlePetService = sf_obj_main.GetComponentInChildren<BattlePetService>();
        //m_JoystickLeft = sf_obj_main.GetComponentInChildren<JoystickLeft>();
        //m_JoystickToMoving = sf_obj_main.GetComponentInChildren<JoystickToMoving>();
        //m_BattleMain = sf_obj_main.GetComponentInChildren<BattleMain>();

        //// 掛在player上
        //m_FSMPlayerService = sf_obj_root_player.GetComponentInChildren<FSMPlayerService>();
    }

    
    public UIDebugCenterRay getUIDebugCenterRay()
    {
        return m_UIDebugCenterRay;
    }


    public GyroCtrl getGyroCtrl()
    {
        return sf_gyro;
    }


    public TowerGameSkillCtrl getTowerGameSkillCtrl()
    {
        return m_TowerGameSkillCtrl;
    }


    public TowerGameBackageCtrl getTowerGameBackageCtrl()
    {
        return m_TowerGameBackageCtrl;
    }

    public PlayerCtrl getPlayerCtrl()
    {
        return m_PlayerCtrl;
    }

    public TowerGameEnemyCtrl getTowerGameEnemyCtrl()
    {
        return m_TowerGameEnemyCtrl;
    }

    public Main getMain()
    {
        return m_Main;
    }

    public PrimaryButtonWatcher getPrimaryButtonWatcher()
    {
        return m_PrimaryButtonWatcher;
    }

    public TestCtrl getTestCtrl()
    {
        return m_TestCtrl;
    }

    public TowerGameCtrl getTowerGameCtrl()
    {
        return m_TowerGameCtrl;
    }

    public TowerBottomBoard getTowerBottomBoard()
    {
        return m_TowerBottomBoard;
    }

    public AudioCtrl getAudioCtrl()
    {
        return m_AudioCtrl;
    }

    public MySystem getMySystem()
    {
        return m_MySystem;
    }


    public UIDebugCtrl getUIDebugCtrl()
    {
        return m_UIDebugCtrl;
    }

    public EnGamePlayCtrl getEnGamePlayCtrl()
    {
        return m_EnGamePlayCtrl;
    }

    public EnGameSelectChar getEnGameSelectChar()
    {
        return m_EnGameSelectChar;
    }

    public BuildingEnGameCtrl getBuildingEnGameCtrl()
    {
        return m_BuildingEnGameCtrl;
    }

    public BuildingTheaterCtrl getBuildingTheaterCtrl()
    {
        return m_BuildingTheaterCtrl;
    }

    public BuildingCtrl getBuildingCtrl()
    {
        return m_BuildingCtrl;
    }

    public BuildingClassroomCtrl getBuildingClassroomCtrl()
    {
        return m_BuildingClassroomCtrl;
    }

    public QuestionCtrl getQuestionCtrl()
    {
        return m_QuestionCtrl;
    }

    public CoverCtrl getCoverCtrl()
    {
        return m_CoverCtrl;
    }

    public FlowInitCtrl getFlowInitCtrl()
    {
        return m_FlowInitCtrl;
    }


    public CSVReader getCSVReader()
    {
        return m_CSVReader;
    }


    public GameFlowCtrl getGameFlowCtrl()
    {
        return m_GameFlowCtrl;
    }

    public PortalCtrl getPortalCtrl()
    {
        return m_PortalCtrl;
    }


    public DirCtrl getDirCtrl()
    {
        return m_DirCtrl;
    }

    public ClickEffect getClickEffect()
    {
        return m_ClickEffect;
    }

    public DialogueCtrl getDialogueCtrl()
    {
        return m_DialogueCtrl;
    }

    //public GameObject getRootPlayer()
    //{
    //    return sf_obj_root_player;
    //}


    //public BattleMain getBattleMain()
    //{
    //    return m_BattleMain;
    //}


    //public JoystickToMoving getJoystickToMoving()
    //{
    //    return m_JoystickToMoving;
    //}


    //public NodeManager getNodeManager()
    //{
    //    return m_NodeManager;
    //}


    //public AStar getAStar()
    //{
    //    return m_AStar;
    //}


    //public JoystickLeft getJoystickLeft()
    //{
    //    return m_JoystickLeft;
    //}


    //public BattleCamera getCameraController()
    //{
    //    return m_CameraScript;
    //}


    //public FSMPlayerService getFSMPlayerService()
    //{
    //    return m_FSMPlayerService;
    //}


    //public BattleEnemyService getBattleEnemyService()
    //{
    //    return m_BattleEnemyService;
    //}


    public FxCtrl getFxCtrl()
    {
        return m_FxCtrl;
    }


    public MyTools getMyTools()
    {
        return m_MyTools;
    }


    //public BattleSaveLoad getBattleSaveLoad()
    //{
    //    return m_BattleSaveLoad;
    //}


    //public BattleDebugService getBattleDebugService()
    //{
    //    return m_BattleDebugService;
    //}


    //public BattlePetService getBattlePetService()
    //{
    //    return m_BattlePetService;
    //}

}


