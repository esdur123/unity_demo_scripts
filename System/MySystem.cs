using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MySystem : MonoBehaviour
{
    [Header("[Camera]")]
    [SerializeField] public Transform sf_tr_camera;
    [SerializeField] Animator sf_anim_camera;
    [Header("[Higher_Red_point]")]
    [SerializeField] public Transform sf_tr_higher_red_point;
    [Header("WarningHint")]
    [SerializeField] GameObject sf_obj_warning;
    [SerializeField] Text sf_txt_warning;
    [SerializeField] Text sf_txt_timer;
    [Header("[Skybox]")]
    [SerializeField] Material[] sf_mt_skybox_arr;
    [Header("360圖的物件本體")]
    [SerializeField] public GameObject sf_obj_360;
    [Header("2DUI按鈕(AR)")]
    [SerializeField] GameObject sf_obj_2D_canvas_ar;
    [SerializeField] Transform sf_tr_env;
    [Header("Gyro.cs")]
    [SerializeField] GyroCtrl sf_gyro;
    Main.buildModeEnum m_playmode;
    public enum skyboxEnum
    {
        default_skybox,
        blue_sky,
        length
    }

    UIDebugCtrl m_UIDebugCtrl = null;
    UIDebugCenterRay m_UIDebugCenterRay = null;
    MyTools m_MyTools = null;
    public void init()
    {
        m_UIDebugCtrl = MyFinder.Instance.getUIDebugCtrl();
        m_UIDebugCenterRay = MyFinder.Instance.getUIDebugCenterRay();
        m_playmode = MyFinder.Instance.getMain().p_playmode;
        m_MyTools = MyFinder.Instance.getMyTools();

        //if (m_playmode != Main.buildModeEnum.fake_ar)
        //{
        //    sf_obj_2D_canvas_ar.SetActive(false);
        //}
        //else
        //{
        //    sf_obj_2D_canvas_ar.SetActive(true);
        //}

    }


    public enum lockCameraEnum
    {
        ar_en_game_select_char,
        ar_en_game_play,      
        length
    }

    public void LockCamera(lockCameraEnum _enum)
    {
        //Debug.LogError("LockCamera: "+_enum);

        // 關掉陀螺儀
        sf_gyro.setGyro(false);

        // 關掉debug測試用的滑鼠視角
        m_UIDebugCtrl.setDebugMousLook(false);

        // 關掉center ray
        m_UIDebugCenterRay.setCenterRay(false);

        // 指定camera的animator
        sf_anim_camera.enabled = true;
        switch (_enum)
        {
            case lockCameraEnum.ar_en_game_select_char:
                sf_anim_camera.Play("camera_en_game_slect_char");
                break;
            case lockCameraEnum.ar_en_game_play:
                sf_anim_camera.Play("camera_en_game_play");                
                break;
            case lockCameraEnum.length:
                break;
            default:
                break;
        }

        
    }

    public void unLockCamera()
    {
        //Debug.LogError(_enum);

        // 開啟陀螺儀
        //Transform ss = new Transform();
        m_MyTools.setLocalRotation(ref sf_tr_camera, Vector3.zero);
        
        sf_gyro.setGyro(true);

        // 開啟debug測試用的滑鼠視角
        m_UIDebugCtrl.setDebugMousLook(true);

        // 開啟center ray
        m_UIDebugCenterRay.setCenterRay(true);

        // 指定camera的animator
        sf_anim_camera.enabled = false;
        //sf_anim_camera.Play("camera_en_game_slect_char");
    }

    public void changeSkybox(skyboxEnum _enum)
    {
        RenderSettings.skybox = sf_mt_skybox_arr[(int)_enum];
    }


    bool m_is_show_warning_hint = false;
    public void showAutoExitHint(string _txt, int _maxtime)
    {
        if (m_is_show_warning_hint) return;
        m_is_show_warning_hint = true;

        StartCoroutine(_showAutoExitHint(_txt, _maxtime));
    }


    IEnumerator _showAutoExitHint(string _txt, int _maxtime)
    {
        int max_time = _maxtime;
        sf_obj_warning.gameObject.SetActive(true);
        sf_txt_warning.text = _txt;

        int timer = max_time;
        for (int i = 0; i < max_time; i++)
        {
            Debug.Log(timer);
            sf_txt_timer.text = "系統將於 " + timer + " 秒自動退出";
            timer--;
            yield return new WaitForSeconds(1);
        }

        quitGame();
        yield break;
    }


    public void quitGame()
    {
        Debug.Log("****** Quit Game ******");
        Application.Quit();
    }


    const float c_delta_degree = 2f;
    float m_add_degree_rot_x = 0;
    bool m_is_allow_Rot = false;   
    void changeCameraRotX(bool _is_back_to_center)
    {

        if (_is_back_to_center)
        {
            m_add_degree_rot_x = 0;
        }

        sf_gyro.changeCamRotX(m_add_degree_rot_x);
        //float rot_x = sf_tr_env.localRotation.eulerAngles.x;
        //float new_rot_x = rot_x + m_add_degree_rot_x;
        //Vector3 dialogue_rot = new Vector3(
        //      new_rot_x
        //    , 0
        //    , 0
        //);

        //if (_is_back_to_center)
        //{
        //    dialogue_rot = Vector3.zero;
        //}

        //sf_tr_env.localRotation = Quaternion.Euler(
        //        dialogue_rot
        //    );
    }

    //public void isPressedChangeEnvRot()
    //{
    //    m_is_allow_Rot = true;
    //}

    //public void isUnPressedChangeEnvRot()
    //{
    //    m_is_allow_Rot = false;
    //}

    public void stopChangeCameraRotX()
    {
        m_is_click_btn = false;
        m_is_allow_Rot = false;
    }

    public bool isClickBtn()
    {
        return m_is_click_btn;
    }


    public void upCameraRotX()
    {
        m_is_click_btn = true;
        m_is_allow_Rot = true;
        m_add_degree_rot_x -= c_delta_degree;
        Debug.Log("111");
        //changeCameraRotX(false);
    }


    bool m_is_click_btn = false;
    public void downCameraRotX()
    {
        m_is_click_btn = true;
        m_is_allow_Rot = true;
        m_add_degree_rot_x += c_delta_degree;
        
    }

    public void centerCameraRotX()
    {
        m_is_click_btn = true;
        m_is_allow_Rot = false;
        changeCameraRotX(true);
    }

    public void myUpdate()
    {
        if (!m_is_allow_Rot) return;
        changeCameraRotX(false);
    }

    public void switchImg360()
    {

    }
}
