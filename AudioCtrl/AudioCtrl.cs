using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AudioCtrl : MonoBehaviour
{
    [Header("AudioListener")]
    [SerializeField] AudioSource sf_audiosource_music;
    [SerializeField] AudioClipUnit[] sf_audiosource_clip;

    public enum AudioClipsEnum
    {
        btn_hover,
        btn_click,
        dialogue_click,
        select_char,
        choose_char,
        en_game_start,
        woosh_1_2,
        lose_1_1,
        special_2_2,
        CR2_Cute_UI_1,
        Negative,
        Ice_Reflect,
        CGM2_Star_Rating_1,
        Pauze_Game_Arcade,
        FA_Funny_Impact_1_3,
        CSG_Coin_02,
        Nitro_Speed,
        FA_Win_Jingle_Loop,
        impact_02,
        FA_Bad_Item_1,
        CR_Wrong_Answer,
        FA_Cute_Jump,
        FA_Confirm_Button_1_2,
        FA_Select_Button_1_6,
        FA_Funny_Impact_1_1,
        Interface_Push_Button,
        Pickup_Soft,
        Positive_Holy2,
        length
    }

    [Header("音樂-enum順序")]
    [SerializeField] AudioClip[] sf_music_arr;
    [Header("音效-enum順序")]
    [SerializeField] AudioClip[] sf_clip_arr;

    public enum MusicClipsEnum
    {
        classroom,
        en_game_room,
        en_game_play,
        tower_game_play,
        CSG_Dance_Party_Jingle_Loop,
        CGM2_Happy_Ktties_Short_Loop,
        length
    }

    Dictionary<int, AudioClip> m_music_dic = new Dictionary<int, AudioClip>();
    Dictionary<int, AudioClip> m_clip_dic = new Dictionary<int, AudioClip>();
    bool m_is_init = false;
    public void init()
    {
        for (int i = 0; i < (int)MusicClipsEnum.length; i++)
        {
            m_music_dic.Add(i, sf_music_arr[i]);
        }

        for (int i = 0; i < (int)AudioClipsEnum.length; i++)
        {
            m_clip_dic.Add(i, sf_clip_arr[i]);
        }

        m_is_init = true;
    }

    //float m_vol_clip = 1;
    public void playClip(AudioClipsEnum _enum ,float _vol = 0.8f)
    {
        if (!m_is_init)
        {
            Debug.Log("防止init之前就按到playClip");
            return; // 
        }

        StartCoroutine(_playClip(_enum, _vol));
    }

    //AudioClipUnit m_picked_unit = null;
    IEnumerator _playClip(AudioClipsEnum _enum, float _vol)
    {
        int index = -1;
        for (int i = 0; i < sf_audiosource_clip.Length; i++)
        {
            if (sf_audiosource_clip[i].is_in_use) continue;
            sf_audiosource_clip[i].is_in_use = true;
            index = i;
            break;
        }

        if (index == -1)
        {
            yield break;
        }

        sf_audiosource_clip[index].sf_audio_source.volume = _vol;
        sf_audiosource_clip[index].sf_audio_source.clip = m_clip_dic[(int)_enum];
        sf_audiosource_clip[index].sf_audio_source.Play();

        yield return new WaitForSeconds(sf_audiosource_clip[index].sf_audio_source.clip.length);
        sf_audiosource_clip[index].is_in_use = false;
        yield break;
    }


    public float m_vol_music = 0;
    public void playMusic(MusicClipsEnum _enum, float _vol = 0.8f)
    {
        if (!m_is_init)
        {
            Debug.Log("防止init之前就按到playMusic");
            return; // 
        }

        if (is_fadeing) return; // 不給搶播
        StartCoroutine(_playMusic(_enum, _vol));

    }

    bool is_fadeing = false;
    IEnumerator _playMusic(MusicClipsEnum _enum, float _vol)
    {
        float delta_time = 0.01f;
        float delta_val = 0.025f;
        is_fadeing = true;

        // 停止播放的意思
        if (_enum == MusicClipsEnum.length)
        {
            // fade out
            bool is_fadeout_01 = true;
            do
            {
                m_vol_music -= delta_val;
                if (m_vol_music <= 0)
                {
                    m_vol_music = 0;
                    is_fadeout_01 = false;
                }
                sf_audiosource_music.volume = m_vol_music;
                yield return new WaitForSeconds(delta_time);

            } while (is_fadeout_01);

            is_fadeing = false;
            yield break;
        }

        // fade out
        bool is_fadeout_02 = true;
        do
        {
            m_vol_music -= delta_val;
            if (m_vol_music <= 0)
            {
                m_vol_music = 0;
                is_fadeout_02 = false;
            }
            sf_audiosource_music.volume = m_vol_music;
            yield return new WaitForSeconds(delta_time);

        } while (is_fadeout_02);
  

        // change clip
        sf_audiosource_music.clip = m_music_dic[(int)_enum];
        sf_audiosource_music.loop = true;
        sf_audiosource_music.Play();

        // fade in
        bool is_fadein_01 = true;
        do
        {
            m_vol_music += delta_val;
            if (m_vol_music >= _vol)
            {
                m_vol_music = _vol;
                is_fadein_01 = false;
            }
            sf_audiosource_music.volume = m_vol_music;
            yield return new WaitForSeconds(delta_time);

        } while (is_fadein_01);

        is_fadeing = false;
        yield break;
    }


}
