using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  模擬VR按鈕 由UIDebugCenterRay的update打射線檢查
/// </summary>
public interface IDebugOnTrigger
{
    void OnDebugClick();
    void OnDebugEnter();
    void OnDebugExit();
}


public class myInterface : MonoBehaviour
{

}
