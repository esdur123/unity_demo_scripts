﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCollider : MonoBehaviour, IDebugOnTrigger
{
    PortalCtrl m_PortalCtrl = null;
    public void OnClick()
    {
        if (m_PortalCtrl == null)
        {
            m_PortalCtrl = MyFinder.Instance.getPortalCtrl();
        }

        m_PortalCtrl.clickPortal();
    }

    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        OnClick();    
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
    }

    void IDebugOnTrigger.OnDebugExit()
    {
    }
}
