﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PortalSet
{
    public Vector3 pos = new Vector3();
    public string name = "";
    public string img_url = "";
    public Texture texture360 = null;
}


public class PortalCtrl : MonoBehaviour
{
    [Header("[傳送門]")]
    [SerializeField] GameObject sf_obj_portal;

    [Header("[攝影機]")]
    [SerializeField] GameObject sf_cam;
    float c_show_dist = 70.0f; // 固定距離 寫死

    [Header("Rander360")]
    [SerializeField] Renderer sf_renderer_360;
    //[SerializeField] Texture2D[] sf_texture_local_arr2;
    List<Texture2D> m_img_360_arr = new List<Texture2D>();
    int m_cur_local_texture_index = 0;
    //public UnityEditor.TextureImporterFormat format;

    GameFlowCtrl m_GameFlowCtrl = null;
    DirCtrl m_DirCtrl = null;
    CoverCtrl m_CoverCtrl = null;
    MyTools m_MyTools = null;
    public void init()
    {
        if (m_GameFlowCtrl == null)
        {
            m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();
        }

        if (m_DirCtrl == null)
        {
            m_DirCtrl = MyFinder.Instance.getDirCtrl();
        }

        if (m_CoverCtrl == null)
        {
            m_CoverCtrl = MyFinder.Instance.getCoverCtrl();
        }

        if (m_MyTools == null)
        {
            m_MyTools = MyFinder.Instance.getMyTools();
        }

        //setPortalPos(); 
        sf_obj_portal.SetActive(false); // 2302 -100 306

        //m_img_360_arr.Clear();
        //for (int i = 0; i < sf_texture_local_arr2.Length; i++)
        //{

        //    Texture2D new_tex = m_MyTools.ChangeTexture2DFormat(
        //              ref sf_texture_local_arr2[i]
        //            , 1820
        //            , 906
        //            , TextureFormat.RGBA32
        //        );
        //    m_img_360_arr.Add(new_tex);


        //    //Texture2D my_texture = new Texture2D(1820, 906, TextureFormat.RGBA32, false);
        //    //byte[] byte_arr = MyFinder.Instance.getMyTools().loadImage(
        //    //        m_game_flow_list[i].init_set.img_url
        //    //    );
        //    //my_texture.LoadImage(byte_arr);
        //    //m_game_flow_list[i].init_set.texture360 = my_texture;
        //}

        //StartCoroutine(downloadLocalImage());
    }

    // 拉到離攝影機固定距離
    public void initPortal(PortalSet _set)
    {
        // open
        sf_obj_portal.SetActive(true);

        // pos normal高度綁0.2f
        //Vector3 fix_pos = new Vector3(
        //        _set.pos.x
        //      , 0
        //      , _set.pos.z
        //    );
        Vector3 show_pos = m_MyTools.CameraDistWorldPos(sf_cam.transform, _set.pos, c_show_dist, true, 0.2f);
        sf_obj_portal.transform.position = show_pos;

        // dir Arrow
        m_DirCtrl.setTarget(sf_obj_portal.transform);
    }


    public Renderer get360Randerer()
    {
        return sf_renderer_360;
    }


    //IEnumerator downloadLocalImage()
    //{
    //    // 直接下載到local
    //    string local_path = MyFinder.Instance.getMyTools().getLocalPath()
    //                        + str_image_name_start + index_texture.ToString() + str_image_name_end;

    //    yield return MyFinder.Instance.getMyTools().dolwloadImageToLocal(
    //              url
    //            , local_path
    //        );
    //}

    public void switch360Image()
    {
        Debug.Log("----switch360Image");
        //[SerializeField] Texture[] sf_texture_local_arr;
        //int m_cur_local_texture_index = 0;
        m_cur_local_texture_index++;
        if (m_cur_local_texture_index == m_max_local_360_img)
        {
            m_cur_local_texture_index = 0;
        }
        sf_renderer_360.material.SetTexture("_MainTex", m_img_360_arr[m_cur_local_texture_index]);
    }


    bool m_is_click_portal = false;
    public void clickPortal()
    {
        if (m_is_click_portal)
        {
            Debug.Log("傳送門 防止連擊");
            return;
        }
        m_is_click_portal = true;

        Debug.Log("傳送門 點擊");
        m_CoverCtrl.playFadeIn();
        //m_GameFlowCtrl.toNextFlow();
    }

    // 由CoverCtrl fadeinCallback 呼叫 結束防止連擊傳送門
    public void resetClickPortect()
    {
        m_is_click_portal = false;
    }

    public void fadeOut360Texture(GameFlowUnit _game_flow_unit)
    {
        Debug.Log("*** fadeOut360Texture ***");
        sf_renderer_360.material.SetTexture("_MainTex", _game_flow_unit.portal_set.texture360);
        m_CoverCtrl.playFadeOut();
        sf_obj_portal.SetActive(false);

        m_GameFlowCtrl.toNextFlow();
    }

    
    //public IEnumerator downloadLocalImage()
    //{
    //    int index = 0;
    //    for (int i = 0; i < m_max_local_360_img; i++)
    //    {
    //        // 直接下載到local
    //        string local_path = MyFinder.Instance.getMyTools().getLocalPath()
    //                            + "local_img_" + index.ToString() + ".jpg";

    //        yield return MyFinder.Instance.getMyTools().dolwloadImageToLocal(
    //                  "http://ce-studio.tw/schoolfiles/test0" + index + ".jpg"
    //                , local_path
    //            );
    //        index++;
    //    }

    //}

    int m_max_local_360_img = 10;
    public void loadLocalImage()
    {
        m_img_360_arr.Clear();
        int index = 0;
        for (int i = 0; i < m_max_local_360_img; i++)
        {
            Texture2D my_texture = new Texture2D(1820, 906, TextureFormat.RGBA32, false);
            byte[] byte_arr = MyFinder.Instance.getMyTools().loadImage(
                    MyFinder.Instance.getMyTools().getLocalPath()+ "local_img_" + index.ToString() + ".jpg"
                );
            my_texture.LoadImage(byte_arr);
            m_img_360_arr.Add(my_texture);
            //m_game_flow_list[i].init_set.texture360 = my_texture;


            //// 直接下載到local
            //string local_path = MyFinder.Instance.getMyTools().getLocalPath()
            //                    + "local_img_" + index.ToString() + ".jpg";

            //yield return MyFinder.Instance.getMyTools().dolwloadImageToLocal(
            //          "http://ce-studio.tw/schoolfiles/test0" + index + ".jpg"
            //        , local_path
            //    );
            index++;
        }


        
       
         
    }
}
