﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverCallbacks : MonoBehaviour
{
    public void closeCover()
    {
        this.gameObject.SetActive(false);
    }

    CoverCtrl m_CoverCtrl = null;
    public void callFadeinCallback()
    {
        if (m_CoverCtrl == null)
        {
            m_CoverCtrl = MyFinder.Instance.getCoverCtrl();
        }

        m_CoverCtrl.fadeinCallback();
    }
}
