﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoverCtrl : MonoBehaviour
{
    [Header("Anim")]
    [SerializeField] Animator sf_anim;

    [Header("Image")]
    [SerializeField] Image sf_img_cover;

    [Header("AnimBloody")]
    [SerializeField] Animator sf_anim_bloody;

    [Header("Loading")]
    [SerializeField] GameObject sf_obj_loading;

    [Header("AwardAlarm")]
    [SerializeField] GameObject sf_obj_award_alarm;
    [SerializeField] Text sf_txt_award_alarm;

    public void playAwardAlarm(string _str)
    {
        sf_obj_award_alarm.SetActive(true);
        sf_txt_award_alarm.text = _str;
    }

    public void playBloody()
    {
        if (sf_anim_bloody.gameObject.activeSelf) return;
        sf_anim_bloody.gameObject.SetActive(true);
    }

    bool m_firsttime_fadeout = false;
    public void playFadeOut()
    {
        sf_anim.gameObject.SetActive(true);
        sf_img_cover.color = new Vector4(0, 0, 0, 1);
        sf_anim.enabled = true;
        sf_anim.Play("cover_fadeout");

        if (!m_firsttime_fadeout)
        {
            m_firsttime_fadeout = true;
            sf_obj_loading.gameObject.SetActive(false);
        }
    }

    public void playFadeIn()
    {
        sf_anim.gameObject.SetActive(true);
        sf_img_cover.color = new Vector4(0, 0, 0, 0);
        sf_anim.enabled = true;
        sf_anim.Play("cover_fadein");
    }

    GameFlowUnit m_GameFlowUnit;
    GameFlowCtrl m_GameFlowCtrl;    
    PortalCtrl m_PortalCtrl;
    BuildingCtrl m_BuildingCtrl;
    public enum FadeinToTypeEnum
    {
        none,
        follow_gameflow,
        from_building_to_another,
        length
    }

    FadeinToTypeEnum m_fadeintype_enum = FadeinToTypeEnum.follow_gameflow;
    public void changeFadeinToType(FadeinToTypeEnum _enum , BuildingCtrl.buildings_order_enum _to_enum)
    {
        Debug.Log("changeFadeinToType: "+ _enum);
        m_fadeintype_enum = _enum;

        // 如果是塔防 那type要改 game flow <from building to another 從building承接到>
        if (_to_enum == BuildingCtrl.buildings_order_enum.tower_game_flow)
        {
            m_fadeintype_enum = FadeinToTypeEnum.follow_gameflow;
        }
    }


    public void init()
    {
        if (m_GameFlowCtrl == null)
        {
            m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();
        }

        if (m_BuildingCtrl == null)
        {
            m_BuildingCtrl = MyFinder.Instance.getBuildingCtrl();
        }

        if (m_PortalCtrl == null)
        {
            m_PortalCtrl = MyFinder.Instance.getPortalCtrl();
        }

        sf_anim.gameObject.SetActive(true);
        sf_img_cover.color = new Vector4(0, 0, 0, 1);
        sf_anim.enabled = false;
    }
    
    public void fadeinCallback()
    {
        StartCoroutine(_fadeinCallback());
    }

    IEnumerator _fadeinCallback()
    {
        Debug.Log("@@@ fadeinCallback @@@ m_fadeintype_enum = " + m_fadeintype_enum);
        
        switch (m_fadeintype_enum)
        {
            case FadeinToTypeEnum.none:
                break;
            case FadeinToTypeEnum.follow_gameflow:

                // 如果是剛進入塔防game flow
                int i_cur_flow_index = m_GameFlowCtrl.getCurFlowIndex();
                if (i_cur_flow_index == -1)
                {
                    m_GameFlowCtrl.initGameFlow(); 
                    m_BuildingCtrl.showFadeinToBuilding(); // 這邊會一併處理fade out
                    break;
                }

                // 如果gameflow 是portal => 切換360圖片
                m_GameFlowUnit = m_GameFlowCtrl.getCurFlow();
                GameFlowUnit.GemeFlowTypeEnum game_flow_type = m_GameFlowUnit.type;
                if (game_flow_type == GameFlowUnit.GemeFlowTypeEnum.portal)
                {
                    m_PortalCtrl.fadeOut360Texture(m_GameFlowUnit);
                    m_PortalCtrl.resetClickPortect();
                    Debug.Log("傳送門 點擊後 完全轉黑*************************************");
                }
                Debug.Log("game_flow_type = " + game_flow_type);
                break;
            case FadeinToTypeEnum.from_building_to_another:
                /// 
                m_BuildingCtrl.showFadeinToBuilding();
                break;
            case FadeinToTypeEnum.length:
                break;
            default:
                break;
        }

        yield break;
    }
}
