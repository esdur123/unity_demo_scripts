﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InitSet
{
    public string img_url = "";
    public Texture texture360 = null;
}


public class FlowInitCtrl : MonoBehaviour
{
    //[Header("Rander")]
    //[SerializeField] Renderer sf_rander_360;

    GameFlowCtrl m_GameFlowCtrl = null;
    PortalCtrl m_PortalCtrl = null;
    Renderer m_renderer_360 = null;
    CoverCtrl m_CoverCtrl = null;
    public void init()
    {
        if (m_GameFlowCtrl == null)
        {
            m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();
        }

        if (m_PortalCtrl == null)
        {
            m_PortalCtrl = MyFinder.Instance.getPortalCtrl();
        }

        m_renderer_360 = m_PortalCtrl.get360Randerer();

        m_CoverCtrl = MyFinder.Instance.getCoverCtrl();

    }

    public void initFlow(InitSet _set)
    {
        m_renderer_360.material.SetTexture("_MainTex", _set.texture360);
        


        m_GameFlowCtrl.toNextFlow();
        //m_CoverCtrl.playFadeOut();
        //MyFinder.Instance.getBuildingCtrl().showFadeinToBuilding()
    }
}
