using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class UIDebugCtrl : MonoBehaviour
{
    [Header("Main")]
    [SerializeField] Main sf_main;
    [Header("Txt")]
    [SerializeField] Text sf_txt;
    [SerializeField] Text sf_txt_top;
    [SerializeField] Text sf_txt_upup;
    [Header("MouseLook")]
    [SerializeField] UIDebugMouseLook sf_mouse_look;
    [Header("CenterRay")]
    //[SerializeField] UIDebugCenterRay sf_center_ray;
    [Header("Info在Loading上面顯示")]
    [SerializeField] Text sf_txt_info_init;

    //bool m_is_debug = false;
    Main.buildModeEnum m_playmode;
    bool m_is_debug_ui = false;
    public void init()
    {
        m_playmode = sf_main.p_playmode;
        m_is_debug_ui = sf_main.is_debug_ui;
        sf_txt.gameObject.SetActive(false);

        //sf_center_ray.init();
        //sf_center_ray.getCenterCycle().gameObject.SetActive(false);
        

        //if (m_playmode != Main.buildModeEnum.vive)//(sf_main.is_debug)
        //{
        //    sf_center_ray.getCenterCycle().gameObject.SetActive(true);
        //}

        if (sf_main.is_debug_ui)
        {
            sf_txt.gameObject.SetActive(true);
            sf_txt_top.gameObject.SetActive(true);
            sf_txt_upup.gameObject.SetActive(true);
        }
        else
        {
            sf_txt.gameObject.SetActive(false);
            sf_txt_top.gameObject.SetActive(false);
            sf_txt_upup.gameObject.SetActive(false);
        }

    }


    public void showBeforeInfoInitTxt(string _str)
    {
        sf_txt_info_init.text = _str;
    }


    public void showDebugTxt(string _str, int _num = 0)
    {
        if (!m_is_debug_ui) return;
        if (_num == 0) sf_txt.text = _str;
        if (_num == 1) sf_txt_top.text = _str;
        if (_num == 2) sf_txt_upup.text = _str;

    }

    bool m_is_allow_mouselook = true;
    public void setDebugMousLook(bool _is_allow)
    {
        m_is_allow_mouselook = _is_allow;
    }


    public void myUpdate()
    {
        //if (m_playmode==Main.buildModeEnum.vive) return;        
        //sf_center_ray.myUpdate();

        if (!m_is_allow_mouselook) return;
        sf_mouse_look.myUpdate();
    }



    //int aaaa = 0;
    //public void testClick()
    //{
    //    aaaa++;
    //    sf_txt_upup.text = aaaa.ToString();
    //}
}
