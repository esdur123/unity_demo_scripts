using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDebugMouseLook : MonoBehaviour
{
    public enum RotationAxes
    {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2
    }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityHor = 9f;
    public float sensitivityVert = 9f;

    public float minmumVert = -45f;
    public float maxmumVert = 45f;

    private float _rotationX = 0;

    [Header("Camera")]
    [SerializeField] Transform sf_tr_Traget;


    // Update is called once per frame
    public void myUpdate()
    {
        // �� UIDebugCtrl����}�� 

        if (!Input.GetKey(KeyCode.LeftAlt)) return;

        if (axes == RotationAxes.MouseX)
        {
            sf_tr_Traget.Rotate(0, Input.GetAxis("Mouse X") * sensitivityHor, 0);
        }
        else if (axes == RotationAxes.MouseY)
        {
            _rotationX = _rotationX - Input.GetAxis("Mouse Y") * sensitivityVert;
            _rotationX = Mathf.Clamp(_rotationX, minmumVert, maxmumVert);

            float rotationY = sf_tr_Traget.localEulerAngles.y;

            sf_tr_Traget.localEulerAngles = new Vector3(_rotationX, rotationY, 0);
        }
        else
        {
            _rotationX -= Input.GetAxis("Mouse Y") * sensitivityVert;
            _rotationX = Mathf.Clamp(_rotationX, minmumVert, maxmumVert);

            float delta = Input.GetAxis("Mouse X") * sensitivityHor;
            float rotationY = sf_tr_Traget.localEulerAngles.y + delta;

            sf_tr_Traget.localEulerAngles = new Vector3(_rotationX, rotationY, 0);
        }

    }
}
