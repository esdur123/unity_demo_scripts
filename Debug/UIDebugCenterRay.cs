using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UIDebugCenterRay : MonoBehaviour
{
    Transform m_tr_base_item;
    [Header("CenterCycle")]
    [SerializeField] Transform sf_tr_center_cycle;
    Vector3 m_sacle_normal = new Vector3(0.05f, 0.05f, 0.05f);
    Vector3 m_sacle_hit = new Vector3(0.1f, 0.1f, 0.1f);
    Main.buildModeEnum m_playmode;
    //bool is_lock_center_point = false;

    [Header("[LineRenderer]")]
    [SerializeField] LineRenderer sf_line_renderer;
    //[Header("[LineRendererDebug]")]
    //[SerializeField] LineRenderer sf_line_renderer_debug;
    [Header("[LineStart]")]
    [SerializeField] Transform sf_tr_line_start;
    float m_line_length = 100f;


    MySystem m_MySystem = null;
    public void init() 
    {
        m_MySystem = MyFinder.Instance.getMySystem();
        sf_line_renderer.enabled = false;
        m_playmode = MyFinder.Instance.getMain().p_playmode;
        //is_lock_center_point = MyFinder.Instance.getMain().is_lock_center_point;

        // fake_AR
        m_tr_base_item = MyFinder.Instance.getMySystem().sf_tr_camera;
        //if (m_playmode == Main.buildModeEnum.fake_ar && !is_lock_center_point)
        //{

        //    m_tr_base_item = sf_tr_line_start;//MyFinder.Instance.getMySystem().sf_tr_higher_red_point;
        //    sf_line_renderer.enabled = true;
        //    sf_line_renderer.SetWidth(0.03f, 0.02f);
        //}
        getCenterCycle().gameObject.SetActive(false);
        if (m_playmode != Main.buildModeEnum.vive)//(sf_main.is_debug)
        {
            getCenterCycle().gameObject.SetActive(true);
        }
    }

    public Transform getCenterCycle()
    {
        return sf_tr_center_cycle;
    }

    // Update is called once per frame
    RaycastHit HitInfo;
    IDebugOnTrigger m_cur_hit_inter_face = null;
    IDebugOnTrigger m_last_hit_inter_face = null;
    GameObject m_cur_hit_obj = null;
    GameObject m_last_hit_obj = null;
    Vector3 m_ray_dir = new Vector3();
    public void myUpdate()
    {
        if (m_is_allow_center_ray)
        {
            centerRay();
        }

        if (m_is_allow_mouse_pos_ray)
        {
            mousePositionRay();
        }
       
    }

    void mousePositionRay()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("mousePositionRay: mouse btn down(0)");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                IDebugOnTrigger clicked_interface = hit.collider.gameObject.GetComponent<IDebugOnTrigger>();

                if (clicked_interface != null)
                {
                    clicked_interface.OnDebugClick();
                }
            }
        }
    }


    public void setCenterRay(bool _is_allow)
    {
        m_is_allow_center_ray = _is_allow;

        if (_is_allow)
        {
            sf_tr_center_cycle.gameObject.SetActive(true);
        }
        else
        {
            sf_tr_center_cycle.gameObject.SetActive(false);
        }
    }

    public void setMousePosRay(bool _is_allow)
    {
        m_is_allow_mouse_pos_ray = _is_allow;
    }


    bool m_is_allow_center_ray = true;
    bool m_is_allow_mouse_pos_ray = true;
    void centerRay()
    {    
        // �� UIDebugCtrl����}�� 
        m_ray_dir = m_tr_base_item.forward;

        //if (m_playmode == Main.buildModeEnum.fake_ar && !is_lock_center_point)
        //{
        //    Vector3 end_pos = m_tr_base_item.position
        //                      + m_tr_base_item.forward * m_line_length;
        //    m_ray_dir = end_pos - m_tr_base_item.position;
        //    m_ray_dir = m_ray_dir.normalized;
        //}

        //Debug.DrawRay(sf_tr_line_start.position, dir, Color.green, 10000);
        //if (Physics.Raycast(m_tr_base_item.position, dir, out HitInfo, 1000.0f))
        if (Physics.Raycast(m_tr_base_item.position, m_ray_dir, out HitInfo, 1000.0f))
        {
            sf_tr_center_cycle.localScale = m_sacle_hit;
            m_cur_hit_obj = HitInfo.collider.gameObject;
            m_cur_hit_inter_face = HitInfo.collider.gameObject.GetComponent<IDebugOnTrigger>();

            if (m_last_hit_obj != m_cur_hit_obj
                && m_cur_hit_inter_face != null
                )
            {
                m_cur_hit_inter_face.OnDebugEnter();
            }

            if (Input.GetMouseButtonDown(0) && !m_MySystem.isClickBtn())
            {
                //Debug.Log("isClickBtn="+MyFinder.Instance.getMySystem().isClickBtn());
                if (m_cur_hit_inter_face != null)
                {
                    m_cur_hit_inter_face.OnDebugClick();
                }
            }

            //if (m_playmode == Main.buildModeEnum.fake_ar && !is_lock_center_point)
            //{
            //    sf_line_renderer.SetPosition(0, m_tr_base_item.position);
            //    sf_line_renderer.SetPosition(1, HitInfo.point);
            //    sf_tr_center_cycle.position = HitInfo.point;
            //}

        }
        else
        {
            sf_tr_center_cycle.localScale = m_sacle_normal;
            m_cur_hit_inter_face = null;
            m_cur_hit_obj = null;

            //if (m_playmode == Main.buildModeEnum.fake_ar && !is_lock_center_point)
            //{
            //    sf_line_renderer.SetPosition(0, m_tr_base_item.position);
            //    Vector3 far_pos = m_tr_base_item.position
            //                      + m_tr_base_item.forward * m_line_length;
            //    sf_line_renderer.SetPosition(1, far_pos);
            //    sf_tr_center_cycle.position = far_pos;
            //}
        }

        if (m_cur_hit_inter_face != m_last_hit_inter_face
               && m_last_hit_inter_face != null
            )
        {
            m_last_hit_inter_face.OnDebugExit();
        }

        m_last_hit_inter_face = m_cur_hit_inter_face;
        m_last_hit_obj = m_cur_hit_obj;
    }
    

}
