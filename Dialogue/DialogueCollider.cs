﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueCollider : MonoBehaviour, IDebugOnTrigger
{

    DialogueCtrl m_DialogueController = null;
    public void OnClick()
    {
        if (m_DialogueController == null)
        {
            m_DialogueController = MyFinder.Instance.getDialogueCtrl();
        }

        //Debug.Log("按下看板 collider");
        m_DialogueController.clickDialogue();
        
    }



    // debug 要用的
    void IDebugOnTrigger.OnDebugClick()
    {
        OnClick();
    }

    void IDebugOnTrigger.OnDebugEnter()
    {
    }

    void IDebugOnTrigger.OnDebugExit()
    {
    }
}
