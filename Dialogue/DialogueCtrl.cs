﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

/// <summary>
/// 對話的Unit
/// </summary>
[System.Serializable]
public class DialogueUnit
{
    public string content = "";
    public int i_speaker = -1;
    public List<int> i_show_char_list = new List<int>();
}

/// <summary>
/// 測試存Local@@!
/// </summary>
[System.Serializable]
public class DialogueSets
{
    //public BuildingCtrl.buildings_order_enum env_type = BuildingCtrl.buildings_order_enum.sphere_360;
    public Vector3 pos;
    public List<string> all_char_name_list = new List<string>();
    public List<DialogueUnit> unit_list;
    public string first_content = "";
    public bool first_all_show = false;
}


public class DialogueCtrl : MonoBehaviour
{

    public void init()
    {

        if (m_GameFlowCtrl == null)
        {
            m_GameFlowCtrl = MyFinder.Instance.getGameFlowCtrl();
        }

        if (m_DirCtrl == null)
        {
            m_DirCtrl = MyFinder.Instance.getDirCtrl();
        }

        m_is_first_time_finished_all_dialogue = false;
        //checkOrCreateSample("Sample_0.txt", "樣本0");

        //Debug.LogError(sets.unit_list[0].content);


        sf_tr_dialogue_root.gameObject.SetActive(false);


        //debugCreateJson();
        //debugReadJson();
        //MyFinder.Instance.getMyTools().downloadImage(
        //          "https://www.teatop.com.tw/upload/catalog_tea_b/ALL_catalog_tea_21G21_i0c7lj7JQC.jpg"
        //        , Application.persistentDataPath + "/nori/aaa.jpg"
        //    );

        //SaveCSVTest();
    }


    public void myUpdate()
    {
        if (!m_is_active) return;
        updatingDiologue();
    }


    //public void reInit()
    //{
    //    m_is_first_time_finished_all_dialogue = false;
    //}

    [Header("[對話角色]")]    
    [SerializeField] DialogueCharacterInfo[] sf_info_chars;
    [SerializeField] DialogueCharacterInfo[] sf_info_chars_env_en_game;
    DialogueCharacterInfo[] m_cur_info_chars = null;

    [Header("[對話位置]")]    
    [SerializeField] Transform[] sf_tr_char_talk_pos;
    Transform[] m_cur_char_talk_tr = null;

    [Header("[名字]")]    
    [SerializeField] Text sf_txt_char_talk_name;
    Text m_txt_char_talk_name = null;

    [Header("[Dialogue本體]")]    
    [SerializeField] Transform sf_tr_dialogue_root;
    [SerializeField] Transform sf_tr_dialogue_root_en_game;
    Transform m_tr_dialogue_root= null;

    [Header("[對話結束-寶石]")]    
    [SerializeField] GameObject sf_obj_gem;
    [SerializeField] GameObject sf_obj_gem_en;
    GameObject m_obj_gem = null;

    [Header("[對話文字]")]
    [SerializeField] Text sf_txt;
    [SerializeField] Text sf_txt_en;
    Text m_txt = null;


    [Header("[攝影機]")]
    [SerializeField] Transform sf_tr_camera;
    //float m_look_cam_y = 0;//-124.0f; // 聚焦高度
    float m_dist_to_camera = 43.3f; // 對話版跟攝影機的距離 寫死
    const float c_dist_to_camera_360 = 43.3f;
    const float c_dist_to_en_game = 300.2231f;
    public enum character_order_enum // 必須對應↑↑ sf_info_chars 的順序
    {
        unity_chan,
        girl_rin,
        boy_satoim,
        girl_magic,
        robert_red,
        animal_whale,
        fruit_pear,
        animal_sheep,
        plant_mouse,
        animal_groundhog,
        animal_cat,
        length
    }

    public void initDialogueEn(DialogueSets _sets)
    {
        m_is_first_time_finished_all_dialogue = false;

        Debug.Log("**********  initDialogueEn");
        m_cur_info_chars = sf_info_chars_env_en_game;
        //m_cur_char_talk_tr = sf_tr_char_talk_pos;
        //m_txt_char_talk_name = sf_txt_char_talk_name;
        m_tr_dialogue_root = sf_tr_dialogue_root_en_game;
        m_obj_gem = sf_obj_gem_en;
        m_dist_to_camera = c_dist_to_en_game;
        m_txt = sf_txt_en;
        

        /// active
        sf_tr_dialogue_root_en_game.gameObject.SetActive(true);

        /// face_to, position & dist
        //Debug.Log("world pos: "+m_tr_dialogue_root.position);
        m_tr_dialogue_root.localPosition = _sets.pos; 
        Vector3 dir = m_tr_dialogue_root.position - sf_tr_camera.position;
        dir.Normalize();

        float rot_x = m_tr_dialogue_root.localRotation.eulerAngles.x;
        m_tr_dialogue_root.position = sf_tr_camera.position + dir * m_dist_to_camera;
        m_tr_dialogue_root.LookAt(sf_tr_camera.position);

        /// rot
        Vector3 dialogue_rot = new Vector3(
                  rot_x
                , m_tr_dialogue_root.localRotation.eulerAngles.y
                , 0
            );

        m_tr_dialogue_root.localRotation = Quaternion.Euler(
                 dialogue_rot
            );

        /// dir Arrow
        m_DirCtrl.setTarget(m_tr_dialogue_root);

        /// dialogue list
        m_dialogueList.Clear();
        for (int i = 0; i < _sets.unit_list.Count; i++)
        {
            m_dialogueList.Add(_sets.unit_list[i]);
        }

        // 第一次出現時 要秀哪段文字
        m_txt.text = _sets.first_content;

        sf_obj_gem.SetActive(false);
        initTypingMachine();

        //// 現在正在講話的人物 (EnGame不顯示XD)
        //int i_speaker = m_dialogueList[0].i_speaker;
        //if (i_speaker != -1)
        //{
        //    m_txt_char_talk_name.text = "[" + charNameDic[i_speaker] + "]";
        //}
    }



    DirCtrl m_DirCtrl = null;
    List<DialogueUnit> m_dialogueList = new List<DialogueUnit>();
    string m_cur_json = "";
    //string c_start_words = "[請點擊test]";
    public Dictionary<int, string> charNameDic = new Dictionary<int, string>();
    public void initDialogue(DialogueSets _sets)
    {
        m_is_first_time_finished_all_dialogue = false;

        Debug.Log("**********  initDialogue: pos= "+ _sets.pos);
        m_cur_info_chars = sf_info_chars;
        m_cur_char_talk_tr = sf_tr_char_talk_pos;
        m_txt_char_talk_name = sf_txt_char_talk_name;
        m_tr_dialogue_root = sf_tr_dialogue_root;
        m_obj_gem = sf_obj_gem;
        m_dist_to_camera = c_dist_to_camera_360;
        m_txt = sf_txt;        

        /// active
        m_tr_dialogue_root.gameObject.SetActive(true);



        /// face_to, position & dist
        //m_tr_dialogue_root.localPosition = _sets.pos;
        //Vector3 dir = m_tr_dialogue_root.position - sf_tr_camera.position;
        //dir.Normalize();
        //m_tr_dialogue_root.position = sf_tr_camera.position + dir * m_dist_to_camera;
        //Vector3 _pos_fix = new Vector3(_sets.pos.x, 0, _sets.pos.z);// _sets.pos; 
        //_pos_fix.Normalize();
        //_pos_fix = new Vector3(_sets.pos.x, 0.25f, _sets.pos.z);
        m_tr_dialogue_root.position = MyFinder.Instance.getMyTools().CameraDistWorldPos(sf_tr_camera, _sets.pos, m_dist_to_camera, true, 0);
        m_tr_dialogue_root.LookAt(sf_tr_camera.position);

        /// rot
        //Vector3 dialogue_rot = new Vector3(
        //          0
        //        , m_tr_dialogue_root.localRotation.eulerAngles.y
        //        , 0
        //    );

        //m_tr_dialogue_root.localRotation = Quaternion.Euler(
        //         dialogue_rot
        //    );

        /// dir Arrow
        m_DirCtrl.setTarget(m_tr_dialogue_root);        

        /// all_char_list
        charNameDic.Clear();

        //Debug.Log(_sets.all_char_name_list.Count);
        if (_sets.all_char_name_list.Count > 0)
        {
            for (int i = 0; i < m_cur_info_chars.Length; i++) // 手動平行 m_cur_info_chars 和 charNameDic
            {

                charNameDic.Add(i, _sets.all_char_name_list[i]);

                m_cur_info_chars[i].init_local_pos = sf_info_chars[i].gameObject.transform.localPosition;
                m_cur_info_chars[i].init_local_rot = new Vector3(
                          m_cur_info_chars[i].gameObject.transform.localRotation.eulerAngles.x
                        , m_cur_info_chars[i].gameObject.transform.localRotation.eulerAngles.y
                        , m_cur_info_chars[i].gameObject.transform.localRotation.eulerAngles.z
                    );

                m_cur_info_chars[i].gameObject.transform.localRotation = Quaternion.Euler(
                         m_cur_info_chars[i].init_local_rot
                    );
            }
        }

        /// dialogue list
        m_dialogueList.Clear();
        for (int i = 0; i < _sets.unit_list.Count; i++)
        {
            m_dialogueList.Add(_sets.unit_list[i]);
        }

        //Debug.LogError(m_dialogueList.Count);
        //Debug.LogError(m_dialogueList[1].i_show_char_list.Count);
        //Debug.LogError(m_dialogueList[0].i_show_char_list[i]);
        
        // 第一次出現時 要秀哪段文字
        m_txt.text = _sets.first_content;
        if (!_sets.first_all_show)
        {
            //關閉所有人物
            for (int i = 0; i < m_cur_info_chars.Length; i++)
            {
                m_cur_info_chars[i].gameObject.SetActive(false);
            }

            //開第一次需要的人物
            for (int i = 0; i < m_dialogueList[0].i_show_char_list.Count; i++)
            {
                //Debug.LogError(i);
                //Debug.LogError(m_dialogueList[0].i_show_char_list[i]);
                //Debug.LogError(m_cur_info_chars.Length);
                //m_cur_info_chars[11].gameObject.SetActive(true);
                m_cur_info_chars[m_dialogueList[0].i_show_char_list[i]].gameObject.SetActive(true);
                m_cur_info_chars[m_dialogueList[0].i_show_char_list[i]].gameObject.transform.localPosition
                    = new Vector3(
                            m_cur_char_talk_tr[i].localPosition.x
                            , m_cur_info_chars[m_dialogueList[0].i_show_char_list[i]].talk_height
                            , m_cur_info_chars[m_dialogueList[0].i_show_char_list[i]].talk_z
                        );

                m_cur_info_chars[m_dialogueList[0].i_show_char_list[i]].gameObject.transform.localRotation = Quaternion.Euler(
                         m_cur_info_chars[m_dialogueList[0].i_show_char_list[i]].talk_local_rot
                    );
            }
        }
        else
        {
            //開啟所有人物
            for (int i = 0; i < m_cur_info_chars.Length; i++)
            {
                m_cur_info_chars[i].gameObject.SetActive(true);
            }
        }

        m_obj_gem.SetActive(false);        
        initTypingMachine();

        // 現在正在講話的人物       
        int i_speaker = m_dialogueList[0].i_speaker;
        if (i_speaker != -1)
        {
            m_txt_char_talk_name.text = "[" + charNameDic[i_speaker] + "]";
        }

    }




    float m_speed = 0.1f;//打字时间间隔
    string m_show_words = "";//保存需要显示的文字
    bool m_is_active = false;
    float m_timer = 0;//计时器
    int m_cur_char = 0;//当前打字位置
    public int m_i_cur_unit = 0;
    public int m_i_cur_last_word = 0;
    bool m_is_finish_all_list = false;
    bool m_is_end_of_this_unit = false;
    void initTypingMachine()
    {
        m_i_cur_last_word = 0;
        m_cur_page_status = enumPageStates.start;
        m_i_cur_unit = 0;
        m_show_words = "";
        m_timer = 0;
        m_is_active = false;

        // 文字顏色
        //Color my_color = Color.white;
        //ColorUtility.TryParseHtmlString("#000000", out my_color);
        //m_txt.color = my_color;

    }


    public void setTextWithEllipsis()
    {
        // 人物先關掉
        for (int i = 0; i < m_cur_info_chars.Length; i++)
        {
            m_cur_info_chars[i].gameObject.SetActive(false);
        }

        // 開需要的人物 並放到正確位置
        //Debug.Log(m_dialogueList.Count);
        //Debug.Log(m_i_cur_unit);        
        for (int i = 0; i < m_dialogueList[m_i_cur_unit].i_show_char_list.Count; i++)
        {
            m_cur_info_chars[m_dialogueList[m_i_cur_unit].i_show_char_list[i]].gameObject.SetActive(true);
            m_cur_info_chars[m_dialogueList[m_i_cur_unit].i_show_char_list[i]].gameObject.transform.localPosition
                = new Vector3(
                        m_cur_char_talk_tr[i].localPosition.x
                        , m_cur_info_chars[m_dialogueList[m_i_cur_unit].i_show_char_list[i]].talk_height
                        , m_cur_info_chars[m_dialogueList[m_i_cur_unit].i_show_char_list[i]].talk_z
                    );

            m_cur_info_chars[m_dialogueList[m_i_cur_unit].i_show_char_list[i]].gameObject.transform.localRotation = Quaternion.Euler(
                     m_cur_info_chars[m_dialogueList[m_i_cur_unit].i_show_char_list[i]].talk_local_rot
                );
        }

        // 現在正在講話的人物       
        int i_speaker = m_dialogueList[m_i_cur_unit].i_speaker;
        if (i_speaker != -1)
        {
            m_txt_char_talk_name.text = "[" + charNameDic[i_speaker] + "]";
        }
        


        string value = m_dialogueList[m_i_cur_unit].content;
        value = value.Substring(m_i_cur_last_word, (value.Length)- m_i_cur_last_word);
        if (m_i_cur_last_word != 0)
        {
            value = "..." + value;
        }


        TextGenerator generator = new TextGenerator();
        RectTransform rectTransform = m_txt.GetComponent<RectTransform>();
        TextGenerationSettings settings = m_txt.GetGenerationSettings(rectTransform.rect.size);
        generator.Populate(value, settings);
        int characterCountVisible = generator.characterCountVisible; // 這裡可以算出能承受多少字元
        string cutText = value;

        //Debug.Log("value.Length="+ value.Length+ ", Visible="+ characterCountVisible);
        
        if (value.Length <= characterCountVisible)
        {
            m_i_cur_last_word = 0;
            m_is_end_of_this_unit = true;
            m_cur_page_status = enumPageStates.finish;
        }
        else
        {
            
            m_is_end_of_this_unit = false;
            m_i_cur_last_word += characterCountVisible - 3;

            if (m_i_cur_last_word < value.Length)
            {
                cutText = value.Substring(0, m_i_cur_last_word);
                cutText += "…";
                m_cur_page_status = enumPageStates.unfinish;
            }
            else
            {
                cutText = value;
                m_cur_page_status = enumPageStates.finish;
            }           
        }

        m_show_words = cutText;
    }


    /// <summary>
    /// 执行打字任务
    /// </summary>
    void updatingDiologue()
    {
        if (m_is_active)
        {
            m_timer += Time.deltaTime;
            if (m_timer >= m_speed)
            {//判断计时器时间是否到达
                m_timer = 0;
                m_cur_char++;
                Debug.Log("m_show_words = "+ m_show_words + ", m_cur_char ="+ m_cur_char);
                m_txt.text = m_show_words.Substring(0, m_cur_char);//刷新文本显示内容
                //Debug.Log(m_cur_char);
                if (m_cur_char >= m_show_words.Length)
                {
                    endingCurPage();
                }

                //m_is_end_of_this_unit = true;
            }
        }
    }


    AudioCtrl m_AudioCtrl = null;
    AudioCtrl getAudioCtrl()
    {
        if (m_AudioCtrl == null)
        {
            m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        }

        return m_AudioCtrl;
    }


    // 這只是個開關
    public void clickDialogue()
    {
        //if (_txt != "") m_show_words = _txt;
        //m_speed = _speed;
        getAudioCtrl().playClip(AudioCtrl.AudioClipsEnum.dialogue_click);

        if (m_is_first_time_finished_all_dialogue)
        {
            Debug.Log("-----------------第一次結束對話框");
            // 把對話框移到看不到的地方
            m_tr_dialogue_root.position = new Vector3(0, 3000, 0);
            //m_is_active = false;

            m_GameFlowCtrl.toNextFlow();
            return;
        }






        if (m_is_active)
        {
            endingCurPage();
            //Debug.Log("endingCurPage");
            return;
        }


        // 全部對話List結束 重來
        if (m_is_finish_all_list)
        {
            m_is_finish_all_list = false;
            Debug.Log("m_is_finish_all_list = true");
            initTypingMachine();
            setTextWithEllipsis();
            m_is_active = true;
            m_obj_gem.SetActive(false);

            //m_GameFlowCtrl.toNextFlow();
            return;
        }

        setTextWithEllipsis(); // m_i_cur_unit 已經在ending的時候++

        //Debug.Log("active dialogue");
        m_obj_gem.SetActive(false);
        m_is_active = true;
    }


    /// <summary>
    /// 结束打字，初始化数据
    /// </summary>
    public enumPageStates m_cur_page_status;
    public enum enumPageStates
    {
        start,
        unfinish,
        finish,
        length
    }


    GameFlowCtrl m_GameFlowCtrl = null;
    bool m_is_first_time_finished_all_dialogue = false;
    void endingCurPage()
    {
        Debug.Log("//////////////// Dialogue Ending Cur Page");
        m_obj_gem.SetActive(true);
        m_is_active = false;
        m_timer = 0;
        m_cur_char = 0;
        m_txt.text = m_show_words;

        // 
        if (m_is_end_of_this_unit)
        {
            m_i_cur_unit++;
        }
        
        if (m_i_cur_unit >= m_dialogueList.Count)
        {
            m_is_finish_all_list = true;

            if (!m_is_first_time_finished_all_dialogue)
            {
                m_is_first_time_finished_all_dialogue = true;


                //Debug.Log("-----------------第一次結束對話框");
                // 把對話框移到看不到的地方
                //m_tr_dialogue_root.position = new Vector3(0, 30000, 0);


                //m_GameFlowCtrl.toNextFlow();
                //m_tr_dialogue_root.gameObject.SetActive(false);


            }

            return;
        }
    }


    /// <summary>
    /// 存入 讀取 Sample 
    /// persistentDataPath 位置在 C:\Users\Nori\AppData\LocalLow\NoriCompany\SchoolVR_02a
    /// </summary>
    const string c_filePathName = "/";// "/../_SaveLocal/Dialogue/";
    //const string c_filename_sample = "DialogueSample.txt";
    public void saveLocal(string saveString, string _filename)
    {
        ///將字串存到硬碟中 
        // 新建時 要創這個資料夾: 專案名稱/_SaveLocal/Dialogue/
        FileStream fs = new FileStream(Application.persistentDataPath + c_filePathName + _filename, FileMode.OpenOrCreate);
        StreamWriter sw = new StreamWriter(fs);
        sw.WriteLine(saveString);
        sw.Close();
        fs.Close();
    }


    public DialogueSets loadLocal(string _filename)
    {
        FileStream fs01 = new FileStream(Application.persistentDataPath + c_filePathName + _filename, FileMode.OpenOrCreate);
        StreamReader sr = new StreamReader(fs01);
        string json_load = "none";
        json_load = sr.ReadLine();
        fs01.Close();
        sr.Close();

        DialogueSets sets = new DialogueSets();
        if (json_load == null)
        {
            return null;
        }

        sets = JsonUtility.FromJson<DialogueSets>(json_load);        
        return sets;
    }

    void checkOrCreateSample(string _filename, string _marked_word)
    {
        bool is_ok = false;
        // 新建時 要創這個資料夾: 專案名稱/_SaveLocal/Dialogue/
        FileStream fs01 = new FileStream(Application.persistentDataPath + c_filePathName + _filename, FileMode.OpenOrCreate);
        StreamReader sr = new StreamReader(fs01);
        string json_load = "none";
        json_load = sr.ReadLine();
        fs01.Close();
        sr.Close();

        /// 如果本地沒資料, 就初始化一個 getInitState()
        /// 改成 -> 直接覆蓋本地端
        //if (json_load == null)
        {
            ///填寫jsingleInfo格式的資料..       
            
            DialogueSets sets = createSample(_marked_word);
            //List<DialogueUnit> myInfoList = createSample(_marked_word);
            //sets.list = myInfoList;
            json_load = JsonUtility.ToJson(sets);
            saveLocal(json_load, _filename);
        }
    }


    public DialogueSets createSample(string _marked_word)
    {
        List<DialogueUnit> _list = new List<DialogueUnit>();
        DialogueUnit unit_0 = new DialogueUnit();
        unit_0.i_show_char_list = new List<int> { (int)character_order_enum.unity_chan };
        unit_0.i_speaker = (int)character_order_enum.unity_chan;
        unit_0.content = "["+ _marked_word+ "] 77777第一句"; // "(Start) 歡迎!! 我是你們的指導員 優姊~!";

        DialogueUnit unit_1 = new DialogueUnit();
        unit_1.i_show_char_list = new List<int> { (int)character_order_enum.unity_chan, (int)character_order_enum.girl_rin };
        unit_1.i_speaker = (int)character_order_enum.girl_rin;
        unit_1.content = "[" + _marked_word + "] 第二句"; // "你好 可以叫我玲玲~ ";

        DialogueUnit unit_4 = new DialogueUnit();
        unit_4.i_show_char_list = new List<int> { (int)character_order_enum.animal_whale, (int)character_order_enum.boy_satoim };
        unit_4.i_speaker = (int)character_order_enum.animal_whale;
        unit_4.content = "[" + _marked_word + "] 第三句"; // "接下來 讓機器人講講千字文!";

        DialogueUnit unit_2 = new DialogueUnit();
        unit_2.i_show_char_list = new List<int> { (int)character_order_enum.robert_red };
        unit_2.i_speaker = (int)character_order_enum.robert_red;
        unit_2.content = "[" + _marked_word + "] 第四句"; // "千字文此文分為八段的文（古時不押韻的文不稱文，只能算作「筆」，押韻的才算「文」，故千字文乃是一長文），它的第一段以二十七聯為文，全押「下平」（按廣韻一書，除「聖」此字於韻補內才找到，音成。），此文也分為四段解釋，由「天地玄黃」（二句四字文為一聯）至「鱗潛羽翔」共九聯，玄為黑色中帶紅，所以首句是指大地的產生，第三聯是指日出而作，日入而息，非是收藏食物。春秋時期才確定二十八宿（音秀），以此為本計算產生出古四分曆，所以亦因此計算法，定出了閏年，律召（此非呂，召者公報，調陽解作律例。）菜重芥薑是指「主糧與輔食品」。第十至第十六聯，以訴秦始王事跡，秦定出皇帝制度及三公九卿制度，癈除舜（帝舜姓虞）及堯（堯號陶唐氏）的讓賢為君制度，更想南、北統代稱霸一體。第十七至二十六聯，是述劉邦滅秦後，與功臣斬白馬立盟誓，史稱「白馬之誓」，改以文治武，形成「布衣卿相」政治格局，達至輝煌的時代，此二十六聯以「下平陽字韻及下平唐字韻」二韻，再以二十七下平清字韻為結。此文亦可說是告說皇知的治國之道也!!!";

        DialogueUnit unit_3 = new DialogueUnit();
        unit_3.i_show_char_list = new List<int> { (int)character_order_enum.animal_cat, (int)character_order_enum.girl_magic };
        unit_3.i_speaker = (int)character_order_enum.animal_cat;
        unit_3.content = "[" + _marked_word + "] 第五句"; // "聽完三國.. 貓咪想說英文.. Kris Sokolowski has always been active, spending his free time mountain climbing, running and practicing martial arts.And at every opportunity, he could be found boarding a plane, en route to explore the world.On his first official date with his now wife, Sokolowski booked flights to South Africa for two weeks. The couple have a son, now 11, who also joins them on their adventures.Sokolowski's outdoor pursuits have helped keep him healthy. At his last yearly physical checkup in December 2020, his doctor called him Iron Man.But around six months after that appointment, Sokolowski started experiencing what he describes as an odd feeling in his stomach.It was kind of like a gurgling, like you're hungry. And it just wasn't going away for a couple of days, he tells CNN Travel today.";

        DialogueUnit unit_5 = new DialogueUnit();
        unit_5.i_show_char_list = new List<int> { (int)character_order_enum.plant_mouse };
        unit_5.i_speaker = (int)character_order_enum.plant_mouse;
        unit_5.content = "[" + _marked_word + "] 第六句 結尾.."; //  "最後一個頁... 沒有了..";

        _list.Add(unit_0);
        _list.Add(unit_1);
        _list.Add(unit_4);
        _list.Add(unit_2);
        _list.Add(unit_3);
        _list.Add(unit_5);

        DialogueSets sets = new DialogueSets();
        //sets.pos = new Vector3(0,-124,471);
        //string[] _names = new string[] { "優姊0", "玲玲", "小彬", "魔術師", "機器人", "小鯨", "梨子", "綿羊", "大嘴花", "土撥鼠", "小貓" };
        sets.pos = new Vector3(-11.36f, 1.8f, -2.96f);
        string[] _names = new string[] { };
        for (int i = 0; i < _names.Length; i++)
        {
            sets.all_char_name_list.Add(_names[i]);
        }
        sets.unit_list = _list;
        return sets;
    }


    //public void debugCreateJson()
    //{
    //    MyFinder.Instance.getUIDebugCtrl().showDebugTxt("嘗試存入Local", 1);
    //    checkOrCreateSample("Sample_1.txt", "樣本1");

    //    MyFinder.Instance.getUIDebugCtrl().showDebugTxt("存入成功", 1);


    //}

    //public void debugReadJson()
    //{
    //    DialogueSets sets = loadLocal("Sample_1.txt");
    //    if (sets == null)
    //    {
    //        MyFinder.Instance.getUIDebugCtrl().showDebugTxt("讀取Local失敗",0);
    //    }
    //    else
    //    {
    //        MyFinder.Instance.getUIDebugCtrl().showDebugTxt("讀取Local成功", 0);
    //    }
    //}

    //void SaveCSVTest()
    //{
    //    List<string[]> rowData = new List<string[]>();

    //    // Creating First row of titles manually..
    //    string[] rowDataTemp = new string[2];
    //    rowDataTemp[0] = "Name";
    //    rowDataTemp[1] = "ID";
    //    //rowDataTemp[2] = "Income";
    //    rowData.Add(rowDataTemp);

    //    // You can add up the values in as many cells as you want.
    //    for (int i = 0; i < 10; i++)
    //    {
    //        rowDataTemp = new string[3];
    //        rowDataTemp[0] = "Sushanta" + i; // name
    //        rowDataTemp[1] = "" + i; // ID
    //        //rowDataTemp[2] = "$" + UnityEngine.Random.Range(5000, 10000); // Income
    //        rowData.Add(rowDataTemp);
    //    }

    //    string[][] output = new string[rowData.Count][];

    //    for (int i = 0; i < output.Length; i++)
    //    {
    //        output[i] = rowData[i];
    //    }

    //    int length = output.GetLength(0);
    //    string delimiter = ",";

    //    StringBuilder sb = new StringBuilder();

    //    for (int index = 0; index < length; index++)
    //        sb.AppendLine(string.Join(delimiter, output[index]));


    //    string filePath = Application.persistentDataPath + "/nori/a01.csv";

    //    StreamWriter outStream = System.IO.File.CreateText(filePath);
    //    outStream.WriteLine(sb);
    //    outStream.Close();
    //}


}

