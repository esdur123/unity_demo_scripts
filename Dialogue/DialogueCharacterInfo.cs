﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueCharacterInfo : MonoBehaviour
{
    public string name = "";
    //public float height = 0;
    public float talk_height = 0;
    public float talk_z = 0;
    public Vector3 talk_local_rot = Vector3.zero;
    public Vector3 init_local_pos = Vector3.zero;
    public Vector3 init_local_rot = Vector3.zero;
    
}
