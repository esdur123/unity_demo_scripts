﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;

[System.Serializable]
public class PrimaryButtonEvent : UnityEvent<bool> { }

public class PrimaryButtonWatcher : MonoBehaviour
{
    public PrimaryButtonEvent primaryButtonPress;

    private bool lastButtonState = false;
    private List<InputDevice> devicesWithPrimaryButton;

    public void myAwake()
    {
        if (primaryButtonPress == null)
        {
            primaryButtonPress = new PrimaryButtonEvent();
        }

        devicesWithPrimaryButton = new List<InputDevice>();
    }

    public void myOnEnable()
    {
        List<InputDevice> allDevices = new List<InputDevice>();
        InputDevices.GetDevices(allDevices);
        foreach (InputDevice device in allDevices)
            InputDevices_deviceConnected(device);

        InputDevices.deviceConnected += InputDevices_deviceConnected;
        InputDevices.deviceDisconnected += InputDevices_deviceDisconnected;
    }


    //bool m_is_vive = false;
    Main.buildModeEnum playmode;
    //bool m_is_hands_debug = false;
    List<UnityEngine.XR.InputDevice> m_inputDevices = null;
    ClickEffect m_ClickEffect = null;
    public void Init()
    {
        m_ClickEffect = MyFinder.Instance.getClickEffect();
        m_inputDevices = new List<UnityEngine.XR.InputDevice>();
        UnityEngine.XR.InputDevices.GetDevices(m_inputDevices);


        //foreach (var device in m_inputDevices)
        //{
        //    string str = string.Format("Device found with name '{0}' and role '{1}'", device.name, device.role.ToString());
        //    MyFinder.Instance.getUIDebugCtrl().showDebugTxt(str+"\n");
        //    //Debug.Log();
        //}
        playmode = MyFinder.Instance.getMain().p_playmode;
        //m_is_hands_debug = MyFinder.Instance.getMain().is_debug_hand;
        for (int i = 0; i < sf_rd_box.Length; i++)
        {
            if (playmode!= Main.buildModeEnum.vive)
            {
                sf_rd_box[i].enabled = false;
                sf_obj_sphere[i].SetActive(false);
            }
            else
            {
                sf_rd_box[i].enabled = true;
                sf_obj_sphere[i].SetActive(true);
            }
        }

    }

    public void myOnDisable()
    {
        InputDevices.deviceConnected -= InputDevices_deviceConnected;
        InputDevices.deviceDisconnected -= InputDevices_deviceDisconnected;
        devicesWithPrimaryButton.Clear();
    }

    private void InputDevices_deviceConnected(InputDevice device)
    {
        bool discardedValue;
        if (device.TryGetFeatureValue(CommonUsages.primaryButton, out discardedValue))
        {
            devicesWithPrimaryButton.Add(device); // 添加任何有主按鈕的設備
        }
    }

    private void InputDevices_deviceDisconnected(InputDevice device)
    {
        if (devicesWithPrimaryButton.Contains(device))
            devicesWithPrimaryButton.Remove(device);
    }

    public void myUpdate()
    {
        //if (m_is_vive) return;

        bool tempState = false;
        foreach (var device in devicesWithPrimaryButton)
        {
            bool primaryButtonState = false;
            tempState = device.TryGetFeatureValue(CommonUsages.primaryButton, out primaryButtonState) // 确实获得了值
                        && primaryButtonState // 我们获得的值
                        || tempState; // 来自其他控制器的累计结果
        }

        if (tempState != lastButtonState) // 自上一帧以来按钮状态已更改
        {
            primaryButtonPress.Invoke(tempState);
            lastButtonState = tempState;
        }

        // nori
        if (playmode== Main.buildModeEnum.vive)
        {
            XRTrackingUpdate();
        }
        
    }
    

    [Header("CubeRenderer")]
    [SerializeField] Renderer[] sf_rd_box;
    [SerializeField] GameObject[] sf_obj_sphere;
    bool m_triggerValue;
    int m_right = 0;
    int m_left = 0;
    Vector3 m_out_v3_r, m_out_v3_l;
    Quaternion m_out_q3_r, m_out_q3_l;
    bool cur_right, cur_left, last_right, last_left = false;

    enum myDeviceEnum // 自已推出來的XD
    {
        none,
        right_hand,
        left_hand,
        length
    }

    void XRTrackingUpdate()
    {        

        // 右手sf_obj_box[0] 位置
        if (m_inputDevices[(int)myDeviceEnum.right_hand].TryGetFeatureValue(UnityEngine.XR.CommonUsages.devicePosition, out m_out_v3_r))
        {
            sf_rd_box[0].transform.position = m_out_v3_r - sf_rd_box[0].transform.forward * 0.1f;
        }

        // 右手sf_obj_box[0] 轉向
        if (m_inputDevices[(int)myDeviceEnum.right_hand].TryGetFeatureValue(UnityEngine.XR.CommonUsages.deviceRotation, out m_out_q3_r))
        {
            sf_rd_box[0].transform.localRotation = m_out_q3_r;
        }

        // 左手sf_obj_box[1] 位置
        if (m_inputDevices[(int)myDeviceEnum.left_hand].TryGetFeatureValue(UnityEngine.XR.CommonUsages.devicePosition, out m_out_v3_l))
        {
            sf_rd_box[1].transform.position = m_out_v3_l - sf_rd_box[1].transform.forward * 0.1f;
        }

        // 左手sf_obj_box[1] 轉向
        if (m_inputDevices[(int)myDeviceEnum.left_hand].TryGetFeatureValue(UnityEngine.XR.CommonUsages.deviceRotation, out m_out_q3_l))
        {
            sf_rd_box[1].transform.localRotation = m_out_q3_l;
        }
        
        // 右手食指按鈕
        if (m_inputDevices[(int)myDeviceEnum.right_hand].TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out m_triggerValue) && m_triggerValue)
        {
            cur_right = true;
            if (!last_right)
            {
                m_right++;
                MyFinder.Instance.getUIDebugCtrl().showDebugTxt(myDeviceEnum.right_hand + ": " + m_right.ToString(), 2);
                onXRClickRight();
            }
        }
        else
        {
            cur_right = false;
        }
        last_right = cur_right;

        // 左手食指按鈕
        if (m_inputDevices[(int)myDeviceEnum.left_hand].TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out m_triggerValue) && m_triggerValue)
        {
            cur_left = true;
            if (!last_left)
            {
                m_left++;
                MyFinder.Instance.getUIDebugCtrl().showDebugTxt(myDeviceEnum.left_hand + ": " + m_left.ToString(), 2);
                onXRClickLeft();
            }
        }
        else
        {
            cur_left = false;
        }
        last_left = cur_left;
    }

    void onXRClickRight()
    {
        m_ClickEffect.playClickEffect(
                  m_out_v3_r
                , sf_rd_box[0].transform.forward
            );
    }

    void onXRClickLeft()
    {
        m_ClickEffect.playClickEffect(
                  m_out_v3_l
                , sf_rd_box[1].transform.forward
            );
    }

    const float c_fixed_fire_forward = 0.3f;
    public Vector3 getCilckHandPoint()
    {

        if (cur_left) // 左手
        {
            return sf_rd_box[1].transform.position + sf_rd_box[1].transform.forward * c_fixed_fire_forward;
        }
        else // 右手
        {
            return sf_rd_box[0].transform.position + sf_rd_box[0].transform.forward * c_fixed_fire_forward;
        }

    }
}