﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickPrefabUnit : MonoBehaviour
{
    public bool is_in_use = false;
    public int ser_num = 0;
    public float scale = 10;
    public ParticleSystem ps = null;

    //ClickEffect m_ClickEffect = null;
    public void OnParticleSystemStopped()
    {
        //if (m_ClickEffect == null)
        //{
        //    m_ClickEffect = MyFinder.Instance.getClickEffect();
        //}
        //Debug.Log("粒子停止");
        MyFinder.Instance.getClickEffect().RecyclePrefab(ser_num);
    }
}
