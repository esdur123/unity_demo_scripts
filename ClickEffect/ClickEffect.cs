﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickEffect : MonoBehaviour
{

    
    //[Header("[Camera]")]
    //[SerializeField] Camera sf_cam;
    //[Header("[Higher_Red_point]")]
    //[SerializeField] Transform sf_tr_higher_red_point;
    Transform m_tr_base_item;
    [Header("[Root]")]
    [SerializeField] Transform sf_tr_root;
    [Header("[Prefab]")]
    [SerializeField] GameObject sf_prefab;
    [SerializeField] Transform sf_tr_prefab_root;
    Main.buildModeEnum m_playmode;
    //bool is_lock_center_point = false;

    float c_show_dist = 20.0f;
    
    public void init()
    {

        m_playmode = MyFinder.Instance.getMain().p_playmode;
        //is_lock_center_point = MyFinder.Instance.getMain().is_lock_center_point;

        m_tr_base_item = MyFinder.Instance.getMySystem().sf_tr_camera;
        //if (m_playmode != Main.buildModeEnum.vive && !is_lock_center_point)
        if (m_playmode != Main.buildModeEnum.vive)
        {
            m_tr_base_item = MyFinder.Instance.getMySystem().sf_tr_higher_red_point;
        }


        InstantiatePrefab(runtime_add_prefab_count);
        

    }


    bool m_is_update = false;
    public void myUpdate()
    {
        if (!m_is_update) return;

        if (Input.GetMouseButtonDown(0) && m_playmode!=Main.buildModeEnum.vive) // Vive模式由 PrimaryButtonWatcher.cs的 onClickRight()和onClickLeft()啟動
        {
            playClickEffect(
                      m_tr_base_item.position
                    , m_tr_base_item.forward
                );
        }
    }

    public void playClickEffect(Vector3 _start_pos, Vector3 _dir)
    {
        Vector3 pos = _start_pos + _dir * c_show_dist; // 靠近攝影機的距離
        GameObject obj = getPrefab();
        obj.transform.SetParent(null);
        obj.transform.position = pos;
        obj.transform.SetParent(sf_tr_root);
        ClickPrefabUnit script = obj.GetComponent<ClickPrefabUnit>();
        obj.transform.localScale = Vector3.one * script.scale; // 大小
        obj.transform.localRotation = Quaternion.identity;
        script.ps.Clear();
        script.ps.Play();
    }


    public void setUpdate(bool _is_on)
    {
        m_is_update = _is_on;
    }

    /// <summary>
    /// 處理 Prefab
    /// </summary>
    // 生成prefab
    List<ClickPrefabUnit> list_prefab_script = new List<ClickPrefabUnit>();
    void InstantiatePrefab(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject obj = Instantiate(sf_prefab);
            obj.transform.SetParent(sf_tr_prefab_root);
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localScale = Vector3.one;
            ClickPrefabUnit script = obj.GetComponent<ClickPrefabUnit>();
            script.is_in_use = false;
            script.ser_num = list_prefab_script.Count;
            script.gameObject.name = list_prefab_script.Count + "號";

            // partical system
            ParticleSystem ps = obj.GetComponent<ParticleSystem>();            
            if (ps != null)
            {
                script.ps = ps;
                ps.Clear();
                //obj.SetActive(false);
            }
            list_prefab_script.Add(script);
        }
    }


    // 取得 prefab
    int runtime_add_prefab_count = 10;
    GameObject getPrefab()
    {
        for (int i = 0; i < list_prefab_script.Count; i++)
        {
            if (list_prefab_script[i].is_in_use) continue;
            list_prefab_script[i].is_in_use = true;
            return list_prefab_script[i].gameObject;
        }

        // if all in use
        int cur_count = list_prefab_script.Count;
        InstantiatePrefab(runtime_add_prefab_count);
        list_prefab_script[cur_count].is_in_use = true;
        return list_prefab_script[cur_count].gameObject;

    }


    //　回收 Prefab
    public void RecyclePrefab(int _index)
    {
        ClickPrefabUnit srcipt = list_prefab_script[_index];
        srcipt.gameObject.transform.SetParent(sf_tr_prefab_root);
        srcipt.gameObject.transform.localPosition = Vector3.zero;
        srcipt.gameObject.transform.localScale = Vector3.one;
        srcipt.ps.Clear();
        srcipt.is_in_use = false;
    }

}
