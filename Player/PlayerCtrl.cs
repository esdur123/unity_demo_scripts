using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCtrl : MonoBehaviour
{
    [Header("Slider")]
    [SerializeField] Slider sf_slider_portal;
    [Header("smoke")]
    [SerializeField] GameObject sf_obj_smoke;
    //[Header("btn回教室")]
    //[SerializeField] GameObject sf_obj_back_to_class;

    float c_max_hp = 1000;
    float m_cur_hp = 1000;
    float c_init_firerate = 1.5f;
    float m_cur_firerate = 1.5f;
    public void init()
    {
        m_cur_firerate = c_init_firerate;
        sf_obj_smoke.SetActive(false);
        sf_slider_portal.gameObject.SetActive(true);
        m_cur_hp = c_max_hp;
        setPorarlSlider();
        m_is_dead_or_end_game = false;
        //m_is_end_game = false;
    }

    public float getFireRate()
    {
        return m_cur_firerate;
    }

    public void clear()
    {
        sf_obj_smoke.SetActive(false);
    }


    void setPorarlSlider()
    {
        sf_slider_portal.value = m_cur_hp / c_max_hp;
        if (m_cur_hp == 0) sf_slider_portal.gameObject.SetActive(false);

        if (sf_slider_portal.value < 0.3f) sf_obj_smoke.SetActive(true);
        if (sf_slider_portal.value >= 0.3f) sf_obj_smoke.SetActive(false);
    }


    // +66%血
    public void skillHeal()
    {
        float add_hp = c_max_hp * 0.66f;
        m_cur_hp += add_hp;
        if (m_cur_hp > c_max_hp) m_cur_hp = c_max_hp;
        setPorarlSlider();
    }


    public float getBulletDmg(TowerGameEnemyCtrl.EnemyTypeEnum _type_enemy, FxCtrl.enumTypeBullet _type_bullet)
    {
        //Debug.LogError("getBulletDmg: "+ _type_enemy+ ", _type_bullet="+ _type_bullet);
        float dmg = 80;
        switch (_type_bullet)
        {
            case FxCtrl.enumTypeBullet.greenProject01:
                break;
            case FxCtrl.enumTypeBullet.blueProject01:
                break;
            case FxCtrl.enumTypeBullet.redProject01:
                dmg = 80;
                break;
            case FxCtrl.enumTypeBullet.dark_blue_ball:
                dmg = 170;
                break;
            case FxCtrl.enumTypeBullet.length:
                break;
            default:
                break;
        }
        return dmg;
    }

    public float getSkillDmg(SkillUnit.type _type_skill)
    {
        //Debug.LogError("getSkillDmg");
        float dmg = 10;
        switch (_type_skill)
        {
            case SkillUnit.type.empty:
                break;
            case SkillUnit.type.skill_fireball:
                dmg = 100;
                break;
            case SkillUnit.type.skill_heal:
                break;
            case SkillUnit.type.length:
                break;
            default:
                break;
        }
        return dmg;
    }

    public bool isDeadOREndGame()
    {
        return m_is_dead_or_end_game;
    }


    //bool m_is_end_game = false;
    public void setEndGame()
    {
       
        //if (m_is_end_game) return;
        //m_is_end_game = true;

        m_is_dead_or_end_game = true;
        //StartCoroutine(_setEndGame());
    }




    bool m_is_dead_or_end_game = false;
    public void receiveDamage(TowerGameEnemyCtrl.EnemyTypeEnum _enemy_type)
    {
        getCoverCtrl().playBloody();



        float dmg = getDamage(_enemy_type);


        m_cur_hp -= dmg;
        if (m_cur_hp < 0) m_cur_hp = 0;
        setPorarlSlider();

        if (m_cur_hp == 0)
        {
            m_is_dead_or_end_game = true;
            playDead();
            //Debug.LogError("你已經死了..");
        }
    }

    void playDead()
    {
        //sf_obj_smoke.SetActive(true);
        getTowerGameCtrl().showScoreEndGame();
    }

    float getDamage(TowerGameEnemyCtrl.EnemyTypeEnum _enemy_type)
    {
        float dmg = 0;
        switch (_enemy_type)
        {
            case TowerGameEnemyCtrl.EnemyTypeEnum.footman:
                dmg = 50;
                break;
        }

        return dmg;
    }

    CoverCtrl m_CoverCtrl = null;
    CoverCtrl getCoverCtrl()
    {
        if (m_CoverCtrl == null) m_CoverCtrl = MyFinder.Instance.getCoverCtrl();
        return m_CoverCtrl;
    }

    TowerGameCtrl m_TowerGameCtrl = null;
    TowerGameCtrl getTowerGameCtrl()
    {
        if (m_TowerGameCtrl == null) m_TowerGameCtrl = MyFinder.Instance.getTowerGameCtrl();
        return m_TowerGameCtrl;
    }





}
