﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirCtrl : MonoBehaviour
{

    [Header("[Mat]")]
    [SerializeField] Material sf_mt_neon_arrow;

    [Header("[左箭頭]")]
    [SerializeField] GameObject sf_obj_left_arrow;

    [Header("[右箭頭]")]
    [SerializeField] GameObject sf_obj_right_arrow;

    [Header("[攝影機]")]
    [SerializeField] Transform sf_tr_camera;

    [Header("[主要目標]")]
    [SerializeField] Transform sf_tr_dir_target;

    Vector4 m_mat_color = new Vector4();
    float c_color = 0.528f;
    float c_max_alpha = 0.5f;
    float c_min_alpha = 0.11f;
    int c_delay_times = 10;
    int m_delay = 0;
    float m_cur_alpha = 0;
    float m_lerp_speed = 0.25f;
    bool m_is_add_alpha = false;
    public bool m_is_checking = false;
    public enum DirArrow
    {
        left,
        right,
        length
    }


    public void init()
    {
        m_delay = 0;
        m_is_add_alpha = false;
        setMatAlpha(c_max_alpha);
        callDirArrow(DirArrow.left, false);
        callDirArrow(DirArrow.right, false);
    }

    public void closeDir()
    {
        callDirArrow(DirArrow.left, false);
        callDirArrow(DirArrow.right, false);
        sf_tr_dir_target = null;
    }

    [Header("dot")]
    public float m_dot = 0;
    [Header("cross")]
    public float m_cross = 0;
    public float m_type_dot_cur = 35; // 每個環境 目標不同的 ***距離*** 會影響這個角度
    const float c_type_dot_360 = 35;
    float c_type_dot_tower_game = 1f;
    float c_type_dot_en_game_dialogue = 1.3f;
    //float c_type_dot_en_game_play = 1.2f;
    public void setCheckingBool(bool _is_checking, GameFlowUnit.GemeFlowTypeEnum _type)
    {
        
        m_is_checking = _is_checking;
        switch (_type)
        {
            case GameFlowUnit.GemeFlowTypeEnum.none:
                break;
            case GameFlowUnit.GemeFlowTypeEnum.init:
                break;
            case GameFlowUnit.GemeFlowTypeEnum.dialogue:
                m_type_dot_cur = c_type_dot_360;
                break;
            case GameFlowUnit.GemeFlowTypeEnum.engame_dialogue:
                m_type_dot_cur = c_type_dot_en_game_dialogue;                
                break;
            case GameFlowUnit.GemeFlowTypeEnum.portal:
                break;
            case GameFlowUnit.GemeFlowTypeEnum.engame_play:
                //m_type_dot_cur = c_type_dot_en_game_play;
                break;                
            case GameFlowUnit.GemeFlowTypeEnum.tower_game:
                m_type_dot_cur = c_type_dot_tower_game;
                //Debug.LogError(_type+ ", c_type_dot_tower_game=" + c_type_dot_tower_game);
                break;
            case GameFlowUnit.GemeFlowTypeEnum.length:
                break;
            default:
                m_type_dot_cur = c_type_dot_360;
                break;
        }
    }


    public void forceCheckingBool(bool _is_checking, float _dot)
    {
        m_is_checking = _is_checking;
        m_type_dot_cur = _dot;

    }

    public void setTarget(Transform _target)
    {
        sf_tr_dir_target = _target;
    }

    bool m_is_left_on = false;
    bool m_is_right_on = false;
    bool m_is_blinking_mat = false;
    public void callDirArrow(DirArrow _enum, bool _is_on)
    {
        GameObject obj_arrow = sf_obj_left_arrow;
        if (_enum== DirArrow.right)
        {
            obj_arrow = sf_obj_right_arrow;
        }

        if (_enum == DirArrow.left) m_is_left_on = _is_on;
        if (_enum == DirArrow.right) m_is_right_on = _is_on;

        if (_is_on)
        {
            obj_arrow.SetActive(true);

        }
        else
        {
            obj_arrow.SetActive(false);
        }

        if (m_is_left_on || m_is_right_on)
        {
            m_is_blinking_mat = true;
        }
        else
        {
            m_is_blinking_mat = false;
        }
    }


    public void myUpdate()
    {
        if (!m_is_checking) return;

        checkingDir();

        if (!m_is_blinking_mat) return;

        if (!m_is_add_alpha)
        {
            m_cur_alpha -= (m_lerp_speed * Time.deltaTime);
            if (m_cur_alpha <= c_min_alpha)
            {
                m_cur_alpha = c_min_alpha;

                m_delay++;
                if (m_delay >= c_delay_times)
                {
                    m_delay = 0;
                    m_is_add_alpha = true;
                }
            }
        }
        else
        {
            m_cur_alpha += (m_lerp_speed * Time.deltaTime);
            if (m_cur_alpha >= c_max_alpha)
            {
                m_cur_alpha = c_max_alpha;
                m_is_add_alpha = false;
            }
        }

        setMatAlpha(m_cur_alpha);
    }


    void setMatAlpha(float _alpha)
    {
        m_mat_color = new Vector4(
                  c_color
                , c_color
                , c_color
                , _alpha
            );

        sf_mt_neon_arrow.SetColor("_TintColor", m_mat_color);
    }


    public bool is_front = false;
    void checkingDir()
    {
        if (sf_tr_dir_target == null)
        {
            //Debug.LogError("sf_tr_dir_target==null");
            return;
        }

        m_dot = Vector3.Dot(
                    sf_tr_camera.forward
                  , sf_tr_dir_target.position - sf_tr_camera.position
            );

        m_cross = Vector3.Cross(sf_tr_camera.forward, sf_tr_dir_target.position - sf_tr_camera.position).y;

        //if (m_dot < c_type_dot_360) // 正面?度之外
        if (m_dot < m_type_dot_cur) // 正面?度之外
        {
            is_front = false;
            if (m_cross > 0)
            {
                callDirArrow(DirArrow.right, true);
                callDirArrow(DirArrow.left, false);
            }
            else
            {
                callDirArrow(DirArrow.left, true);
                callDirArrow(DirArrow.right, false);
            }
        }
        else
        {
            is_front = true;
            callDirArrow(DirArrow.left, false);
            callDirArrow(DirArrow.right, false);
        }

        /*
            在做rpg类游戏的过程中,经常遇到要判断周围怪物相对自身的方位

            1.判断目标在自己的前后方位可以使用下面的方法:
              Vector3.Dot(transform.forward, target.position)
              返回值为正时,目标在自己的前方,反之在自己的后方

            2.判断目标在机子的左右方位可以使用下面的方法:
              Vector3.Cross(transform.forward, target.position).y
              返回值为正时,目标在自己的右方,反之在自己的左方
              https://blog.csdn.net/cen616899547/article/details/38336185         
         */
    }
}
