using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCtrl : MonoBehaviour
{
    [SerializeField] Main sf_main;
    // Start is called before the first frame update
    [Header("Item")]
    [SerializeField] GameObject sf_obj_test;
    //[Header("XR Input")]
    //[SerializeField] Wave.XR.Sample.WaveXR_Input xr_input;
    void Start()
    {
        //if(!sf_main.is_debug) return;
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (!xr_input.gameObject.activeSelf) return;
        //MyFinder.Instance.getUIDebugCtrl().showDebugTxt("" + xr_input.mousePosition);
        

    }

    public void testUpDown(bool is_up)
    {
        float delta = 3;
        if (is_up)
        {
            sf_obj_test.transform.localPosition += new Vector3(0,0,1) * delta;            
        }
        else
        {
            sf_obj_test.transform.localPosition += new Vector3(0, 0, -1) * delta;
        }

        MyFinder.Instance.getUIDebugCtrl().showDebugTxt("Enemy_Z = " + sf_obj_test.transform.localPosition.z.ToString());
    }
}
