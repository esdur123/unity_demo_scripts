﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// AssetStoer買來就有的檔案,
/// 改造成沒有update, destroy拿掉, 
/// </summary>
/// 
public class FxMover : MonoBehaviour
{
    /*
    public enum enumTypeFx
    {
        bullet = 0,
        hit = 1,
        flash = 2,
        TEST = 999
    }


    public enum enumTypeBullet
    {
        greenProject01 = 0,
        blueProject01 = 1,
        redProject01 = 2,
        TEST = 999
    }     
         */
    // fx type
    public FxCtrl.enumTypeFx fx_type = FxCtrl.enumTypeFx.length;
    public FxCtrl.enumTypeBullet type_bullet = FxCtrl.enumTypeBullet.length;
    float damage = 0;
    float m_max_lifetime = 5;
    float m_speed = 0;
    int m_iType = 0;
    int p_num = 0;
    int m_indexFlash = -1;
    int m_indexHit = -1;
    int m_index = -1;
    Vector3 m_targetPos = new Vector3();
    Vector3 m_startPos = new Vector3();
    FxCtrl m_FxCtrl = null;
    MyTools m_MyTools = null;
    [SerializeField] Rigidbody rb;


    public void getCtrls()
    {
        m_FxCtrl = MyFinder.Instance.getFxCtrl();
        m_MyTools = MyFinder.Instance.getMyTools();
    }

    //public void setDamage(float _dmg)
    //{
    //    damage = _dmg;
    //}



    public void setIndex(int index)
    {
        m_index = index;
    }


    public void setFlash(int i, Vector3 startPos)
    {
        m_indexFlash = i;
        m_startPos = startPos;
    }


    public void setHit(int i)
    {
        m_indexHit = i;
    }


    public void setITypeNum(int type, int num)
    {
        m_iType = type;
        p_num = num;
    }


    public void setTargetPos(Vector3 pos)
    {
        m_targetPos = pos;
    }


    public int getType()
    {
        return m_iType;
    }


    public int getNum()
    {
        return p_num;
    }


    public void setSpeed(float speed)
    {
        m_speed = speed;
    }


    public void setSpeedToForward()
    {
        rb.velocity = transform.forward * m_speed;

    }


    GameObject m_tempFlash = null;
    public void showFlash()
    {
        if (m_indexFlash == -1)
        {
            //Debug.LogError(m_indexFlash);
            //Debug.LogError("m_indexFlash = -1");
            return;
        }
        //Debug.LogError(m_indexFlash);
        m_tempFlash = m_FxCtrl.getFxObjListGameobject(m_indexFlash);
        m_tempFlash.transform.position = m_startPos;
        m_MyTools.RotateGameobjectToTraget(
                  m_tempFlash
                , m_targetPos
                , true
            );
        m_tempFlash.transform.forward = gameObject.transform.forward;
        m_tempFlash.SetActive(true);
        //var flashInstance = Instantiate(flash, transform.position, Quaternion.identity);
    }


    GameObject m_tempHit = null;
    public void showHit()
    {
        if (m_indexHit == -1)
        {
            //Debug.LogError("m_indexHit = -1");
            return;
        }

        //Debug.LogError(m_indexHit);
        m_tempHit = m_FxCtrl.getFxObjListGameobject(m_indexHit);
        m_tempHit.transform.position = transform.position;
        m_MyTools.RotateGameobjectToTraget(
                  m_tempHit
                , transform.position
                , true
            );
        m_tempHit.SetActive(true);
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "enemy") return;
        showHit();
        recycle();
    }


    //https ://docs.unity3d.com/ScriptReference/Rigidbody.OnCollisionEnter.html
    void OnCollisionEnter(Collision collision)
    {
        // 現在子彈改用Trigger
        return;
        if (collision.gameObject.tag != "enemy") return;
        showHit();
        recycle();
    }


    public void startInvoke()
    {
        Invoke("recycle", m_max_lifetime);
    }

    void recycle()
    {
        CancelInvoke("recycle");
        m_FxCtrl.recycleFxObj(m_index);
    }

    private void OnDisable()
    {
        CancelInvoke("recycle");
    }
}
