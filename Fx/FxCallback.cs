using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxCallback : MonoBehaviour
{
    int mIndex = -1;

    public void setIndex(int index)
    {
        mIndex = index;
    }

    public void OnParticleSystemStopped()
    {
        //Debug.Log("*******�ɤl����:mIndex�@= "+ mIndex+", "+this.gameObject.name);
        MyFinder.Instance.getFxCtrl().recycleFxObj(mIndex);
    }
}
