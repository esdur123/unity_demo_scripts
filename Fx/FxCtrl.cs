using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fxObj // fx通用
{
    //public bool isBullet = true; // fx 或 bullet
    public bool isInUse = false;
    public int type = -1; // bullet/hit/flash
    public int num = -1;  // j, 此種fx中的第幾個
    public FxMover scriptProjectileMover;
    public FxCallback scriptCallback;
    public GameObject gobj = null;
    public int index = 0; // 此fx在總陣列中的index
    public int iType = -1; // i, enumTypeOOO的int值    
    public int iTypeHit = -1;    // 此fx對應的擊中特效, 在總陣列m_fxObjList中的index
    public int iTypeFlash = -1;  // 此fx對應的發射特效, 在總陣列m_fxObjList中的index    
    public int iIndexBullet = -1; // 此fx對應的子彈Fx是第index幾個
    public int iIndexFlash = -1;  // 此fx對應的flash Fx是第index幾個
    public int iIndexHit = -1;    // 此fx對應的hit Fx是第index幾個
}


public class FxCtrl : MonoBehaviour
{
    int C_MAX_SINGLE_FX = 0;
    List<fxObj> m_fxObjList = new List<fxObj>();
    int m_indexFxObjList = 0;

    [SerializeField] Transform tf_ObjectPool_bullet;
    [SerializeField] Transform tf_ObjectPool_hit;
    [SerializeField] Transform tf_ObjectPool_flash;
    [SerializeField] GameObject[] sf_fx_bullet; // 順序請對應  enumTypeBullet
    [SerializeField] GameObject[] sf_fx_hit;    // 順序請對應  enumTypeHit
    [SerializeField] GameObject[] sf_fx_flash;  // 順序請對應  enumTypeFlash

    [Header("測試用")]
    [SerializeField] GameObject sf_obj_firepoint;
    [SerializeField] GameObject sf_obj_target;

    //PlayerCtrl m_PlayerCtrl = null;
    AudioCtrl m_AudioCtrl = null;

    public enum enumTypeFx
    {
        bullet = 0,
        hit = 1,
        flash = 2,
        length
    }


    public enum enumTypeBullet
    {
        greenProject01 = 0,
        blueProject01 = 1,
        redProject01 = 2,
        special_ball = 3,
        pink_ball = 4,
        dark_blue_ball = 5,
        length
    }


    public enum enumTypeHit
    {
        greenProject01 = 0,
        blueProject01 = 1,
        redProject01 = 2,
        electricity = 3,
        TEST = 999
    }


    public enum enumTypeFlash
    {
        greenProject01 = 0,
        blueProject01 = 1,
        redProject01 = 2,
        TEST = 999
    }


    MyTools m_MyTools = null;
    public void init()
    {
        m_AudioCtrl = MyFinder.Instance.getAudioCtrl();
        m_MyTools = MyFinder.Instance.getMyTools();
        //m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        C_MAX_SINGLE_FX = MyDefined.OBJECT_POOL_MAX_SINGLE_FX; // 單個fx最大數量
        initFxBulletArray();
        initHitArray();
        initFlashArray();
        //Debug.Log("init BattleFxService");
    }


    /// 取得m_fxObjList的第index個GameObject
    public GameObject getFxObjListGameobject(int index)
    {
        return m_fxObjList[index].gobj;
    }


    /// 回收fxObj
    Vector3 temp_zeroV = Vector3.zero;
    public void recycleFxObj(int index)
    {
        //Debug.Log("index="+ index);
        if (!m_fxObjList[index].isInUse) return;


        //Debug.Log("### 回收 ###:index　= " + index);
        m_fxObjList[index].gobj.SetActive(false);
        m_fxObjList[index].gobj.transform.localPosition = Vector3.zero;

        //if (m_fxObjList[index].scriptProjectileMover != null)
        //{
        //    //m_fxObjList[index].scriptProjectileMover.setSpeed(0);
        //}


        switch (m_fxObjList[index].type)
        {
            case (int)enumTypeFx.bullet:
                break;
            case (int)enumTypeFx.flash:
                break;
            case (int)enumTypeFx.hit:
                break;

            default:
                break;
        }

        if (true)
        {
            m_fxObjList[index].isInUse = false;
        }

    }


    /// 建立FxHitArray
    fxObj temp_flashA = null;
    //int m_indexFlash = 0;
    void initFlashArray()
    {
        //Debug.Log("C_MAX_SINGLE_FX=" + C_MAX_SINGLE_FX);
        // fx有時會對應hit和flash, 規則還沒定
        for (int i = 0; i < sf_fx_flash.Length; i++)
        {
            if (sf_fx_flash[i] == null)
            {
                Debug.Log("**Error**: sf_fx_flash[" + i + "] = null");
                continue;
            }

            //if (sf_fx_flash[i] == null) continue;
            for (int j = 0; j < C_MAX_SINGLE_FX; j++)
            {
                temp_flashA = initFxFlashUnit(i, j, m_indexFxObjList);
                if (temp_flashA != null)
                {
                    m_fxObjList.Add(temp_flashA);
                    m_indexFxObjList++;
                }
            }
        }
    }


    /// 根據 sf_fx_flash 的array順序去決定這個fx(or flash)的係數
    fxObj temp_flashB = null;
    fxObj initFxFlashUnit(int i, int j, int index) // num從0開始
    {
        GameObject num = sf_fx_flash[i];
        if (num == null)
        {
            return null;
        }

        temp_flashB = new fxObj();
        temp_flashB.gobj = Instantiate(num, tf_ObjectPool_flash.position, tf_ObjectPool_flash.rotation);
        temp_flashB.gobj.transform.SetParent(tf_ObjectPool_flash);
        temp_flashB.gobj.transform.localPosition = Vector3.zero;

        temp_flashB = _initFxUnitGeneral(
              (int)enumTypeFx.flash
            , temp_flashB
            , i
            , j
            , index
        );

        temp_flashB.gobj.SetActive(false);
        return temp_flashB;
    }


    /// 取得一個可用的flash index
    public int getFlashIndex(int iType)
    {
        for (int i = 0; i < m_fxObjList.Count; i++)
        {
            if (m_fxObjList[i].type != (int)enumTypeFx.flash) continue;
            if (iType != m_fxObjList[i].iType) continue;
            if (m_fxObjList[i].isInUse) continue;

            m_fxObjList[i].isInUse = true;
            return i;
        }

        return -1;
    }


    /// 取得一個可用的hit index
    public int getHitIndex(int iType)
    {
        for (int i = 0; i < m_fxObjList.Count; i++)
        {
            if (m_fxObjList[i].type != (int)enumTypeFx.hit) continue;
            if (iType != m_fxObjList[i].iType) continue;
            if (m_fxObjList[i].isInUse) continue;

            m_fxObjList[i].isInUse = true;
            return i;
        }

        return -1;
    }


    /// 建立FxHitArray
    fxObj temp_hitA = null;
    //int m_indexHit = 0;
    void initHitArray()
    {
        //Debug.Log("C_MAX_SINGLE_FX=" + C_MAX_SINGLE_FX);
        // fx有時會對應hit和flash, 規則還沒定
        for (int i = 0; i < sf_fx_hit.Length; i++)
        {
            if (sf_fx_hit[i] == null)
            {
                Debug.Log("**Error**: sf_fx_hit[" + i + "] = null");
                continue;
            }

            //if (sf_fx_hit[i] == null) continue;
            for (int j = 0; j < C_MAX_SINGLE_FX; j++)
            {
                temp_hitA = initFxHitUnit(i, j, m_indexFxObjList);
                if (temp_hitA != null)
                {
                    m_fxObjList.Add(temp_hitA);
                    m_indexFxObjList++;
                }
            }
        }
    }


    /// 根據 sf_fx_bullet 的array順序去決定這個fx(or bullet)的係數
    fxObj temp_hitB = null;
    fxObj initFxHitUnit(int i, int j, int index) // num從0開始
    {
        GameObject num = sf_fx_hit[i];
        if (num == null)
        {
            return null;
        }

        temp_hitB = new fxObj();
        temp_hitB.gobj = Instantiate(num, tf_ObjectPool_hit.position, tf_ObjectPool_hit.rotation);
        temp_hitB.gobj.transform.SetParent(tf_ObjectPool_hit);
        temp_hitB.gobj.transform.localPosition = Vector3.zero;

        temp_hitB = _initFxUnitGeneral(
              (int)enumTypeFx.hit
            , temp_hitB
            , i
            , j
            , index
        );

        temp_hitB.gobj.SetActive(false);
        return temp_hitB;
    }


    /// 建立FxBulletArray
    fxObj temp_fxA = null;
    void initFxBulletArray()
    {
        //Debug.Log("C_MAX_SINGLE_FX="+ C_MAX_SINGLE_FX);
        // fx有時會對應hit和flash, 規則還沒定
        for (int i = 0; i < sf_fx_bullet.Length; i++)
        {
            if (sf_fx_bullet[i] == null)
            {
                Debug.Log("**Error**: sf_fx_bullet[" + i + "] = null");
                continue;
            }

            //if (sf_fx_bullet[i] == null) continue;
            for (int j = 0; j < C_MAX_SINGLE_FX; j++)
            {
                temp_fxA = initFxBulletUnit(i, j, m_indexFxObjList);
                if (temp_fxA != null)
                {
                    m_fxObjList.Add(temp_fxA);
                    m_indexFxObjList++;
                }
            }
        }
    }



    /// 根據 sf_fx_bullet 的array順序去決定這個fx(or bullet)的係數
    fxObj temp_fxB = null;
    fxObj initFxBulletUnit(int i, int j, int index) // num從0開始
    {
        GameObject num = sf_fx_bullet[i];
        if (num == null)
        {
            return null;
        }

        temp_fxB = new fxObj();
        temp_fxB.gobj = Instantiate(num, tf_ObjectPool_bullet.position, tf_ObjectPool_bullet.rotation);
        temp_fxB.gobj.transform.SetParent(tf_ObjectPool_bullet);
        temp_fxB.gobj.transform.localPosition = Vector3.zero;

        temp_fxB = _initFxUnitGeneral(
                  (int)enumTypeFx.bullet
                , temp_fxB
                , i
                , j
                , index
            );

        temp_fxB.scriptProjectileMover = temp_fxB.gobj.GetComponent<FxMover>();
        if (temp_fxB.scriptProjectileMover != null)
        {
            temp_fxB.scriptProjectileMover.setITypeNum(i, j);
            temp_fxB.scriptProjectileMover.setIndex(index);
        }

        temp_fxB.iTypeHit = -1; // 對應的hit預設(無)
        temp_fxB.iTypeFlash = -1; // 對應的flash預設(無)

        switch (i) // 第i種fx對應的 hit和flash和speed各是哪種      
        {
            case (int)enumTypeBullet.greenProject01:
                temp_fxB.iTypeHit = (int)enumTypeHit.greenProject01;
                temp_fxB.iTypeFlash = (int)enumTypeFlash.greenProject01;
                temp_fxB.scriptProjectileMover.setSpeed(MyDefined.FX_BULLET_SPEED_0);
                break;
            case (int)enumTypeBullet.blueProject01:
                temp_fxB.iTypeHit = (int)enumTypeHit.blueProject01;
                temp_fxB.iTypeFlash = (int)enumTypeFlash.blueProject01;
                temp_fxB.scriptProjectileMover.setSpeed(MyDefined.FX_BULLET_SPEED_ICE);
                break;
            case (int)enumTypeBullet.redProject01:
                temp_fxB.iTypeHit = (int)enumTypeHit.redProject01;
                temp_fxB.iTypeFlash = (int)enumTypeFlash.redProject01;
                temp_fxB.scriptProjectileMover.setSpeed(MyDefined.FX_BULLET_SPEED_0);
                break;
            case (int)enumTypeBullet.special_ball:
                temp_fxB.iTypeHit = (int)enumTypeHit.electricity;
                temp_fxB.iTypeFlash = (int)enumTypeFlash.blueProject01;
                temp_fxB.scriptProjectileMover.setSpeed(MyDefined.FX_BULLET_SPEED_0);
                break;
            case (int)enumTypeBullet.pink_ball:
                temp_fxB.iTypeHit = (int)enumTypeHit.electricity;
                temp_fxB.iTypeFlash = (int)enumTypeFlash.blueProject01;
                temp_fxB.scriptProjectileMover.setSpeed(MyDefined.FX_BULLET_SPEED_1);
                break;
            case (int)enumTypeBullet.dark_blue_ball:
                temp_fxB.iTypeHit = (int)enumTypeHit.electricity;
                temp_fxB.iTypeFlash = (int)enumTypeFlash.blueProject01;
                temp_fxB.scriptProjectileMover.setSpeed(MyDefined.FX_BULLET_SPEED_0);
                break;

                
            default:
                break;
        }

        temp_fxB.gobj.SetActive(false);
        return temp_fxB;
    }


    /// 初始化fxUnit的通用函式
    fxObj _initFxUnitGeneral(int type, fxObj tempFx, int i, int j, int index)
    {
        tempFx.isInUse = false;
        tempFx.type = type;
        tempFx.iType = i;
        tempFx.num = j;
        tempFx.index = index;
        tempFx.gobj.name = tempFx.type + "_i" + i + "_j" + j + "_index" + index;
        tempFx.scriptCallback = tempFx.gobj.GetComponent<FxCallback>();
        if (tempFx.scriptCallback != null)
        {
            tempFx.scriptCallback.setIndex(index);
        }
        return tempFx;
    }


  


    ///  發射子彈
    int temp_iFlash = -1;
    int temp_iHit = -1;
    float fireRate = 100; // 改由playerCtrl的getPlayerCtrl()取得
    private float nextFire = 0;
    float m_cur_time = 0;
    public void fireBullet(enumTypeBullet _type_bullet, Vector3 startPos, Vector3 targetPos, bool isLockY, bool isLockDelay)
    {
        
        //MyFinder.Instance.getUIDebugCtrl().showDebugTxt("fireBullet 0");
        fireRate = getPlayerCtrl().getFireRate();

        if (isLockDelay)
        {
            //MyFinder.Instance.getUIDebugCtrl().showDebugTxt("fireBullet 1");
            if (m_cur_time < nextFire) return;
            nextFire = m_cur_time + fireRate;
        }

        m_AudioCtrl.playClip(AudioCtrl.AudioClipsEnum.Nitro_Speed);

        //MyFinder.Instance.getUIDebugCtrl().showDebugTxt("fireBullet 2");
        //float dmg = m_PlayerCtrl.getDamage(iType);

        for (int i = 0; i < m_fxObjList.Count; i++)
        {
            if (m_fxObjList[i].type != (int)enumTypeFx.bullet) continue;
            if ((int)_type_bullet != m_fxObjList[i].iType) continue;
            if (m_fxObjList[i].isInUse) continue;

            m_fxObjList[i].isInUse = true;
            m_fxObjList[i].gobj.transform.position = startPos;

            m_MyTools.RotateGameobjectToTraget(
                      m_fxObjList[i].gobj
                    , targetPos
                    , isLockY
                );
            m_fxObjList[i].scriptProjectileMover.getCtrls();
            m_fxObjList[i].scriptProjectileMover.setSpeedToForward(); // 這邊改變velocity方向 here problem
            temp_iFlash = getFlashIndex(m_fxObjList[i].iTypeFlash); // 這邊寫錯
            m_fxObjList[i].scriptProjectileMover.setFlash(temp_iFlash, startPos);
            m_fxObjList[i].scriptProjectileMover.setTargetPos(targetPos);
            temp_iHit = getHitIndex(m_fxObjList[i].iTypeHit); // 這邊寫錯
            m_fxObjList[i].scriptProjectileMover.setHit(temp_iHit);
            m_fxObjList[i].scriptProjectileMover.showFlash();
            m_fxObjList[i].scriptProjectileMover.startInvoke();
            m_fxObjList[i].scriptProjectileMover.type_bullet = _type_bullet;
            //m_fxObjList[i].scriptProjectileMover.setDamage(dmg); // 改擊中的時候給傷害
            m_fxObjList[i].gobj.SetActive(true);
            break;
        }

        //MyFinder.Instance.getUIDebugCtrl().showDebugTxt("fireBullet 3");
        temp_iFlash = -1;
        temp_iHit = -1;
    }



    public void myUpdate()
    {
        
        m_cur_time = Time.time;
    }


    PlayerCtrl m_PlayerCtrl = null;
    PlayerCtrl getPlayerCtrl()
    {
        if (m_PlayerCtrl == null) m_PlayerCtrl = MyFinder.Instance.getPlayerCtrl();
        return m_PlayerCtrl;
    }
}

