﻿using UnityEngine;

namespace SelectedEffectOutline
{
    public class NoriSelectEffectOutline : MonoBehaviour//, IDebugOnTrigger
    {
        public enum ETriggerMethod { MouseMove = 0, MouseRightPress, MouseLeftPress };
        public enum EDrawMethod { OutlineInvisible = 0, OutlineAll, OutlineVisible, OutlineAndFill };
        [Header("Trigger")]
        public ETriggerMethod m_TriggerMethod = ETriggerMethod.MouseMove;
        public EDrawMethod m_DrawMethod = EDrawMethod.OutlineAll;
        public bool m_Persistent = false;
        [Header("Outline")]
        public Color m_OutlineColor = Color.green;
        [Range(1f, 10f)] public float m_OutlineWidth = 2f;
        [Range(0f, 1f)] public float m_OutlineFactor = 1f;
        public bool m_BasedOnVertexColorR = false;
        [Header("Shader")]
        public Shader m_SdrOutline;
        Shader m_SdrOriginal = null;
        [Header("Randerer")]
        public Renderer m_Rd = null;
        bool m_IsMouseOn = false;


        //private void Start()
        //{
        //    init();
        //}

        //public void init()
        //{
        //    //m_Rd = GetComponent<Renderer>();
        //    m_SdrOriginal = m_Rd.material.shader;
        //}


        public void onEnter()
        {
            m_IsMouseOn = true;
            if (m_TriggerMethod != ETriggerMethod.MouseMove) return;

            if (m_SdrOriginal == null)
            {
                m_SdrOriginal = m_Rd.material.shader;
            }
            
            // Renderer 換成特效Shader
            Material[] mats = m_Rd.materials;
            for (int i = 0; i < mats.Length; i++)
            {
                mats[i].shader = m_SdrOutline;
            }

            // Shader 參數 外框大小
            for (int i = 0; i < mats.Length; i++)
            {
                mats[i].SetFloat("_OutlineWidth", m_OutlineWidth);
                mats[i].SetColor("_OutlineColor", m_OutlineColor);
                mats[i].SetFloat("_OutlineFactor", m_OutlineFactor);
                mats[i].SetFloat("_OutlineBasedVertexColorR", m_BasedOnVertexColorR ? 0f : 1f);
                mats[i].SetInt("_ZTest2ndPass", (int)UnityEngine.Rendering.CompareFunction.Always);
                mats[i].SetInt("_ZTest3rdPass", (int)UnityEngine.Rendering.CompareFunction.Always);
            }
        }


        public void onExit()
        {
            m_IsMouseOn = false;

            // Renderer 換回原本Shader
            if (m_Persistent) return;

            Material[] mats = m_Rd.materials;
            for (int i = 0; i < mats.Length; i++)
            {
                mats[i].shader = m_SdrOriginal;
            }
        }


        //void IDebugOnTrigger.OnDebugClick()
        //{



        //}

        //void IDebugOnTrigger.OnDebugEnter()
        //{
        //    onEnter();
        //}

        //void IDebugOnTrigger.OnDebugExit()
        //{
        //    onExit();
        //}
    }


}
