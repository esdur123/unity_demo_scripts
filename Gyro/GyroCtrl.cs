using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 自定義的陀螺儀控制 目前只有IOS運作假AR的時候需要
/// </summary>
public class GyroCtrl : MonoBehaviour
{
    // Fields
    private readonly Quaternion baseIdentity = Quaternion.Euler(90f, 0f, 0f);
    private Quaternion baseOrientation = Quaternion.Euler(90f, 0f, 0f);
    private Quaternion baseOrientationRotationFix = Quaternion.identity;
    private Quaternion calibration = Quaternion.identity;
    private Quaternion cameraBase = Quaternion.identity;
    private bool debug = true;
    public static bool gyroAvaiable;
    private bool gyroEnabled = true;
    private Quaternion gyroInitialRotation;
    public static bool gyroOff;
    private Quaternion initialRotation;
    private readonly Quaternion landscapeLeft = Quaternion.Euler(0f, 0f, -90f);
    private readonly Quaternion landscapeRight = Quaternion.Euler(0f, 0f, 90f);
    private const float lowPassFilterFactor = 0.1f;
    private Quaternion offsetRotation;
    private Quaternion referanceRotation = Quaternion.identity;
    private readonly Quaternion upsideDown = Quaternion.Euler(0f, 0f, 180f);

    Main m_Main = null;
    Main getMain()
    {
        if (m_Main == null) m_Main = MyFinder.Instance.getMain();
        return m_Main;
    }

    public void myAwake()
    {
        gyroAvaiable = SystemInfo.supportsGyroscope;
    }

    Main.buildModeEnum m_playmode;
    float m_rot_angel_fixed_x = 0;
    public void changeCamRotX(float _x)
    {
        //Debug.Log(_x);
        m_rot_angel_fixed_x = _x;
    }


    public void init()
    {
        m_playmode = MyFinder.Instance.getMain().p_playmode;

        if (m_playmode != Main.buildModeEnum.fake_ar) return;

        Input.gyro.enabled = true;
        base.enabled = true;
        this.AttachGyro();
        this.initialRotation = base.transform.localRotation;
        this.gyroInitialRotation = Input.gyro.attitude;
    }


    bool m_is_allow_gyro = true;
    public void setGyro(bool _is_allow_gyro)
    {
        m_is_allow_gyro = _is_allow_gyro;
    }

    //Quaternion _temp_qt;    
    public void myUpdate()
    {
        if (m_playmode != Main.buildModeEnum.fake_ar) return;
        if (!m_is_allow_gyro) return;

        gyroOff = PlayerPrefs.GetInt("gyro-off") == 1;
        if (this.gyroEnabled)
        {
            //_temp_qt = Quaternion.Slerp(base.transform.localRotation, this.cameraBase * (ConvertRotation(this.referanceRotation * Input.gyro.attitude) * this.GetRotFix()), 0.5f);//0.1f
            //_temp_qt.x += m_rot_angel_fixed_x;
            //Debug.Log("_temp_qt="+ _temp_qt);
            //MyFinder.Instance.getUIDebugCtrl().showDebugTxt(_temp_qt.ToString(), 0);
            base.transform.localRotation = Quaternion.Slerp(base.transform.localRotation, this.cameraBase * (ConvertRotation(this.referanceRotation * Input.gyro.attitude) * this.GetRotFix()), 0.5f);//0.1f
                                                                                                                                                                                                      /// rot
            Vector3 new_rot = new Vector3(
                      base.transform.localRotation.eulerAngles.x + m_rot_angel_fixed_x
                    , base.transform.localRotation.eulerAngles.y
                    , base.transform.localRotation
                    .eulerAngles.z
                );

            base.transform.localRotation = Quaternion.Euler(
                     new_rot
                );

            //Debug.Log(base.transform.localRotation);
        }
        //testLeftClick();
    }

    //int test_i = 0;
    //void testLeftClick()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        test_i++;
    //        MyFinder.Instance.getUIDebugCtrl().showDebugTxt("clicked "+ test_i);
    //    }
    //}

    // Methods
    
    private void AttachGyro()
    {
        this.gyroEnabled = true;
        this.ResetBaseOrientation();
        this.UpdateCalibration(true);
        this.UpdateCameraBaseRotation(true);
        this.RecalculateReferenceRotation();
    }



    private static Quaternion ConvertRotation(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    private void DetachGyro()
    {
        this.gyroEnabled = false;
    }

    private Quaternion GetRotFix()
    {
        return Quaternion.identity;
    }

    private void RecalculateReferenceRotation()
    {
        this.referanceRotation = Quaternion.Inverse(this.baseOrientation) * Quaternion.Inverse(this.calibration);
    }

    private void ResetBaseOrientation()
    {
        this.baseOrientationRotationFix = this.GetRotFix();
        this.baseOrientation = this.baseOrientationRotationFix * this.baseIdentity;
    }



    private void UpdateCalibration(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            Vector3 toDirection = (Vector3)(Input.gyro.attitude * -Vector3.forward);
            toDirection.z = 0f;
            if (toDirection == Vector3.zero)
            {
                this.calibration = Quaternion.identity;
            }
            else
            {
                this.calibration = Quaternion.FromToRotation((Vector3)(this.baseOrientationRotationFix * Vector3.up), toDirection);
            }
        }
        else
        {
            this.calibration = Input.gyro.attitude;
        }
    }

    private void UpdateCameraBaseRotation(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            Vector3 forward = base.transform.forward;
            forward.y = 0f;
            if (forward == Vector3.zero)
            {
                this.cameraBase = Quaternion.identity;
            }
            else
            {
                this.cameraBase = Quaternion.FromToRotation(Vector3.forward, forward);
            }
        }
        else
        {
            this.cameraBase = base.transform.rotation;
        }
    }
}